package com.futvan.z.job;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.futvan.z.framework.common.bean.z_form_table;
import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.DateUtil;
import com.futvan.z.framework.util.EmailUtil;
import com.futvan.z.framework.util.SpringUtil;

public class zJob implements Job {

	public void execute(JobExecutionContext context) throws JobExecutionException {

	}
	
	/**
	 * 创建按钮
	 */
	private void CreateTableButton() {
		for(Entry<String, z_form_table> tableMap: z.tables.entrySet()){
			z_form_table table = tableMap.getValue();
			if(z.isNull(table.getParent_table_id()) && !"z_role".equals(table.getTable_id())) {
				//所有主表，不包括角色表
				HashMap<String, String> bean = new HashMap<String, String>();
				bean.put("table_id", table.getTable_id());
				bean.put("zid", table.getZid());
				
				CreateListButton_list_card_save_button(bean);
				CreateListButton_list_card_delete_button(bean);
				CreateListButton_list_card_edit_button(bean);
			}
			
		}
	}
	
	private void CreateListButton_list_card_save_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_list_card_save_button";
		if(ButtonNotExist(pid,button_id)){
			StringBuffer button_sql = new StringBuffer();
			button_sql.append("INSERT INTO z_form_table_button ");
			button_sql.append(" (zid, ");
			button_sql.append(" pid, ");
			button_sql.append(" page_type, ");
			button_sql.append(" function_type, ");
			button_sql.append(" js_onclick, ");
			button_sql.append(" button_id, ");
			button_sql.append(" button_icon, ");
			button_sql.append(" button_name) ");
			button_sql.append(" VALUES ( ");
			button_sql.append(" '"+zid+"', ");
			button_sql.append(" '"+pid+"', ");
			button_sql.append(" 'list_card', ");
			button_sql.append(" '1', ");
			button_sql.append(" 'list_card_save(this);', ");
			button_sql.append(" '"+button_id+"', ");
			button_sql.append(" 'fa fa-floppy-o', ");
			button_sql.append(" '保存') ");
			SpringUtil.getSqlSession().insert("insert", button_sql.toString());
		}
		
	}

	private void CreateListButton_list_card_delete_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_list_card_delete_button";
		if(ButtonNotExist(pid,button_id)){
			StringBuffer button_sql = new StringBuffer();
			button_sql.append("INSERT INTO z_form_table_button ");
			button_sql.append(" (zid, ");
			button_sql.append(" pid, ");
			button_sql.append(" page_type, ");
			button_sql.append(" function_type, ");
			button_sql.append(" js_onclick, ");
			button_sql.append(" button_id, ");
			button_sql.append(" button_icon, ");
			button_sql.append(" button_name) ");
			button_sql.append(" VALUES ( ");
			button_sql.append(" '"+zid+"', ");
			button_sql.append(" '"+pid+"', ");
			button_sql.append(" 'list_card', ");
			button_sql.append(" '1', ");
			button_sql.append(" 'list_card_delete(this);', ");
			button_sql.append(" '"+button_id+"', ");
			button_sql.append(" 'fa fa-trash', ");
			button_sql.append(" '删除') ");
			SpringUtil.getSqlSession().insert("insert", button_sql.toString());
		}
		
	}

	private void CreateListButton_list_card_edit_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_list_card_edit_button";
		if(ButtonNotExist(pid,button_id)){
			StringBuffer button_sql = new StringBuffer();
			button_sql.append("INSERT INTO z_form_table_button ");
			button_sql.append(" (zid, ");
			button_sql.append(" pid, ");
			button_sql.append(" page_type, ");
			button_sql.append(" function_type, ");
			button_sql.append(" js_onclick, ");
			button_sql.append(" button_id, ");
			button_sql.append(" button_icon, ");
			button_sql.append(" button_name) ");
			button_sql.append(" VALUES ( ");
			button_sql.append(" '"+zid+"', ");
			button_sql.append(" '"+pid+"', ");
			button_sql.append(" 'list_card', ");
			button_sql.append(" '0', ");
			button_sql.append(" 'list_card_edit(this);', ");
			button_sql.append(" '"+button_id+"', ");
			button_sql.append(" 'fa fa-pencil-square-o', ");
			button_sql.append(" '修改') ");
			SpringUtil.getSqlSession().insert("insert", button_sql.toString());
		}
		
	}
	
	private boolean ButtonNotExist(String pid, String button_id) {
		String sql = "SELECT COUNT(*) FROM z_form_table_button WHERE pid = '"+pid+"' AND button_id = '"+button_id+"'";
		int num = SpringUtil.getSqlSession().selectOne("selectoneint", sql);
		if(num==0) {
			return true;
		}else {
			return false;
		}
	}

}
