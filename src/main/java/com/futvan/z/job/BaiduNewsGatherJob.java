package com.futvan.z.job;

import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.SuperZ;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.DateUtil;
import com.futvan.z.framework.util.HttpUtil;
/**
 * 采购百度新闻-国内
 * @author zj
 *
 */
public class BaiduNewsGatherJob implements Job {

	public void execute(JobExecutionContext context) throws JobExecutionException {
		z.Log("采集任务启动|时间："+DateUtil.getDateTime());
		int snum1 = gatherCms_guonei();//国内新闻
		z.Log("国内新闻-采集完成|本次成功采集记录数："+snum1);
		int snum2 = gatherCms_guoji();//国际新闻
		z.Log("国际新闻-采集完成|本次成功采集记录数："+snum2);
		int snum3 = gatherCms_ent();//娱乐新闻
		z.Log("娱乐新闻-采集完成|本次成功采集记录数："+snum3);
		int snum4 = gatherCms_lady();//女人新闻 
		z.Log("女人新闻 -采集完成|本次成功采集记录数："+snum4);
		int snum5 = gatherCms_sports();//体育新闻
		z.Log("体育新闻-采集完成|本次成功采集记录数："+snum5);
		int snum6 = gatherCms_finance();//财经新闻
		z.Log("财经新闻-采集完成|本次成功采集记录数："+snum6);
		int snum7 = gatherCms_tech();//科技新闻
		z.Log("科技新闻-采集完成|本次成功采集记录数："+snum7);
		int snum8 = gatherCms_game();//游戏新闻
		z.Log("游戏新闻-采集完成|本次成功采集记录数："+snum8);
		z.Log("采集任务全部执行结束");
	}
	
	/**
	 * 上传新闻
	 * @param title 标题
	 * @param logo LOGO链接
	 * @param html HTML信息
	 * @param columnid 栏目ID
	 * @param areaid 地区ID
	 */
	public static Result UploadCMS(String title,String html,String columnid,String publish_user,String from_url) {
		Result result = new Result();
		if(z.isNotNull(title)) {
			if(z.isNotNull(html)) {
				HashMap<String,String> parameters = new HashMap<String, String>();
				parameters.put("columnid", columnid);
				parameters.put("title", title);
				parameters.put("html", html);
				parameters.put("publish_user", publish_user);
				parameters.put("from_url", from_url);
				result = HttpUtil.doPostResult("http://www.shenbianxiaoshi.com/xs_cms_save", parameters);
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("UploadCMS:html is null");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("UploadCMS:title is null");
		}
		return result;
	}

	public static int gatherCms_game() {
		int success_num = 0;
		String url = "https://news.baidu.com/game";
		String html = HttpUtil.doPost(url, null);
		Document docAll = Jsoup.parse(html);
		Elements lilist = docAll.select("#col-left li a");
		for (Element li : lilist) {
			try {
				String title = li.text();
				String cms_url = li.attr("href");

				//获取明细页面内容
				String cms_html = HttpUtil.doPost(cms_url, null);
				Document cms_doc = Jsoup.parse(cms_html);
				String author = "";
				if(z.isNotNull(cms_doc)) {
					author = cms_doc.body().selectFirst("#right-container .item-wrap .author-name").text();
				}


				String cms_text = cms_doc.body().selectFirst("#left-container .article-content").html();

				Result r = UploadCMS(title,cms_text,"",author,cms_url);
				if(z.isNotNull(r) && r.getCode().equals(Code.SUCCESS)) {
					success_num = success_num+1;
				}
				if(z.isNotNull(r) && r.getCode().equals(Code.ERROR)) {
					z.Log(r.getMsg()+"|"+r.getData());
				}
			} catch (Exception e) {
				z.Error("采购新闻出错："+li.text()+"|"+li.attr("href"), e);
				continue;
			}
		}
		return success_num;
	}

	public static int gatherCms_tech() {
		int success_num = 0;
		String url = "https://news.baidu.com/tech";
		String html = HttpUtil.doPost(url, null);
		Document docAll = Jsoup.parse(html);
		Elements lilist = docAll.select("div.middle-focus-news li a");
		for (Element li : lilist) {
			try {
				String title = li.text();
				String cms_url = li.attr("href");

				//获取明细页面内容
				String cms_html = HttpUtil.doPost(cms_url, null);
				Document cms_doc = Jsoup.parse(cms_html);
				String author = "";
				if(z.isNotNull(cms_doc)) {
					author = cms_doc.body().selectFirst("#right-container .item-wrap .author-name").text();
				}


				String cms_text = cms_doc.body().selectFirst("#left-container .article-content").html();

				Result r = UploadCMS(title,cms_text,"",author,cms_url);
				if(z.isNotNull(r) && r.getCode().equals(Code.SUCCESS)) {
					success_num = success_num+1;
				}
				if(z.isNotNull(r) && r.getCode().equals(Code.ERROR)) {
					z.Log(r.getMsg()+"|"+r.getData());
				}
			} catch (Exception e) {
				z.Error("采购新闻出错："+li.text()+"|"+li.attr("href"), e);
				continue;
			}
		}
		return success_num;
	}

	public static int gatherCms_finance() {
		int success_num = 0;
		String url = "https://news.baidu.com/finance";
		String html = HttpUtil.doPost(url, null);
		Document docAll = Jsoup.parse(html);
		Elements lilist = docAll.select("div.middle-focus-news li a");
		for (Element li : lilist) {
			try {
				String title = li.text();
				String cms_url = li.attr("href");

				//获取明细页面内容
				String cms_html = HttpUtil.doPost(cms_url, null);
				Document cms_doc = Jsoup.parse(cms_html);
				String author = "";
				if(z.isNotNull(cms_doc)) {
					author = cms_doc.body().selectFirst("#right-container .item-wrap .author-name").text();
				}


				String cms_text = cms_doc.body().selectFirst("#left-container .article-content").html();

				Result r = UploadCMS(title,cms_text,"",author,cms_url);
				if(z.isNotNull(r) && r.getCode().equals(Code.SUCCESS)) {
					success_num = success_num+1;
				}
				if(z.isNotNull(r) && r.getCode().equals(Code.ERROR)) {
					z.Log(r.getMsg()+"|"+r.getData());
				}
			} catch (Exception e) {
				z.Error("采购新闻出错："+li.text()+"|"+li.attr("href"), e);
				continue;
			}
		}
		return success_num;
	}

	public static int gatherCms_sports() {
		int success_num = 0;
		String url = "https://news.baidu.com/sports";
		String html = HttpUtil.doPost(url, null);
		Document docAll = Jsoup.parse(html);
		Elements lilist = docAll.select("div.b-left li a");
		for (Element li : lilist) {
			try {
				String title = li.text();
				String cms_url = li.attr("href");

				//获取明细页面内容
				String cms_html = HttpUtil.doPost(cms_url, null);
				Document cms_doc = Jsoup.parse(cms_html);
				String author = "";
				if(z.isNotNull(cms_doc)) {
					author = cms_doc.body().selectFirst("#right-container .item-wrap .author-name").text();
				}


				String cms_text = cms_doc.body().selectFirst("#left-container .article-content").html();

				Result r = UploadCMS(title,cms_text,"",author,cms_url);
				if(z.isNotNull(r) && r.getCode().equals(Code.SUCCESS)) {
					success_num = success_num+1;
				}
				if(z.isNotNull(r) && r.getCode().equals(Code.ERROR)) {
					z.Log(r.getMsg()+"|"+r.getData());
				}
			} catch (Exception e) {
				z.Error("采购新闻出错："+li.text()+"|"+li.attr("href"), e);
				continue;
			}
		}
		return success_num;
	}

	public static int gatherCms_lady() {
		int success_num = 0;
		String url = "https://news.baidu.com/lady";
		String html = HttpUtil.doPost(url, null);
		Document docAll = Jsoup.parse(html);
		Elements lilist = docAll.select("div.b-left li a");
		for (Element li : lilist) {
			try {
				String title = li.text();
				String cms_url = li.attr("href");

				//获取明细页面内容
				String cms_html = HttpUtil.doPost(cms_url, null);
				Document cms_doc = Jsoup.parse(cms_html);
				String author = "";
				if(z.isNotNull(cms_doc)) {
					author = cms_doc.body().selectFirst("#right-container .item-wrap .author-name").text();
				}

				String cms_text = cms_doc.body().selectFirst("#left-container .article-content").html();

				Result r = UploadCMS(title,cms_text,"",author,cms_url);
				if(z.isNotNull(r) && r.getCode().equals(Code.SUCCESS)) {
					success_num = success_num+1;
				}
				if(z.isNotNull(r) && r.getCode().equals(Code.ERROR)) {
					z.Log(r.getMsg()+"|"+r.getData());
				}
			} catch (Exception e) {
				z.Error("采购新闻出错："+li.text()+"|"+li.attr("href"), e);
				continue;
			}
		}
		return success_num;
	}

	public static int gatherCms_ent() {
		int success_num = 0;
		String url = "https://news.baidu.com/ent";
		String html = HttpUtil.doPost(url, null);
		Document docAll = Jsoup.parse(html);
		Elements lilist = docAll.select("div.b-left li a");
		for (Element li : lilist) {
			try {
				String title = li.text();
				String cms_url = li.attr("href");

				//获取明细页面内容
				String cms_html = HttpUtil.doPost(cms_url, null);
				Document cms_doc = Jsoup.parse(cms_html);
				String author = "";
				if(z.isNotNull(cms_doc)) {
					author = cms_doc.body().selectFirst("#right-container .item-wrap .author-name").text();
				}


				String cms_text = cms_doc.body().selectFirst("#left-container .article-content").html();

				Result r = UploadCMS(title,cms_text,"",author,cms_url);
				if(z.isNotNull(r) && r.getCode().equals(Code.SUCCESS)) {
					success_num = success_num+1;
				}
				if(z.isNotNull(r) && r.getCode().equals(Code.ERROR)) {
					z.Log(r.getMsg()+"|"+r.getData());
				}
			} catch (Exception e) {
				z.Error("采购新闻出错："+li.text()+"|"+li.attr("href"), e);
				continue;
			}
		}
		return success_num;
	}

	public static int gatherCms_guoji() {
		int success_num = 0;
		String url = "https://news.baidu.com/guoji";
		String html = HttpUtil.doPost(url, null);
		Document docAll = Jsoup.parse(html);
		Elements lilist = docAll.select("div.b-left li a");
		for (Element li : lilist) {
			try {
				String title = li.text();
				String cms_url = li.attr("href");

				//获取明细页面内容
				String cms_html = HttpUtil.doPost(cms_url, null);
				Document cms_doc = Jsoup.parse(cms_html);
				String author = "";
				if(z.isNotNull(cms_doc)) {
					author = cms_doc.body().selectFirst("#right-container .item-wrap .author-name").text();
				}


				String cms_text = cms_doc.body().selectFirst("#left-container .article-content").html();

				Result r = UploadCMS(title,cms_text,"",author,cms_url);
				if(z.isNotNull(r) && r.getCode().equals(Code.SUCCESS)) {
					success_num = success_num+1;
				}
				if(z.isNotNull(r) && r.getCode().equals(Code.ERROR)) {
					z.Log(r.getMsg()+"|"+r.getData());
				}
				//			z.Log("title:"+title);
				//			z.Log("cms_url:"+cms_url);
				//			z.Log("author："+author);
				//			z.Log("cms_text:"+cms_text);
			} catch (Exception e) {
				z.Error("采购新闻出错："+li.text()+"|"+li.attr("href"), e);
				continue;
			}
		}
		return success_num;
	}

	public static int gatherCms_guonei() {
		int success_num = 0;
		String url = "https://news.baidu.com/guonei";
		String html = HttpUtil.doPost(url, null);
		Document docAll = Jsoup.parse(html);
		Elements lilist = docAll.select("div.b-left li a");
		for (Element li : lilist) {
			try {
				String title = li.text();
				String cms_url = li.attr("href");

				//获取明细页面内容
				String cms_html = HttpUtil.doPost(cms_url, null);
				Document cms_doc = Jsoup.parse(cms_html);
				String author = "";
				if(z.isNotNull(cms_doc)) {
					author = cms_doc.body().selectFirst("#right-container .item-wrap .author-name").text();
				}


				String cms_text = cms_doc.body().selectFirst("#left-container .article-content").html();

				Result r = UploadCMS(title,cms_text,"",author,cms_url);

				if(z.isNotNull(r) && r.getCode().equals(Code.SUCCESS)) {
					success_num = success_num+1;
				}
				if(z.isNotNull(r) && r.getCode().equals(Code.ERROR)) {
					z.Log(r.getMsg()+"|"+r.getData());
				}
			} catch (Exception e) {
				z.Error("采购新闻出错："+li.text()+"|"+li.attr("href"), e);
				continue;
			}
		}
		
		return success_num;

	}

}
