package com.futvan.z.job;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.BaiduUtil;
import com.futvan.z.framework.util.SpringUtil;
/**
 * 舆情信息采集任务-每小时0点自动执行
 * @author zz
 *
 */
public class SearchEnginesJob implements Job {

	public void execute(JobExecutionContext context) throws JobExecutionException {
		//获取所有舆情采集模板
		String sql = "select * from erp_search_engines ";
		List<HashMap<String,String>> list = SpringUtil.getSqlSession().selectList("select", sql);
		for (HashMap<String, String> se : list) {
			String keyinfo = se.get("keyinfo");
			String email = se.get("email");
			//执行采集任务
			try {
				BaiduUtil.getList2(keyinfo,5,null,0,email,se.get("zid"));
			} catch (Exception e) {
				throw new JobExecutionException("舆情信息采集任务-每小时0点自动执行出错"+e.getMessage());
			}
		}
	}

}
