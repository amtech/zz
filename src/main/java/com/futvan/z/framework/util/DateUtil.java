package com.futvan.z.framework.util;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.futvan.z.framework.core.z;
/**
 * 日期工具类
 * @author zz
 *
 */
public class DateUtil {

	/**
	 * 获取日期
	 */
	public static String getDate() {
		return getDateTime("yyyy-MM-dd");
	}

	/**
	 * 获取日期时间
	 */
	public static String getDateTime() {
		return getDateTime("yyyy-MM-dd HH:mm:ss");
	}

	
	/**
	 * 将字符串转换为时间 
	 * @param strDate
	 * @param pattern
	 * @return
	 */
	public static Date StringToDate(String strDate,String pattern) {
		if(z.isNull(pattern)) {
			pattern = "yyyy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(strDate, pos);
		return strtodate;
	}
	
	/**
	 * 将日期转为字符串
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String DateToString(Date date,String pattern) {
		if(z.isNull(pattern)) {
			pattern = "yyyy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		String dateString = formatter.format(date);
		return dateString;
	}

	/**
	 * 获取日期
	 * @param pattern yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String getDateTime(String pattern) {
		SimpleDateFormat dateformat1 = new SimpleDateFormat(pattern);
		return dateformat1.format(new java.util.Date());
	}

	/**
	 * 日期格式化
	 * 
	 * @param date
	 * @param pattern
	 *            yyyy-MM-dd yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String FormatDate(Object date, String pattern) {
		if(date!=null) {
			SimpleDateFormat dateformat1 = new SimpleDateFormat(pattern);
			return dateformat1.format(date);
		}else {
			return "";
		}

	}
	
	/**
	 * 时间加分钟
	 * @param date 日期
	 * @param minute 增加的分钟
	 * @return
	 */
	public static String addMinute(String date, int minute) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Calendar cd = Calendar.getInstance();
			cd.setTime(sdf.parse(date));
			cd.add(Calendar.MINUTE, minute);//加分钟
			return sdf.format(cd.getTime());
		} catch (Exception e) {
			return null;
		}
	}
	

}
