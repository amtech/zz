package com.futvan.z.framework.util;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.futvan.z.framework.core.z;

public class SpringUtil implements ApplicationContextAware{
	private static ApplicationContext applicationContext;

	public void setApplicationContext(ApplicationContext _applicationContext) throws BeansException {
		applicationContext = _applicationContext;
	}
	
	public static Object getBean(String name) throws BeansException {
        return applicationContext.getBean(name);
    }
	
	public static SqlSession getSqlSession() {
		Object obj = getBean("sqlSession");
		if(z.isNotNull(obj) && obj instanceof SqlSession) {
			return (SqlSession)obj;
		}else {
			return null;
		}
	}
	
}
