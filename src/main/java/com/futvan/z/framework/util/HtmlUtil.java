package com.futvan.z.framework.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * HTML处理工具类
 * @author zj
 *
 */
public class HtmlUtil {
	/**
	 * 分析HTML提取信息
	 * @param htmlTXT
	 * @param selectId   
	 *        所有超连接的地址：a[href]
	 *        
	 * @return
	 */
	public static Elements getElements(String htmlTXT,String selectId){
		Elements returnvalue = new Elements();
		Document docAll = Jsoup.parse(htmlTXT);
		returnvalue = docAll.select(selectId);
		return returnvalue;
	}
	
	/**
	 * 分析HTML提取信息
	 * @param htmlTXT
	 * @param selectId   
	 *        所有超连接的地址：a[href]
	 *        
	 * @return
	 */
	public static Element getElement(String htmlTXT,String selectId){
		return Jsoup.parse(htmlTXT).select(selectId).first();
	}
}
