package com.futvan.z.framework.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.ProxyIP;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.bean.z_form_table_column;
import com.futvan.z.framework.core.z;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/***
 * 百度工具类
 * @author zj
 *
 */
public class BaiduUtil {

	public static void main(String[] args) throws Exception {
		//		List<ProxyIP> iplist = CommonServicesUtil.getProxyIPList(1);
		//		for (ProxyIP ip : iplist) {
		//			try {
		//				getList("zframeworks","免费JAVA开发平台","",3,ip.getIp(),ip.getPort());
		//			} catch (Exception e) {
		//				continue;
		//			}
		//		}
		//		getList("zframeworks","免费JAVA开发平台","",50,null,0);
	}

	private static void SendEmail(String email, List<HashMap<String, String>> resultList) throws Exception {
		String filepath = FileUtil.CreateFileSavePath("yuqing");
		File email_file = new File(filepath+"\\"+DateUtil.getDateTime("yyyyMMddHHmm")+".xls");
		WritableWorkbook wwb = Workbook.createWorkbook(email_file);
		//创建工作表
		wwb.createSheet("舆情采集记录", 0);
		
		//获取工作表 
		WritableSheet ws = wwb.getSheet(0);

		//表头行样式
		WritableCellFormat TableHead = new WritableCellFormat();
		TableHead.setBorder(Border.ALL, BorderLineStyle.THIN);
		TableHead.setAlignment(Alignment.LEFT);
		TableHead.setBackground(Colour.GRAY_25);
		//表体数据行样式
		WritableCellFormat TableRow = new WritableCellFormat();
		TableRow.setAlignment(Alignment.LEFT);
		
		//生成表头行
		ws.addCell(new Label(0,0,"标题",TableHead));
		ws.addCell(new Label(1,0,"文章链接",TableHead));
		
		for (int i=0;i<resultList.size();i++) {
			ws.addCell(new Label(0,i+1,resultList.get(i).get("title"),TableRow));
			ws.addCell(new Label(1,i+1,resultList.get(i).get("url"),TableRow));
		}
		wwb.write();
		wwb.close();
		
		Result r = EmailUtil.sent(email, "舆情报告", "舆情报告见附件", email_file.getPath());
		if(z.isNotNull(r) && Code.SUCCESS.equals(r.getCode())) {
			email_file.delete();
		}
	}
	
	

	/**
	 * 	搜索百度记录
	 * @param website 目标网站名称 例如：身边小事
	 * @param domain_name 域名 shenbianxiaoshi.com
	 * @param search 关键词 例如：美罗国际
	 * @param title 文章标题 例如：希望小学
	 * @param pages 读取分页数
	 * @throws Exception 
	 */
	public static void getList(String domain_name,String search,String title,int pages,String ip,int port) throws Exception {
		if(pages<1) {
			pages = 1;
		}

		if(z.isNotNull(ip) && port>0) {
			z.Log("代理IP："+ip+"|端口："+port);
		}

		search = StringUtil.UrlEncode(search);
		if(z.isNull(search)) {
			z.Error("搜索百度记录 出错， search is null");
			return;
		}

		if(z.isNull(domain_name)) {
			z.Error("搜索百度记录 出错， domain_name 域名不能为空");
			return;
		}
		for (int i = 0; i < pages; i++) {
			int page_num = i+1;
			z.Log("搜索百度 [第"+page_num+"页]");
			int pn = i*10;
			String url = "https://www.baidu.com/s?wd="+search+"&pn="+pn;
			String html = "";
			if(z.isNotNull(ip) && port>0) {
				html = HttpUtil.doGetThrowsException(url, null,ip,port);
			}else {
				html = HttpUtil.doGet(url, null);
			}
			Document doc = Jsoup.parse(html);
			Elements list = doc.select("#content_left div");
			for (Element el : list) {
				String cms_title = el.select("h3").text();//标题
				String from_website = el.select(".f13 a.c-showurl").text();//来源网站地址
				String from_url = el.select("h3 a").attr("href");//URL
				if(z.isNotNull(from_url)) {
					if(z.isNotNull(cms_title) || z.isNotNull(from_website)) {
						if(from_website.indexOf(domain_name)>=0) {
							if(z.isNotNull(title)) {
								if(cms_title.indexOf(title)>=0) {
									lookCMS(cms_title,from_website,from_url,ip,port);
								}
							}else {
								lookCMS(cms_title,from_website,from_url,ip,port);
							}
						}
					}
				}

			}
		}

	}



	
	/**
	 * 采集在百度的舆情信息
	 * @param search 关键词
	 * @param pages 采集百度页数
	 * @param ip 代理IP
	 * @param port 代理IP端口
	 * @param email 接收报告Email
	 * @param zid 舆情模板ID
	 * @return
	 * @throws Exception
	 */
	public static List<HashMap<String,String>> getList2(String search,int pages,String ip,int port,String email, String zid) throws Exception {
		List<HashMap<String,String>> resultList = new ArrayList<HashMap<String,String>>();
	
		if(pages<1) {
			pages = 1;
		}
	
		if(z.isNotNull(ip) && port>0) {
			z.Log("代理IP："+ip+"|端口："+port);
		}
	
		search = StringUtil.UrlEncode(search);
		if(z.isNotNull(search)) {
			for (int i = 0; i < pages; i++) {
				int page_num = i+1;
				z.Log("搜索百度 [第"+page_num+"页]");
				int pn = i*10;
				String url = "https://www.baidu.com/s?wd="+search+"&pn="+pn;
				String html = "";
				if(z.isNotNull(ip) && port>0) {
					html = HttpUtil.doGetThrowsException(url, null,ip,port);
				}else {
					html = HttpUtil.doGet(url, null);
				}
				Document doc = Jsoup.parse(html);
				Elements list = doc.select("#content_left div");
				for (Element el : list) {
					String cms_title = el.select("h3").text();//标题
					String from_website = el.select(".f13 a.c-showurl").text();//来源网站地址
					String from_url = el.select("h3 a").attr("href");//URL
					if(z.isNotNull(from_url)) {
						//设置返回集合
						HashMap<String,String> rMap = new HashMap<String, String>();
						rMap.put("title", cms_title+"|来源网站："+from_website);
						rMap.put("url", from_url);
						resultList.add(rMap);
					}
	
				}
			}
		}else {
			z.Error("关键词无法转换成UrlEncode");
		}
	
		//生成并发送
		if(resultList.size()>0 && z.isNotNull(email)) {
			SendEmail(email,resultList);
		}
		
		//保存数据
		for (HashMap<String, String> map : resultList) {
			String isql = "INSERT INTO erp_search_engines_info (zid, title, url, search_engines_id) VALUES ('"+z.newZid("erp_search_engines_info")+"', '"+map.get("title")+"', '"+map.get("url")+"', '"+zid+"')";
			SpringUtil.getSqlSession().insert("insert", isql);
		}
	
		return resultList;
	
	}

	private static void lookCMS(String cms_title,String from_website,String from_url,String ip,int port) {
		z.Log("======================================================================================================");
		z.Log("标题："+cms_title);
		z.Log("来源网站："+from_website);
		z.Log("来源网址："+from_url);
		String html = "";
		String isOk = "";
		if(z.isNotNull(ip) && port>0) {
			html = HttpUtil.doGet(from_url, null, ip, port);
		}else {
			html = HttpUtil.doGet(from_url, null);
		}
		if(z.isNull(html)) {
			z.Log("调用失败");
		}
		z.Log("======================================================================================================");
	}

}
