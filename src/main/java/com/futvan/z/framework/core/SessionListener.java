package com.futvan.z.framework.core;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;


/**
 * Session监听
 * @author zj
 *
 */
@WebListener
public class SessionListener extends SuperZ implements HttpSessionListener {

	/**
	 * Session创建
	 */
	public void sessionCreated(HttpSessionEvent se) {
	}

	/**
	 * Session销毁
	 */
	public void sessionDestroyed(HttpSessionEvent se) {
		DeleteSessionUser(se.getSession().getId());
	}
	
}
