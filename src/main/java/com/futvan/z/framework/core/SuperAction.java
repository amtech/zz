package com.futvan.z.framework.core;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceHandlerMethodArgumentResolver;
import org.springframework.mobile.device.DeviceResolverHandlerInterceptor;
import org.springframework.mobile.device.LiteDeviceResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

import com.futvan.z.system.zorg.z_org;
import com.futvan.z.system.zuser.z_user;

public class SuperAction extends SuperZ{

	@Autowired
	protected HttpServletRequest request;
	
	/**
	 * 获取WebApp路径
	 * @return Path
	 * 获取WebApp目录下的图片
	 * 例：String filePath = getWebAppPath()+"img\\zlogin.png";
	 * 
	 */
	protected String getWebAppPath() {
		return request.getSession().getServletContext().getRealPath(""); 
	}

	
	/**
	 * 	获取访问设备类型
	 * 
	 * device.isMobile() == true  手机
	 * device.isTablet() == true  平板
	 * 	其它 == PC
	 * 
	 * //获取访问设备类型
		Device device = getRequestDevice();
		if (device.isMobile()) {
			//手机页面
			return "common/main/login_mobile";
        }else {
        	//PC页面
        	return "common/main/login";  
        }
	 * @return
	 */
//	protected Device getRequestDevice() {
//		if(z.isNotNull(request)) {
//			LiteDeviceResolver deviceResolver = new LiteDeviceResolver();
//			Device device = deviceResolver.resolveDevice(request);
//			return device;
//		}else {
//			return null;
//		}
//	}
}
