package com.futvan.z.framework.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.futvan.z.framework.common.bean.QueryTerms;
import com.futvan.z.framework.common.bean.Result;

public class SuperBean{
	private String zid;//主键
	private String pid;//上级主键
	private String number;//编号
	private String name;//名称
	private String seq;//序号
	private String create_user;//创建者
	private String create_time;//创建时间
	private String update_user;//修改者
	private String update_time;//修改时间
	private String orgid;//所属组织ID
	private String zversion;//数据版本
	private String remarks;//备注
	private String sql_order_by;//SQL-Order by 
	private String sql_where_other;//SQL-其它条件
	/**
	 * @return zid
	 */
	public String getZid() {
		return zid;
	}
	/**
	 * @param zid 要设置的 zid
	 */
	public void setZid(String zid) {
		this.zid = zid;
	}
	/**
	 * @return pid
	 */
	public String getPid() {
		return pid;
	}
	/**
	 * @param pid 要设置的 pid
	 */
	public void setPid(String pid) {
		this.pid = pid;
	}
	/**
	 * @return number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * @param number 要设置的 number
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name 要设置的 name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return seq
	 */
	public String getSeq() {
		return seq;
	}
	/**
	 * @param seq 要设置的 seq
	 */
	public void setSeq(String seq) {
		this.seq = seq;
	}
	/**
	 * @return create_user
	 */
	public String getCreate_user() {
		return create_user;
	}
	/**
	 * @param create_user 要设置的 create_user
	 */
	public void setCreate_user(String create_user) {
		this.create_user = create_user;
	}
	/**
	 * @return create_time
	 */
	public String getCreate_time() {
		return create_time;
	}
	/**
	 * @param create_time 要设置的 create_time
	 */
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	/**
	 * @return update_user
	 */
	public String getUpdate_user() {
		return update_user;
	}
	/**
	 * @param update_user 要设置的 update_user
	 */
	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}
	/**
	 * @return update_time
	 */
	public String getUpdate_time() {
		return update_time;
	}
	/**
	 * @param update_time 要设置的 update_time
	 */
	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}
	/**
	 * @return orgid
	 */
	public String getOrgid() {
		return orgid;
	}
	/**
	 * @param orgid 要设置的 orgid
	 */
	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}
	/**
	 * @return zversion
	 */
	public String getZversion() {
		return zversion;
	}
	/**
	 * @param zversion 要设置的 zversion
	 */
	public void setZversion(String zversion) {
		this.zversion = zversion;
	}
	/**
	 * @return remarks
	 */
	public String getRemarks() {
		return remarks;
	}
	/**
	 * @param remarks 要设置的 remarks
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	/**
	 * @return sql_order_by
	 */
	public String getSql_order_by() {
		return sql_order_by;
	}
	/**
	 * @param sql_order_by 要设置的 sql_order_by
	 */
	public void setSql_order_by(String sql_order_by) {
		this.sql_order_by = sql_order_by;
	}
	/**
	 * @return sql_where_other
	 */
	public String getSql_where_other() {
		return sql_where_other;
	}
	/**
	 * @param sql_where_other 要设置的 sql_where_other
	 */
	public void setSql_where_other(String sql_where_other) {
		this.sql_where_other = sql_where_other;
	}
	/* （非 Javadoc）
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SuperBean [zid=" + zid + ", pid=" + pid + ", number=" + number + ", name=" + name + ", seq=" + seq
				+ ", create_user=" + create_user + ", create_time=" + create_time + ", update_user=" + update_user
				+ ", update_time=" + update_time + ", orgid=" + orgid + ", zversion=" + zversion + ", remarks=" + remarks
				+ ", sql_order_by=" + sql_order_by + ", sql_where_other=" + sql_where_other + "]";
	}
	
}
