package com.futvan.z.framework.tags;

import java.util.List;

import javax.servlet.jsp.JspException;

import com.futvan.z.framework.common.bean.z_form_table_column;
import com.futvan.z.framework.core.SuperTag;
import com.futvan.z.framework.core.z;
import com.futvan.z.system.zcode.z_code;
import com.futvan.z.system.zcode.z_code_detail;

public class column extends SuperTag{
	private String tableId;
	private String column_id;
	private String column_type;
	private String column_name;
	private String column_help;
	private String mode_type;
	private String column_size;
	private String textarea_height;
	private String is_null;
	private String is_hidden;
	private String is_readonly;
	private String arrangement_direction;
	private String isbr;
	private String p_code_id;
	private String z5_table;
	private String z5_key;
	private String z5_value;
	private String value;
	@Override
	public int doEndTag() throws JspException {
		StringBuffer out_html = new StringBuffer();
		
		//处理空值 ，统一输出“”
		if(z.isNull(value)) {
			value="";
		}
		
		if(!"1".equals(is_hidden)) {//判读是否隐藏

			out_html.append("<div class=\"col-md-"+column_size+"\">").append("\r\n");
			out_html.append("<fieldset class=\"form-group\">").append("\r\n");
			out_html.append("<label data-toggle=\"tooltip\" data-placement=\"top\" class=\""+ColumnIsNull(is_null)+"\" title=\""+column_help+"\">【"+column_name+"】</label>").append("\r\n");
			if("0".equals(column_type)) {//文本
				out_html.append("<input placeholder=\""+column_help+"\" id=\""+column_id+"_id\" name=\""+column_id+"\" type=\"text\" class=\"form-control\" value=\""+value+"\" "+ColumnIsReadonly(is_readonly)+"/>").append("\r\n");
			}else if("1".equals(column_type)) {//多行文本
				String textarea_height = "300";
				if(textarea_height!=null && !"".equals(textarea_height)) {
					textarea_height = textarea_height;
				}
				out_html.append("<textarea placeholder=\""+column_help+"\" id=\""+column_id+"_id\" name=\""+column_id+"\" style=\"height: "+textarea_height+"px\" class=\"form-control\" "+ColumnIsReadonly(is_readonly)+">"+value+"</textarea>").append("\r\n");
			}else if("2".equals(column_type)) {//数字
				out_html.append("<input placeholder=\""+column_help+"\" id=\""+column_id+"_id\" name=\""+column_id+"\" type=\"number\" class=\"form-control\" value=\""+value+"\" "+ColumnIsReadonly(is_readonly)+"/>").append("\r\n");
			}else if("3".equals(column_type)) {//文件
				out_html.append("<div class=\"input-group\">").append("\r\n");
				String ColumnValue = value;
				out_html.append("<input  placeholder=\""+column_help+"\" id=\""+column_id+"_id\" name=\""+column_id+"\" value=\""+getZ5DisplayValue(z5_table, z5_key, z5_value, ColumnValue)+"\" type=\"url\" class=\"form-control marginleft1\" "+ColumnIsReadonly(is_readonly)+"/>").append("\r\n");
				out_html.append("<span class=\"input-group-btn btn-group btn-group-sm marginright1\">").append("\r\n");
				out_html.append("<button class=\"btn btn-light\" onclick=\"uploadFile('"+column_id+"','"+z.newNumber()+"')\" type=\"button\"><i class=\"fa fa-upload\"></i></button>").append("\r\n");
				out_html.append("<button class=\"btn btn-light\" onclick=\"downloadFile('"+column_id+"')\" type=\"button\"><i class=\"fa fa-download\"></i></button>").append("\r\n");
				out_html.append("</span>").append("\r\n");
				out_html.append("</div>").append("\r\n");
			}else if("4".equals(column_type)) {//图片
				out_html.append("<div class=\"input-group\">").append("\r\n");
				String ColumnValue = value;
				out_html.append("<input  placeholder=\""+column_help+"\" id=\""+column_id+"_id\" name=\""+column_id+"\" value=\""+getZ5DisplayValue(z5_table, z5_key, z5_value, ColumnValue)+"\" type=\"url\" class=\"form-control marginleft1\" "+ColumnIsReadonly(is_readonly)+"/>").append("\r\n");
				out_html.append("<span class=\"input-group-btn btn-group btn-group-sm marginright1\">").append("\r\n");
				out_html.append("<button class=\"btn btn-light\" onclick=\"uploadFile('"+column_id+"','"+z.newNumber()+"')\" type=\"button\"><i class=\"fa fa-upload\"></i></button>").append("\r\n");
				out_html.append("<button class=\"btn btn-light\" onclick=\"lookupImg('"+column_id+"')\" type=\"button\"><i class=\"fa fa-picture-o\"></i></button>").append("\r\n");
				out_html.append("</span>").append("\r\n");
				out_html.append("</div>").append("\r\n");
			}else if("5".equals(column_type)) {//多选
				//获取Code
				if(!"".equals(p_code_id) && p_code_id!=null) {
					z_code code = z.code.get(p_code_id);
					if(code!=null && code.getZ_code_detail_list().size()>0) {
						List<z_code_detail> delailList = code.getZ_code_detail_list();
						out_html.append("<div class=\"zcheckbox_div\">").append("\r\n");
						//out_html.append("<label>").append("\r\n");
						out_html.append("<input id=\""+column_id+"_id\" name=\""+column_id+"\" type=\"hidden\"  value=\""+value+"\"/>").append("\r\n");
						for (z_code_detail code_d : delailList) {
							boolean ic = isChecked(value,code_d.getZ_key());
							if(ic) {
								out_html.append("<div class=\""+GetArrangementDirectionCss(arrangement_direction)+"\"><input class=\"zcheckbox\" onchange=\"getCheckedValue('"+column_id+"_id','"+column_id+"_checked_id')\" type=\"checkbox\" checked=\"checked\" id=\""+column_id+"_checked_id\" value=\""+code_d.getZ_key()+"\" "+ColumnIsReadonly(is_readonly)+"> "+code_d.getZ_value()+" </div>").append("\r\n");
							}else {
								out_html.append("<div class=\""+GetArrangementDirectionCss(arrangement_direction)+"\"><input class=\"zcheckbox\" onchange=\"getCheckedValue('"+column_id+"_id','"+column_id+"_checked_id')\" type=\"checkbox\"                     id=\""+column_id+"_checked_id\" value=\""+code_d.getZ_key()+"\" "+ColumnIsReadonly(is_readonly)+"> "+code_d.getZ_value()+" </div>").append("\r\n");
							}

						}
						//out_html.append("</label>").append("\r\n");
						out_html.append("</div>").append("\r\n");
					}

				}
			}else if("6".equals(column_type)) {//单选
				//获取Code
				if(!"".equals(p_code_id) && p_code_id!=null) {
					z_code code = z.code.get(p_code_id);
					if(code!=null && code.getZ_code_detail_list().size()>0) {
						List<z_code_detail> delailList = code.getZ_code_detail_list();
						out_html.append("<div class=\"zcheckbox_div\">").append("\r\n");
						//out_html.append("<label>").append("\r\n");
						for (z_code_detail code_d : delailList) {
							if(value.equals(code_d.getZ_key())) {
								out_html.append("<div class=\""+GetArrangementDirectionCss(arrangement_direction)+"\"><input class=\"zcheckbox\" type=\"radio\" checked=\"checked\" name=\""+column_id+"\" id=\""+column_id+"_id\" value=\""+code_d.getZ_key()+"\" "+ColumnIsReadonly(is_readonly)+"> "+code_d.getZ_value()+" </div>").append("\r\n");
							}else {
								out_html.append("<div class=\""+GetArrangementDirectionCss(arrangement_direction)+"\"><input class=\"zcheckbox\" type=\"radio\" name=\""+column_id+"\" id=\""+column_id+"_id\" value=\""+code_d.getZ_key()+"\" "+ColumnIsReadonly(is_readonly)+"> "+code_d.getZ_value()+" </div>").append("\r\n");
							}

						}
						//out_html.append("</label>").append("\r\n");
						out_html.append("</div>").append("\r\n");
					}

				}
			}else if("7".equals(column_type)) {//下拉框
				out_html.append("<select class=\"form-control\" name=\""+column_id+"\" id=\""+column_id+"_id\" "+ColumnIsReadonly(is_readonly)+">").append("\r\n");
				out_html.append("<option value=\"\">请选择</option>").append("\r\n");
				//获取Code
				if(!"".equals(p_code_id) && p_code_id!=null) {
					z_code code = z.code.get(p_code_id);
					if(code!=null && code.getZ_code_detail_list().size()>0) {
						List<z_code_detail> delailList = code.getZ_code_detail_list();
						for (z_code_detail code_d : delailList) {
							if(value.equals(code_d.getZ_key())) {
								out_html.append("<option value=\""+code_d.getZ_key()+"\" selected=\"selected\">"+code_d.getZ_value()+"</option>").append("\r\n");
							}else {
								out_html.append("<option value=\""+code_d.getZ_key()+"\" >"+code_d.getZ_value()+"</option>").append("\r\n");
							}
						}
					}
				}
				out_html.append("</select>").append("\r\n");
			}else if("8".equals(column_type)) {//Z5
				out_html.append("<div class=\"input-group\">").append("\r\n");
				String ColumnValue = value;
				String SelectDataOnClick = "onclick=\"Z5list('"+tableId+"_"+column_id+"','"+column_id+"_id','"+column_id+"_display',0)\"";
				out_html.append("<input id=\""+column_id+"_id\" name=\""+column_id+"\" type=\"hidden\"  value=\""+ColumnValue+"\"/>").append("\r\n");
				out_html.append("<input  placeholder=\""+column_help+"\" id=\""+column_id+"_display\" "+SelectDataOnClick+" name=\""+column_id+"_display\" value=\""+getZ5DisplayValue(z5_table, z5_key, z5_value, ColumnValue)+"\" type=\"text\" class=\"form-control marginleft1\"  readonly=\"readonly\" />").append("\r\n");
				out_html.append("<span class=\"input-group-prepend marginright1\">").append("\r\n");
				out_html.append("<button class=\"btn btn-light\" onclick=\"Z5Clear('"+tableId+"_"+column_id+"','"+column_id+"_id','"+column_id+"_display')\" type=\"button\" "+ColumnIsDisabled(is_readonly)+"><i class=\"fa fa-trash-o\"></i></button>").append("\r\n");
				out_html.append("<button class=\"btn btn-light\" "+SelectDataOnClick+" type=\"button\" "+ColumnIsDisabled(is_readonly)+"><i class=\"fa fa-search\"></i></button>").append("\r\n");
				out_html.append("</span>").append("\r\n");
				out_html.append("</div>").append("\r\n");
			}else if("9".equals(column_type)) {//日期
				out_html.append("<input  placeholder=\""+column_help+"\" id=\""+column_id+"_id\" name=\""+column_id+"\" type=\"text\" class=\"Wdate form-control\" value=\""+value+"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})\" "+ColumnIsReadonly(is_readonly)+"/>").append("\r\n");
			}else if("10".equals(column_type)) {//日期时间
				out_html.append("<input  placeholder=\""+column_help+"\" id=\""+column_id+"_id\" name=\""+column_id+"\" type=\"text\" class=\"Wdate form-control\" value=\""+value+"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})\" "+ColumnIsReadonly(is_readonly)+"/>").append("\r\n");
			}else if("11".equals(column_type)) {//HTML输入框
				String textarea_height = "300";
				if(textarea_height!=null && !"".equals(textarea_height)) {
					textarea_height = textarea_height;
				}
				out_html.append("<textarea  placeholder=\""+column_help+"\" id=\""+column_id+"_id\" name=\""+column_id+"\" style=\"height: "+textarea_height+"px\" "+ColumnIsReadonly(is_readonly)+">"+value+"</textarea>").append("\r\n");
				out_html.append("<script type=\"text/javascript\">InitHTMLColumn('"+column_id+"_id');</script>").append("\r\n");
			}else if("12".equals(column_type)) {//源码输入框
				String textarea_height = "300";
				if(textarea_height!=null && !"".equals(textarea_height)) {
					textarea_height = textarea_height;
				}
				out_html.append("<textarea  placeholder=\""+column_help+"\"  id=\""+column_id+"_id\" name=\""+column_id+"\" style=\"height: "+textarea_height+"px\" "+ColumnIsReadonly(is_readonly)+">"+value+"</textarea>").append("\r\n");
				out_html.append("<script type=\"text/javascript\">InitCodeColumn('"+column_id+"_id','"+mode_type+"','"+textarea_height+"');</script>").append("\r\n");
			}else if("password".equals(column_type)) {//文本
				out_html.append("<input placeholder=\""+column_help+"\" id=\""+column_id+"_id\" name=\""+column_id+"\" type=\"password\" class=\"form-control\" value=\""+value+"\" "+ColumnIsReadonly(is_readonly)+"/>").append("\r\n");
			}
			out_html.append("</fieldset>").append("\r\n");
			out_html.append("</div>").append("\r\n");
			if("1".equals(isbr)) {
				out_html.append("<div class=\"col-md-12\"></div>").append("\r\n");
			}

		}
		outHTML(out_html.toString());
		return super.doEndTag();
	}
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getColumn_id() {
		return column_id;
	}
	public void setColumn_id(String column_id) {
		this.column_id = column_id;
	}
	public String getColumn_type() {
		return column_type;
	}
	public void setColumn_type(String column_type) {
		this.column_type = column_type;
	}
	public String getColumn_name() {
		return column_name;
	}
	public void setColumn_name(String column_name) {
		this.column_name = column_name;
	}
	public String getColumn_help() {
		return column_help;
	}
	public void setColumn_help(String column_help) {
		this.column_help = column_help;
	}
	public String getMode_type() {
		return mode_type;
	}
	public void setMode_type(String mode_type) {
		this.mode_type = mode_type;
	}
	public String getColumn_size() {
		return column_size;
	}
	public void setColumn_size(String column_size) {
		this.column_size = column_size;
	}
	public String getTextarea_height() {
		return textarea_height;
	}
	public void setTextarea_height(String textarea_height) {
		this.textarea_height = textarea_height;
	}
	public String getIs_null() {
		return is_null;
	}
	public void setIs_null(String is_null) {
		this.is_null = is_null;
	}
	public String getIs_hidden() {
		return is_hidden;
	}
	public void setIs_hidden(String is_hidden) {
		this.is_hidden = is_hidden;
	}
	public String getIs_readonly() {
		return is_readonly;
	}
	public void setIs_readonly(String is_readonly) {
		this.is_readonly = is_readonly;
	}
	public String getIsbr() {
		return isbr;
	}
	public void setIsbr(String isbr) {
		this.isbr = isbr;
	}
	public String getP_code_id() {
		return p_code_id;
	}
	public void setP_code_id(String p_code_id) {
		this.p_code_id = p_code_id;
	}
	public String getZ5_table() {
		return z5_table;
	}
	public void setZ5_table(String z5_table) {
		this.z5_table = z5_table;
	}
	public String getZ5_key() {
		return z5_key;
	}
	public void setZ5_key(String z5_key) {
		this.z5_key = z5_key;
	}
	public String getZ5_value() {
		return z5_value;
	}
	public void setZ5_value(String z5_value) {
		this.z5_value = z5_value;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getArrangement_direction() {
		return arrangement_direction;
	}
	public void setArrangement_direction(String arrangement_direction) {
		this.arrangement_direction = arrangement_direction;
	}

}
