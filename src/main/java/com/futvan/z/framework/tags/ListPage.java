package com.futvan.z.framework.tags;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.beanutils.BeanUtils;

import com.alibaba.fastjson.JSONArray;
import com.futvan.z.framework.common.bean.QueryTerms;
import com.futvan.z.framework.common.bean.z_form_table;
import com.futvan.z.framework.common.bean.z_form_table_button;
import com.futvan.z.framework.common.bean.z_form_table_column;
import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.SuperTag;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.StringUtil;

public class ListPage extends SuperTag{
	private List<HashMap<String,String>> list;
	private HashMap<String,String> parameter;
	private String userid;

	@Override
	public int doEndTag() throws JspException {
		
		StringBuffer out_html = new StringBuffer();

		try {
			z_form_table table = z.tables.get(parameter.get("tableId"));
			List<z_form_table_column> columnList = table.getColumns();
			List<z_form_table_button> buttonList = table.getButtons();
			
			
			
			//创建功能代码
			String pageType = "list";
			if("1".equals(table.getList_display_style())) {
				pageType = "list_card";
			}
			CreateFunctionHtml(buttonList,out_html,pageType,userid,table);
			
			
			//创建查询域名
			CreateSelectHtml("list",parameter,out_html,table,columnList);
			
			//创建表格代码
			out_html.append("<div data-options=\"region:'center',border:false\">").append("\r\n");
			
			
			if("1".equals(table.getList_display_style())) {
				//列表显示为卡片组
				CreateCardGroup(out_html,columnList,parameter,table,userid,list);
			}else {
				//列表显示为数据表格
				CreateTableHtml(out_html,columnList,list,parameter);
			}
			out_html.append("</div>").append("\r\n");
			
			//创建分页代码
			out_html.append("<div data-options=\"region:'south',border:false\" style=\"height:40px;overflow:hidden\">").append("\r\n");
			CreatePagingHtml("list",parameter,out_html);
			out_html.append("</div>").append("\r\n");
			
			
			//创建JS方法
			CreateJS(out_html,pageType,table,parameter,userid);
			
			
		} catch (Exception e) {
			throw new JspException("ListPage:自定义标签构建错误："+e.getMessage());
		}

		//输出HTML
		outHTML(out_html.toString());
		return super.doEndTag();
	}


	public List<HashMap<String, String>> getList() {
		return list;
	}

	public void setList(List<HashMap<String, String>> list) {
		this.list = list;
	}

	public HashMap<String, String> getParameter() {
		return parameter;
	}

	public void setParameter(HashMap<String, String> parameter) {
		this.parameter = parameter;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}

}
