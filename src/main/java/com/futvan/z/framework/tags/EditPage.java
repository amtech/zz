package com.futvan.z.framework.tags;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspException;

import org.apache.commons.beanutils.BeanUtils;

import com.futvan.z.system.zcode.z_code;
import com.futvan.z.system.zcode.z_code_detail;
import com.futvan.z.framework.common.bean.z_form_table;
import com.futvan.z.framework.common.bean.z_form_table_button;
import com.futvan.z.framework.common.bean.z_form_table_column;
import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.SuperTag;
import com.futvan.z.framework.core.SuperZ;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.DateUtil;
import com.futvan.z.framework.util.StringUtil;
import com.futvan.z.framework.util.SystemUtil;

public class EditPage extends SuperTag {
	private String userid;//用户ID
	private String pageType;//页面类型
	private HashMap<String,String> parameter;//所有参数
	private Map<String, List<HashMap>> detail;//所有子表记录
	@Override
	public int doEndTag() throws JspException {
		StringBuffer out_html = new StringBuffer();
		try {
			//获取表对象
			z_form_table table = z.tables.get(parameter.get("tableId"));
			//创建按钮
			CreateFunctionHtml(table.getButtons(),out_html,pageType,userid,table);
			//创建表单
			CreateForm(out_html,table,pageType,parameter);
			//如果有下级表，创建下级表列表
			CreateDetailList(detail,out_html,table,pageType);
			//创建JS方法
			CreateJS(out_html,pageType,table,parameter,userid);
		} catch (Exception e) {
			throw new JspException("EditPage:自定义标签构建错误："+e.getMessage());
		}
		//输出HTML
		outHTML(out_html.toString());
		return super.doEndTag();
	}
	/**
	 * @return userid
	 */
	public String getUserid() {
		return userid;
	}
	/**
	 * @param userid 要设置的 userid
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	/**
	 * @return pageType
	 */
	public String getPageType() {
		return pageType;
	}
	/**
	 * @param pageType 要设置的 pageType
	 */
	public void setPageType(String pageType) {
		this.pageType = pageType;
	}
	/**
	 * @return parameter
	 */
	public HashMap<String, String> getParameter() {
		return parameter;
	}
	/**
	 * @param parameter 要设置的 parameter
	 */
	public void setParameter(HashMap<String, String> parameter) {
		this.parameter = parameter;
	}
	/**
	 * @return detail
	 */
	public Map<String, List<HashMap>> getDetail() {
		return detail;
	}
	/**
	 * @param detail 要设置的 detail
	 */
	public void setDetail(Map<String, List<HashMap>> detail) {
		this.detail = detail;
	}
	
	
	
}
