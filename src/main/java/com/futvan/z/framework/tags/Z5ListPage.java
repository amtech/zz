package com.futvan.z.framework.tags;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.beanutils.BeanUtils;

import com.alibaba.fastjson.JSONArray;
import com.futvan.z.framework.common.bean.QueryTerms;
import com.futvan.z.framework.common.bean.z_form_table;
import com.futvan.z.framework.common.bean.z_form_table_column;
import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.SuperTag;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.StringUtil;
/**
 * Z5List页面
 * @author zz
 * @CreateDate 2018-10-22
 */
public class Z5ListPage<E> extends SuperTag{
	private List<HashMap<String,String>> list;
	private HashMap<String,String> parameter;

	@Override
	public int doEndTag() throws JspException {

		StringBuffer out_html = new StringBuffer();

		try {
			z_form_table table = z.tables.get(parameter.get("tableId"));
			List<z_form_table_column> columnList = table.getColumns();

			//创建功能代码
			CreateFunctionHtml(out_html);

			//创建查询域名
			CreateSelectHtml("Z5list",parameter,out_html,table,columnList);

			//创建表格代码
			out_html.append("<div data-options=\"region:'center',border:false\">").append("\r\n");
			CreateTableHtml(out_html,columnList,list,parameter);
			out_html.append("</div>").append("\r\n");

			//创建分页代码
			out_html.append("<div data-options=\"region:'south',border:false\" style=\"height:40px;overflow:hidden\">").append("\r\n");
			CreatePagingHtml("Z5list",parameter,out_html);
			out_html.append("</div>").append("\r\n");
			

		} catch (Exception e) {
			throw new JspException("ListPage:自定义标签构建错误："+e.getMessage());
		}

		//输出HTML
		outHTML(out_html.toString());
		return super.doEndTag();
	}

	/**
	 * 创建功能区代码
	 * @param out_html
	 */
	private void CreateFunctionHtml(StringBuffer out_html) {
		out_html.append("<div data-options=\"region:'north',border:false\"  >").append("\r\n");
		
		out_html.append("	<div class=\"row\" style=\"padding: 5px;margin:0px;\">").append("\r\n");
		//添加选择按钮
		out_html.append("		<div class=\"col-md-3\">").append("\r\n");
		out_html.append("			<div class=\"btn-group btn-group-sm\" role=\"group\">").append("\r\n");
		out_html.append("				<button id=\"select_button\" type=\"button\" class=\"btn btn-light\" onclick=\"getReturnValue();\"><i class=\"fa fa-get-pocket\"></i> 选择</button>").append("\r\n");
		out_html.append("			</div>").append("\r\n");
		out_html.append("		</div>").append("\r\n");
		//添加查询框
		out_html.append("		<div class=\"col-md-6\">").append("\r\n");
		out_html.append("			<div class=\"input-group input-group-sm\">").append("\r\n");
		out_html.append("				<input id=\"Z5QueryParametersInput\" value=\"\" type=\"text\" class=\"form-control form-control-sm\" placeholder=\"查询条件\" onkeydown=\"Z5QueryOnkeydown();\" />").append("\r\n");
		out_html.append("				<span class=\"input-group-btn\">").append("\r\n");
		out_html.append("					<button class=\"btn btn-light\" onclick=\"Z5Query()\" type=\"button\"><i class=\"fa fa-search\"> 查询</i></button>").append("\r\n");
		out_html.append("				</span>").append("\r\n");
		out_html.append("			</div>").append("\r\n");
		out_html.append("		</div>").append("\r\n");
		out_html.append("	</div>").append("\r\n");
		out_html.append("</div>").append("\r\n");
	}
	public List<HashMap<String, String>> getList() {
		return list;
	}

	public void setList(List<HashMap<String, String>> list) {
		this.list = list;
	}

	public HashMap<String, String> getParameter() {
		return parameter;
	}

	public void setParameter(HashMap<String, String> parameter) {
		this.parameter = parameter;
	}

}
