package com.futvan.z.framework.common.bean;
import com.futvan.z.framework.core.SuperBean;
public class z_form extends SuperBean{
	private String project_id;
	private String form_id;
	private String form_name;
	private String isolation_mode;
	public String getProject_id() {
		return project_id;
	}
	public void setProject_id(String project_id) {
		this.project_id = project_id;
	}
	public String getForm_id() {
		return form_id;
	}
	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}
	public String getForm_name() {
		return form_name;
	}
	public void setForm_name(String form_name) {
		this.form_name = form_name;
	}
	public String getIsolation_mode() {
		return isolation_mode;
	}
	public void setIsolation_mode(String isolation_mode) {
		this.isolation_mode = isolation_mode;
	}
}
