package com.futvan.z.framework.common.bean;

import com.futvan.z.framework.core.SuperBean;

public class z_form_table_button extends SuperBean{
	private String page_type;//页面类型
	private String js_onclick;//JS方法
	private String button_icon;//按钮图标
	private String button_name;//按钮名称
	private String button_id;//按钮ID
	private String is_hidden;//按钮是否隐藏
	/**
	 * @return page_type
	 */
	public String getPage_type() {
		return page_type;
	}
	/**
	 * @param page_type 要设置的 page_type
	 */
	public void setPage_type(String page_type) {
		this.page_type = page_type;
	}
	/**
	 * @return js_onclick
	 */
	public String getJs_onclick() {
		return js_onclick;
	}
	/**
	 * @param js_onclick 要设置的 js_onclick
	 */
	public void setJs_onclick(String js_onclick) {
		this.js_onclick = js_onclick;
	}
	/**
	 * @return button_icon
	 */
	public String getButton_icon() {
		return button_icon;
	}
	/**
	 * @param button_icon 要设置的 button_icon
	 */
	public void setButton_icon(String button_icon) {
		this.button_icon = button_icon;
	}
	/**
	 * @return button_name
	 */
	public String getButton_name() {
		return button_name;
	}
	/**
	 * @param button_name 要设置的 button_name
	 */
	public void setButton_name(String button_name) {
		this.button_name = button_name;
	}
	/**
	 * @return button_id
	 */
	public String getButton_id() {
		return button_id;
	}
	/**
	 * @param button_id 要设置的 button_id
	 */
	public void setButton_id(String button_id) {
		this.button_id = button_id;
	}
	/**
	 * @return is_hidden
	 */
	public String getIs_hidden() {
		return is_hidden;
	}
	/**
	 * @param is_hidden 要设置的 is_hidden
	 */
	public void setIs_hidden(String is_hidden) {
		this.is_hidden = is_hidden;
	}
}
