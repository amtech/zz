package com.futvan.z.framework.common.bean;

import com.futvan.z.framework.core.SuperBean;

public class z_form_table_column extends SuperBean{
	private String column_id;
	private String column_name;
	private String column_type;
	private String column_length_db;
	private String textarea_height;
	private String column_size;
	private String colunm_length_list;
	private String column_default;
	private String column_default_type;
	private String compare;
	private String is_null;
	private String is_hidden;
	private String is_hidden_list;
	private String is_readonly;
	private String arrangement_direction;
	private String p_code_id;
	private String z5_table;
	private String z5_key;
	private String z5_value;
	private String z5_value2;
	private String z5_sql;
	private String z5_page_display;
	private String mode_type;
	private String column_help;
	private String isbr;
	private String ishr;
	private String tr_href;
	public String getColumn_id() {
		return column_id;
	}
	public void setColumn_id(String column_id) {
		this.column_id = column_id;
	}
	public String getColumn_name() {
		return column_name;
	}
	public void setColumn_name(String column_name) {
		this.column_name = column_name;
	}
	public String getColumn_type() {
		return column_type;
	}
	public void setColumn_type(String column_type) {
		this.column_type = column_type;
	}
	public String getColumn_length_db() {
		return column_length_db;
	}
	public void setColumn_length_db(String column_length_db) {
		this.column_length_db = column_length_db;
	}
	public String getTextarea_height() {
		return textarea_height;
	}
	public void setTextarea_height(String textarea_height) {
		this.textarea_height = textarea_height;
	}
	public String getColumn_size() {
		return column_size;
	}
	public void setColumn_size(String column_size) {
		this.column_size = column_size;
	}
	public String getColunm_length_list() {
		return colunm_length_list;
	}
	public void setColunm_length_list(String colunm_length_list) {
		this.colunm_length_list = colunm_length_list;
	}
	public String getColumn_default() {
		return column_default;
	}
	public void setColumn_default(String column_default) {
		this.column_default = column_default;
	}
	public String getColumn_default_type() {
		return column_default_type;
	}
	public void setColumn_default_type(String column_default_type) {
		this.column_default_type = column_default_type;
	}
	public String getCompare() {
		return compare;
	}
	public void setCompare(String compare) {
		this.compare = compare;
	}
	public String getIs_null() {
		return is_null;
	}
	public void setIs_null(String is_null) {
		this.is_null = is_null;
	}
	public String getIs_hidden() {
		return is_hidden;
	}
	public void setIs_hidden(String is_hidden) {
		this.is_hidden = is_hidden;
	}
	public String getIs_readonly() {
		return is_readonly;
	}
	public void setIs_readonly(String is_readonly) {
		this.is_readonly = is_readonly;
	}
	public String getP_code_id() {
		return p_code_id;
	}
	public void setP_code_id(String p_code_id) {
		this.p_code_id = p_code_id;
	}
	public String getZ5_table() {
		return z5_table;
	}
	public void setZ5_table(String z5_table) {
		this.z5_table = z5_table;
	}
	public String getZ5_key() {
		return z5_key;
	}
	public void setZ5_key(String z5_key) {
		this.z5_key = z5_key;
	}
	public String getZ5_value() {
		return z5_value;
	}
	public void setZ5_value(String z5_value) {
		this.z5_value = z5_value;
	}
	public String getZ5_value2() {
		return z5_value2;
	}
	public void setZ5_value2(String z5_value2) {
		this.z5_value2 = z5_value2;
	}
	public String getZ5_sql() {
		return z5_sql;
	}
	public void setZ5_sql(String z5_sql) {
		this.z5_sql = z5_sql;
	}
	public String getZ5_page_display() {
		return z5_page_display;
	}
	public void setZ5_page_display(String z5_page_display) {
		this.z5_page_display = z5_page_display;
	}
	public String getIs_hidden_list() {
		return is_hidden_list;
	}
	public void setIs_hidden_list(String is_hidden_list) {
		this.is_hidden_list = is_hidden_list;
	}
	public String getMode_type() {
		return mode_type;
	}
	public void setMode_type(String mode_type) {
		this.mode_type = mode_type;
	}
	public String getColumn_help() {
		if(column_help==null || "".equals(column_help) || "null".equals(column_help) || "NULL".equals(column_help) || "Null".equals(column_help) || "NuLL".equals(column_help)) {
			return "";
		}else {
			return column_help;
		}
	}
	public void setColumn_help(String column_help) {
		this.column_help = column_help;
	}
	public String getIsbr() {
		return isbr;
	}
	public void setIsbr(String isbr) {
		this.isbr = isbr;
	}
	public String getTr_href() {
		return tr_href;
	}
	public void setTr_href(String tr_href) {
		this.tr_href = tr_href;
	}
	public String getIshr() {
		return ishr;
	}
	public void setIshr(String ishr) {
		this.ishr = ishr;
	}
	public String getArrangement_direction() {
		return arrangement_direction;
	}
	public void setArrangement_direction(String arrangement_direction) {
		this.arrangement_direction = arrangement_direction;
	}
	
}
