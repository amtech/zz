package com.futvan.z.framework.common.bean;

import java.util.List;

import com.futvan.z.framework.core.SuperBean;

public class z_form_table extends SuperBean{
	private String table_id;
	private String table_title;
	private String parent_table_id;
	private List<z_form_table_column> columns;
	private List<z_form_table_button> buttons;
	private List<z_form_table> detailTable;
	private String list_dblclick;//双击行执行动作
	private String list_action;//列表加载数据方法
	private String list_display_style;//列表显示形式
	private String card_size;//卡片规格
	private String default_rowcount;//列表默认显示行数
	private String orderby;//排序条件
	private String orderby_pattern;//排序模式
	private String edit_page_layout;//编辑页面布局 0 上下结构 1 左右结构
	private String is_collapsed;//子表是否默认收缩状态
	private String list_body_js;
	private String edit_body_js;
	private String list_ready_js;
	private String edit_ready_js;
	/**
	 * @return table_id
	 */
	public String getTable_id() {
		return table_id;
	}
	/**
	 * @param table_id 要设置的 table_id
	 */
	public void setTable_id(String table_id) {
		this.table_id = table_id;
	}
	/**
	 * @return table_title
	 */
	public String getTable_title() {
		return table_title;
	}
	/**
	 * @param table_title 要设置的 table_title
	 */
	public void setTable_title(String table_title) {
		this.table_title = table_title;
	}
	/**
	 * @return parent_table_id
	 */
	public String getParent_table_id() {
		return parent_table_id;
	}
	/**
	 * @param parent_table_id 要设置的 parent_table_id
	 */
	public void setParent_table_id(String parent_table_id) {
		this.parent_table_id = parent_table_id;
	}
	/**
	 * @return columns
	 */
	public List<z_form_table_column> getColumns() {
		return columns;
	}
	/**
	 * @param columns 要设置的 columns
	 */
	public void setColumns(List<z_form_table_column> columns) {
		this.columns = columns;
	}
	/**
	 * @return buttons
	 */
	public List<z_form_table_button> getButtons() {
		return buttons;
	}
	/**
	 * @param buttons 要设置的 buttons
	 */
	public void setButtons(List<z_form_table_button> buttons) {
		this.buttons = buttons;
	}
	/**
	 * @return detailTable
	 */
	public List<z_form_table> getDetailTable() {
		return detailTable;
	}
	/**
	 * @param detailTable 要设置的 detailTable
	 */
	public void setDetailTable(List<z_form_table> detailTable) {
		this.detailTable = detailTable;
	}
	/**
	 * @return list_action
	 */
	public String getList_action() {
		return list_action;
	}
	/**
	 * @param list_action 要设置的 list_action
	 */
	public void setList_action(String list_action) {
		this.list_action = list_action;
	}
	/**
	 * @return orderby
	 */
	public String getOrderby() {
		return orderby;
	}
	/**
	 * @param orderby 要设置的 orderby
	 */
	public void setOrderby(String orderby) {
		this.orderby = orderby;
	}
	/**
	 * @return orderby_pattern
	 */
	public String getOrderby_pattern() {
		return orderby_pattern;
	}
	/**
	 * @param orderby_pattern 要设置的 orderby_pattern
	 */
	public void setOrderby_pattern(String orderby_pattern) {
		this.orderby_pattern = orderby_pattern;
	}
	/**
	 * @return edit_page_layout
	 */
	public String getEdit_page_layout() {
		return edit_page_layout;
	}
	/**
	 * @param edit_page_layout 要设置的 edit_page_layout
	 */
	public void setEdit_page_layout(String edit_page_layout) {
		this.edit_page_layout = edit_page_layout;
	}
	/**
	 * @return is_collapsed
	 */
	public String getIs_collapsed() {
		return is_collapsed;
	}
	/**
	 * @param is_collapsed 要设置的 is_collapsed
	 */
	public void setIs_collapsed(String is_collapsed) {
		this.is_collapsed = is_collapsed;
	}
	/**
	 * @return list_body_js
	 */
	public String getList_body_js() {
		return list_body_js;
	}
	/**
	 * @param list_body_js 要设置的 list_body_js
	 */
	public void setList_body_js(String list_body_js) {
		this.list_body_js = list_body_js;
	}
	/**
	 * @return edit_body_js
	 */
	public String getEdit_body_js() {
		return edit_body_js;
	}
	/**
	 * @param edit_body_js 要设置的 edit_body_js
	 */
	public void setEdit_body_js(String edit_body_js) {
		this.edit_body_js = edit_body_js;
	}
	/**
	 * @return list_ready_js
	 */
	public String getList_ready_js() {
		return list_ready_js;
	}
	/**
	 * @param list_ready_js 要设置的 list_ready_js
	 */
	public void setList_ready_js(String list_ready_js) {
		this.list_ready_js = list_ready_js;
	}
	/**
	 * @return edit_ready_js
	 */
	public String getEdit_ready_js() {
		return edit_ready_js;
	}
	/**
	 * @param edit_ready_js 要设置的 edit_ready_js
	 */
	public void setEdit_ready_js(String edit_ready_js) {
		this.edit_ready_js = edit_ready_js;
	}
	/**
	 * @return default_rowcount
	 */
	public String getDefault_rowcount() {
		return default_rowcount;
	}
	/**
	 * @param default_rowcount 要设置的 default_rowcount
	 */
	public void setDefault_rowcount(String default_rowcount) {
		this.default_rowcount = default_rowcount;
	}
	/**
	 * @return list_dblclick
	 */
	public String getList_dblclick() {
		return list_dblclick;
	}
	/**
	 * @param list_dblclick 要设置的 list_dblclick
	 */
	public void setList_dblclick(String list_dblclick) {
		this.list_dblclick = list_dblclick;
	}
	/**
	 * @return list_display_style
	 */
	public String getList_display_style() {
		return list_display_style;
	}
	/**
	 * @param list_display_style 要设置的 list_display_style
	 */
	public void setList_display_style(String list_display_style) {
		this.list_display_style = list_display_style;
	}
	/**
	 * @return card_size
	 */
	public String getCard_size() {
		return card_size;
	}
	/**
	 * @param card_size 要设置的 card_size
	 */
	public void setCard_size(String card_size) {
		this.card_size = card_size;
	}
	
}
