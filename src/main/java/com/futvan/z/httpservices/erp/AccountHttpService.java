package com.futvan.z.httpservices.erp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.futvan.z.erp.erp_account.Erp_accountService;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
@Controller
public class AccountHttpService extends SuperAction{
	
	@Autowired
	private Erp_accountService erp_accountService;
	
	/**
	 * 进账
	 * @param userid 客户ID
	 * @param a_type 账户类型
	 * @param amount 进账数量
	 * @param mode 操作模式
	 * @param bizid 业务ID
	 * @return Result.date 明细账ID
	 * @throws Exception
	 */
	@RequestMapping(value="/acc_add")
	public @ResponseBody Result acc_add(String userid,String a_type,String amount,String mode,String bizid) throws Exception {
		return erp_accountService.Add(userid, a_type, amount, mode, bizid);
	}
	
	/**
	 * 出账
	 * @param userid 客户ID
	 * @param a_type 账户类型
	 * @param amount 进账数量
	 * @param mode 操作模式
	 * @param bizid 业务ID
	 * @return Result.date 明细账ID
	 * @throws Exception
	 */
	@RequestMapping(value="/acc_sub")
	public @ResponseBody Result acc_sub(String userid,String a_type,String amount,String mode,String bizid) throws Exception {
		return erp_accountService.Subtract(userid, a_type, amount, mode, bizid);
	}
	
	/**
	 * 解除冻结
	 * @param adid 明细账ID
	 * @return Result.date 明细账ID
	 * @throws Exception
	 */
	@RequestMapping(value="/acc_unfrozen")
	public @ResponseBody Result acc_unfrozen(String adid) throws Exception {
		return erp_accountService.Unfrozen(adid);
	}
	
	/**
	 * 删除明细账
	 * @param adid 明细账ID
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/acc_remove")
	public @ResponseBody Result acc_remove(String adid) throws Exception {
		return erp_accountService.Remove(adid);
	}
}
