package com.futvan.z.httpservices.test;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.core.z;
import org.springframework.web.bind.annotation.RequestParam;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.HashMap;
import java.util.List;
/**
 * 根据客户ID获取客户被拜访记录
 */
@Controller
public class Get_test_visit_form_list_for_idHttpService extends SuperAction{
 
	@RequestMapping(value="/get_test_visit_form_list_for_id")
	public @ResponseBody Result Service(@RequestParam HashMap<String, String> bean) {
		Result result = new Result();
 		Result authority = z.isServiceAuthority(bean,request);
 		if(Code.SUCCESS.equals(authority.getCode())){
 			//获取客户ID
 			String customer_company = bean.get("customer_company");
 			if(z.isNotNull(customer_company)) {
 				//定义SQL
 				StringBuffer sql = new StringBuffer();
 				sql.append("SELECT ");
 				sql.append("  c.customer_company, ");
 				sql.append("  c.customer_name, ");
 				sql.append("  c.customer_tel, ");
 				sql.append("  u.user_name, ");
 				sql.append("  d.d_time ");
 				sql.append("FROM test_visit_form c ");
 				sql.append("INNER JOIN test_visit_form_times d  ON c.zid = d.pid ");
 				sql.append("LEFT JOIN z_user u ON d.d_user = u.zid ");
 				sql.append("WHERE c.customer_company = '"+customer_company+"' ");
 				
 				//获取数据库连接session  mysql001为数据库标识
 				SqlSessionTemplate mysql001_db = z.dbs.get("mysql001");
 				
 				//根据SQL查询数据
 				List<HashMap<String,String>> list = mysql001_db.selectList("select", sql);
 				
 				//返回数据给访问者
 				result.setCode(Code.SUCCESS);//设置返回状态
 				result.setMsg("ok");//设置返回信息
 				result.setData(list);//设置返回数据
 			}else {
 				result.setCode(Code.ERROR);
 				result.setMsg("客户ID不可以为空   |  custom_id is not null");
 			}
 		}else {
 			result = authority;
 		}
		return result;
	}
}
