package com.futvan.z.httpservices.system;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.WeiXinUtil;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.HashMap;
/**
 * 获取微信用户OpenId与session_key
 */
@Controller
public class Jscode2sessionHttpService extends SuperAction{
 
	@RequestMapping(value="/jscode2session")
	public @ResponseBody Result Service(@RequestParam HashMap<String, String> bean) {
		Result result = new Result();
 		Result authority = z.isServiceAuthority(bean,request);
 		if(Code.SUCCESS.equals(authority.getCode())){
 			//从系统参数中获取微信小程序APPID与SECRET
 			String appid = z.sp.get("wechat_applet_appid");
 			String secret = z.sp.get("wechat_applet_secret");
			String js_code = bean.get("js_code");
			result = WeiXinUtil.jscode2session(appid, secret, js_code);
 		}else {
 			result = authority;
 		}
		return result;
	}
}
