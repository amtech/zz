package com.futvan.z.httpservices.system;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.WeiXinUtil;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.HashMap;
/**
 * 解析微信encryptedData获取手机号
 */
@Controller
public class Wx_getUserPhoneNumberHttpService extends SuperAction{
 
	@RequestMapping(value="/wx_getUserPhoneNumber")
	public @ResponseBody Result Service(@RequestParam HashMap<String, String> bean) {
		Result result = new Result();
 		Result authority = z.isServiceAuthority(bean,request);
 		if(Code.SUCCESS.equals(authority.getCode())){
 			String session_key = bean.get("session_key");
			String encryptedData = bean.get("encryptedData");
			String iv = bean.get("iv");
			result = WeiXinUtil.getUserPhoneNumber(session_key, encryptedData, iv);
 		}else {
 			result = authority;
 		}
		return result;
	}
}
