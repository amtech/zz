package com.futvan.z.httpservices.system;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.SpringUtil;
import com.futvan.z.system.zuser.z_user;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.HashMap;
/**
 * 微信用户验证openid登录
 */
@Controller
public class UserLoginForOpenidHttpService extends SuperAction{
 
	@RequestMapping(value="/userLoginForOpenid")
	public @ResponseBody Result Service(@RequestParam HashMap<String, String> bean) {
		Result result = new Result();
 		Result authority = z.isServiceAuthority(bean,request);
 		if(Code.SUCCESS.equals(authority.getCode())){
 			String openid = bean.get("openid");
			if(z.isNotNull(openid)) {
				z_user user_query = new z_user();
				user_query.setOpenid(openid);
				user_query.setIs_start("1");
				z_user user = SpringUtil.getSqlSession().selectOne("z_user_select", user_query);
				if(z.isNotNull(user)) {
					result.setCode(Code.SUCCESS);
					result.setMsg("ok");
					result.setData(user);
				}else {
					result.setCode(Code.ERROR);
					result.setMsg("登录验证失败，未找到用户账号");
				}
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("openid is null");
			}
 		}else {
 			result = authority;
 		}
		return result;
	}
}
