package com.futvan.z.httpservices.system;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.SpringUtil;
import com.futvan.z.framework.util.WeiXinUtil;
import com.futvan.z.system.zuser.z_user;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.HashMap;
/**
 * 用户绑定微信OpenId
 */
@Controller
public class UserBindingWeiXinOpenIdHttpService extends SuperAction{
 
	@RequestMapping(value="/userBindingWeiXinOpenId")
	public @ResponseBody Result Service(@RequestParam HashMap<String, String> bean) {
		Result result = new Result();
 		Result authority = z.isServiceAuthority(bean,request);
 		if(Code.SUCCESS.equals(authority.getCode())){
 			String session_key = bean.get("session_key");
			String encryptedData = bean.get("encryptedData");
			String iv = bean.get("iv");
			String openid = bean.get("openid");
			Result r = WeiXinUtil.getUserPhoneNumber(session_key, encryptedData, iv);
			if(Code.SUCCESS.equals(r.getCode())) {
				//获取手机号
				String phone = String.valueOf(r.getData());
				if(z.isNotNull(openid)) {
					//根据Openid查询用户是否存在，如果存在直接返回
					z_user user_query = new z_user();
					user_query.setOpenid(openid);
					user_query.setIs_start("1");
					z_user user = SpringUtil.getSqlSession().selectOne("z_user_select", user_query);
					if(z.isNotNull(user)) {
						result.setCode(Code.SUCCESS);
						result.setMsg("ok");
						result.setData(user);
					}else {
						//如果未找到用户，根据手机号判读是否存在
						user_query = new z_user();
						user_query.setTel(phone);
						user_query.setIs_start("1");
						user = SpringUtil.getSqlSession().selectOne("z_user_select", user_query);
						if(z.isNotNull(user)) {
							
							//判读该用户是否有绑定过openid
							if(z.isNotNull(user.getOpenid())) {
								//有openid,判读新老openid如果不同，用新openid更新旧的openid
								if(!openid.equals(user.getOpenid())) {
									//更新旧的openid
									z_user z_user_update_zid_parameter = new z_user();
									z_user_update_zid_parameter.setZid(user.getZid());
									z_user_update_zid_parameter.setOpenid(openid);
									SpringUtil.getSqlSession().update("z_user_update_zid", z_user_update_zid_parameter);
								}
								
								//返回用户
								user.setOpenid(openid);
								result.setCode(Code.SUCCESS);
								result.setMsg("ok");
								result.setData(user);
								
							}else {
								//无openid,直接绑定该openid到该用上。
								z_user z_user_update_zid_parameter = new z_user();
								z_user_update_zid_parameter.setZid(user.getZid());
								z_user_update_zid_parameter.setOpenid(openid);
								SpringUtil.getSqlSession().update("z_user_update_zid", z_user_update_zid_parameter);
								
								//返回该用户
								user.setOpenid(openid);
								result.setCode(Code.SUCCESS);
								result.setMsg("ok");
								result.setData(user);
							}
						}else {
							result.setCode(Code.ERROR);
							result.setMsg("手机号["+phone+"]无此用户");
						}
					}
				}else {
					result.setCode(Code.ERROR);
					result.setMsg("openid is null");
				}
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("根据encryptedData解析手机号出错|"+r.getMsg());
			}
 		}else {
 			result = authority;
 		}
		return result;
	}
}
