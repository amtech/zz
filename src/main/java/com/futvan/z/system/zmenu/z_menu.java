package com.futvan.z.system.zmenu;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_menu extends SuperBean{
	//URL
	private String url;

	//上级菜单
	private String parentid;

	//是否隐藏
	private String is_hidden;

	//是否展开
	private String isopen;

	//菜单类型
	private String menu_type;

	//所属功能
	private String formid;

	//所属报表
	private String reportid;

	//关联功能
	private List<z_menu_form_button> z_menu_form_button_list;

	//报表按钮
	private List<z_menu_report_button> z_menu_report_button_list;

	/**
	* getURL
	* @return url
	*/
	public String getUrl() {
		return url;
  	}

	/**
	* setURL
	* @return url
	*/
	public void setUrl(String url) {
		this.url = url;
 	}

	/**
	* get上级菜单
	* @return parentid
	*/
	public String getParentid() {
		return parentid;
  	}

	/**
	* set上级菜单
	* @return parentid
	*/
	public void setParentid(String parentid) {
		this.parentid = parentid;
 	}

	/**
	* get是否隐藏
	* @return is_hidden
	*/
	public String getIs_hidden() {
		return is_hidden;
  	}

	/**
	* set是否隐藏
	* @return is_hidden
	*/
	public void setIs_hidden(String is_hidden) {
		this.is_hidden = is_hidden;
 	}

	/**
	* get是否展开
	* @return isopen
	*/
	public String getIsopen() {
		return isopen;
  	}

	/**
	* set是否展开
	* @return isopen
	*/
	public void setIsopen(String isopen) {
		this.isopen = isopen;
 	}

	/**
	* get菜单类型
	* @return menu_type
	*/
	public String getMenu_type() {
		return menu_type;
  	}

	/**
	* set菜单类型
	* @return menu_type
	*/
	public void setMenu_type(String menu_type) {
		this.menu_type = menu_type;
 	}

	/**
	* get所属功能
	* @return formid
	*/
	public String getFormid() {
		return formid;
  	}

	/**
	* set所属功能
	* @return formid
	*/
	public void setFormid(String formid) {
		this.formid = formid;
 	}

	/**
	* get所属报表
	* @return reportid
	*/
	public String getReportid() {
		return reportid;
  	}

	/**
	* set所属报表
	* @return reportid
	*/
	public void setReportid(String reportid) {
		this.reportid = reportid;
 	}

	/**
	* get关联功能
	* @return 关联功能
	*/
	public List<z_menu_form_button> getZ_menu_form_button_list() {
		return z_menu_form_button_list;
  	}

	/**
	* set关联功能
	* @return 关联功能
	*/
	public void setZ_menu_form_button_list(List<z_menu_form_button> z_menu_form_button_list) {
		this.z_menu_form_button_list = z_menu_form_button_list;
 	}

	/**
	* get报表按钮
	* @return 报表按钮
	*/
	public List<z_menu_report_button> getZ_menu_report_button_list() {
		return z_menu_report_button_list;
  	}

	/**
	* set报表按钮
	* @return 报表按钮
	*/
	public void setZ_menu_report_button_list(List<z_menu_report_button> z_menu_report_button_list) {
		this.z_menu_report_button_list = z_menu_report_button_list;
 	}

}
