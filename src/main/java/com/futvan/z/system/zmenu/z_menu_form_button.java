package com.futvan.z.system.zmenu;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_menu_form_button extends SuperBean{
	//按钮名称
	private String buttonId;

	//所属表
	private String tableid;

	//所属页面
	private String page_type;

	/**
	* get按钮名称
	* @return buttonId
	*/
	public String getButtonId() {
		return buttonId;
  	}

	/**
	* set按钮名称
	* @return buttonId
	*/
	public void setButtonId(String buttonId) {
		this.buttonId = buttonId;
 	}

	/**
	* get所属表
	* @return tableid
	*/
	public String getTableid() {
		return tableid;
  	}

	/**
	* set所属表
	* @return tableid
	*/
	public void setTableid(String tableid) {
		this.tableid = tableid;
 	}

	/**
	* get所属页面
	* @return page_type
	*/
	public String getPage_type() {
		return page_type;
  	}

	/**
	* set所属页面
	* @return page_type
	*/
	public void setPage_type(String page_type) {
		this.page_type = page_type;
 	}

}
