package com.futvan.z.system.zmenu;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.SystemUtil;
@Service
public class ZmenuService extends SuperService{


	/**
	 * 根据FormId添加关联Button
	 * @param pid 主表ID
	 * @param formid 表单ID
	 * @throws Exception
	 */
	public void AddButtons(String pid,String formid) throws Exception{
		
		//获取表单所有功能按钮
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append("  zft.zid tableid,");
		sql.append("  zftb.page_type,");
		sql.append("  zftb.zid buttonId ");
		sql.append("FROM z_form zf ");
		sql.append("INNER JOIN z_form_table zft  ON zf.zid = zft.pid ");
		sql.append("INNER JOIN z_form_table_button zftb  ON zft.zid = zftb.pid ");
		sql.append("WHERE zf.zid = '"+formid+"' ");
		List<Map<String,String>> buttons = selectList(sql.toString());
		
		//插入菜单关联Button表
		for (Map<String, String> buttonMap : buttons) {
			String z_menu_form_button_zid = z.newZid("z_menu_form_button");//明细表ID
			String tableid = buttonMap.get("tableid");//表ID
			String page_type = buttonMap.get("page_type");//页面类型
			String buttonId = buttonMap.get("buttonId");//按钮ZID
			
			StringBuffer insertsql = new StringBuffer();
			insertsql.append("INSERT INTO z_menu_form_button (");
			insertsql.append("  zid,");
			insertsql.append("  pid,");
			insertsql.append("  tableid,");
			insertsql.append("  page_type,");
			insertsql.append("  buttonId");
			insertsql.append(") VALUES(");
			insertsql.append("'"+z_menu_form_button_zid+"',");
			insertsql.append("'"+pid+"',");
			insertsql.append("'"+tableid+"',");
			insertsql.append("'"+page_type+"',");
			insertsql.append("'"+buttonId+"'");
			insertsql.append(") ");
			insert(insertsql.toString());
		}
		
	}

	public List<Map<String,String>> getbuttonsList(String formid,String menuid) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append("  zft.zid tablezid,");
		sql.append("  zft.table_id,");
		sql.append("  zftb.page_type, ");
		sql.append("  zftb.zid buttonzid, ");
		sql.append("  zftb.button_id, ");
		sql.append("  zftb.button_name ");
		sql.append("FROM z_form zf ");
		sql.append("INNER JOIN z_form_table zft  ON zf.zid = zft.pid ");
		sql.append("INNER JOIN z_form_table_button zftb  ON zft.zid = zftb.pid ");
		sql.append("WHERE zf.zid = '"+formid+"' ");
		sql.append("and zftb.zid not in(SELECT zmb.buttonId FROM z_menu_form_button zmb WHERE zmb.pid = '"+menuid+"')");
		sql.append("ORDER BY zft.table_id,zftb.page_type DESC ");
		List<Map<String,String>> buttons = selectList(sql.toString());
		return buttons;
	}
	
	public List<Map<String,String>> getReportButtonsList(String reportid,String menuid) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT zb.zid buttonzid,zb.button_id,zb.button_name ");
		sql.append("  FROM z_report zc  ");
		sql.append("  LEFT JOIN z_report_button zb ON zc.zid = zb.pid ");
		sql.append("  WHERE zc.zid = '"+reportid+"'  and zb.zid NOT IN( SELECT zmb.buttonId FROM z_menu_report_button zmb WHERE zmb.pid = '"+menuid+"'  )");
		List<Map<String,String>> buttons = selectList(sql.toString());
		return buttons;
	}

	public void ImportButtons(String menuid, String buttonIds) {
		String [] buttonArray = buttonIds.split(",");
		for (int i = 0; i < buttonArray.length; i++) {
			//导入
			String z_menu_form_button_zid = z.newZid("z_menu_form_button");//明细表ID
			String buttonId = buttonArray[i];
			StringBuffer insertsql = new StringBuffer();
			insertsql.append("INSERT INTO z_menu_form_button (");
			insertsql.append("  zid,");
			insertsql.append("  pid,");
			insertsql.append("  tableid,");
			insertsql.append("  page_type,");
			insertsql.append("  buttonId");
			insertsql.append(") SELECT ");
			insertsql.append("'"+z_menu_form_button_zid+"',");
			insertsql.append("'"+menuid+"',");
			insertsql.append(" zftb.pid,");
			insertsql.append(" zftb.page_type,");
			insertsql.append(" zftb.zid ");
			insertsql.append(" FROM z_form_table_button zftb  WHERE zftb.zid = '"+buttonId+"'  ");
			insert(insertsql.toString());
		}
	}
	
	public void ImportReportButtons(String menuid, String buttonIds) {
		String [] buttonArray = buttonIds.split(",");
		for (int i = 0; i < buttonArray.length; i++) {
			//导入
			String z_menu_form_button_zid = z.newZid("z_menu_report_button");//明细表ID
			String buttonId = buttonArray[i];
			StringBuffer insertsql = new StringBuffer();
			insertsql.append("INSERT INTO z_menu_report_button (");
			insertsql.append("  zid,");
			insertsql.append("  pid,");
			insertsql.append("  buttonId");
			insertsql.append(") SELECT ");
			insertsql.append("'"+z_menu_form_button_zid+"',");
			insertsql.append("'"+menuid+"',");
			insertsql.append(" zb.zid ");
			insertsql.append(" FROM z_report_button zb WHERE zb.zid = '"+buttonId+"'  ");
			insert(insertsql.toString());
		}
	}
}
