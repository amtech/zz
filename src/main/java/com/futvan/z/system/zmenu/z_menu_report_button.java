package com.futvan.z.system.zmenu;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_menu_report_button extends SuperBean{
	//按钮名称
	private String buttonId;

	/**
	* get按钮名称
	* @return buttonId
	*/
	public String getButtonId() {
		return buttonId;
  	}

	/**
	* set按钮名称
	* @return buttonId
	*/
	public void setButtonId(String buttonId) {
		this.buttonId = buttonId;
 	}

}
