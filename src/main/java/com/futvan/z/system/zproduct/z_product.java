package com.futvan.z.system.zproduct;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_product extends SuperBean{
	//所属品类
	private String type;

	//规格
	private String specifications;

	//重量
	private String weight;

	//单位
	private String unit;

	//零售价
	private String price_retail;

	//成本价格
	private String price_cost;

	//保质期[天]
	private String shelflife;

	//二维码
	private String qrcode;

	//厂家编号
	private String manufacturer_number;

	//模板类型
	private String template_type;

	//花色类型
	private String variety_type;

	//产品描述
	private String info;

	//详细描述
	private String info_html;

	/**
	* get所属品类
	* @return type
	*/
	public String getType() {
		return type;
  	}

	/**
	* set所属品类
	* @return type
	*/
	public void setType(String type) {
		this.type = type;
 	}

	/**
	* get规格
	* @return specifications
	*/
	public String getSpecifications() {
		return specifications;
  	}

	/**
	* set规格
	* @return specifications
	*/
	public void setSpecifications(String specifications) {
		this.specifications = specifications;
 	}

	/**
	* get重量
	* @return weight
	*/
	public String getWeight() {
		return weight;
  	}

	/**
	* set重量
	* @return weight
	*/
	public void setWeight(String weight) {
		this.weight = weight;
 	}

	/**
	* get单位
	* @return unit
	*/
	public String getUnit() {
		return unit;
  	}

	/**
	* set单位
	* @return unit
	*/
	public void setUnit(String unit) {
		this.unit = unit;
 	}

	/**
	* get零售价
	* @return price_retail
	*/
	public String getPrice_retail() {
		return price_retail;
  	}

	/**
	* set零售价
	* @return price_retail
	*/
	public void setPrice_retail(String price_retail) {
		this.price_retail = price_retail;
 	}

	/**
	* get成本价格
	* @return price_cost
	*/
	public String getPrice_cost() {
		return price_cost;
  	}

	/**
	* set成本价格
	* @return price_cost
	*/
	public void setPrice_cost(String price_cost) {
		this.price_cost = price_cost;
 	}

	/**
	* get保质期[天]
	* @return shelflife
	*/
	public String getShelflife() {
		return shelflife;
  	}

	/**
	* set保质期[天]
	* @return shelflife
	*/
	public void setShelflife(String shelflife) {
		this.shelflife = shelflife;
 	}

	/**
	* get二维码
	* @return qrcode
	*/
	public String getQrcode() {
		return qrcode;
  	}

	/**
	* set二维码
	* @return qrcode
	*/
	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
 	}

	/**
	* get厂家编号
	* @return manufacturer_number
	*/
	public String getManufacturer_number() {
		return manufacturer_number;
  	}

	/**
	* set厂家编号
	* @return manufacturer_number
	*/
	public void setManufacturer_number(String manufacturer_number) {
		this.manufacturer_number = manufacturer_number;
 	}

	/**
	* get模板类型
	* @return template_type
	*/
	public String getTemplate_type() {
		return template_type;
  	}

	/**
	* set模板类型
	* @return template_type
	*/
	public void setTemplate_type(String template_type) {
		this.template_type = template_type;
 	}

	/**
	* get花色类型
	* @return variety_type
	*/
	public String getVariety_type() {
		return variety_type;
  	}

	/**
	* set花色类型
	* @return variety_type
	*/
	public void setVariety_type(String variety_type) {
		this.variety_type = variety_type;
 	}

	/**
	* get产品描述
	* @return info
	*/
	public String getInfo() {
		return info;
  	}

	/**
	* set产品描述
	* @return info
	*/
	public void setInfo(String info) {
		this.info = info;
 	}

	/**
	* get详细描述
	* @return info_html
	*/
	public String getInfo_html() {
		return info_html;
  	}

	/**
	* set详细描述
	* @return info_html
	*/
	public void setInfo_html(String info_html) {
		this.info_html = info_html;
 	}

}
