package com.futvan.z.system.zform;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.bean.z_form_table;
import com.futvan.z.framework.common.service.CommonService;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.FileUtil;
import com.futvan.z.framework.util.StringUtil;
@Controller
public class ZformAction extends SuperAction{
	@Autowired
	private ZformService zformService;

	/**
	 * 创建自定义按钮java代码
	 * @param tableId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/CreateButtonJavaCode")
	public @ResponseBody Result CreateButtonJavaCode(String pid_id,String button_id_id,String function_type) throws Exception {
		Result result = new Result();
		if(z.isNotNull(pid_id)) {
			if(z.isNotNull(function_type)) {
				//获取工程路径
				String project_path = z.sp.get("project_path");

				z_form_table table = z.tablesForZid.get(pid_id);

				//根据表id，获取formId
				String formId = sqlSession.selectOne("selectone", "SELECT c.form_id FROM z_form c  INNER JOIN z_form_table d ON c.zid = d.pid WHERE d.zid = '"+pid_id+"'");

				//获取项目ID
				StringBuffer getprojectidsql = new StringBuffer();
				getprojectidsql.append(" SELECT c.project_id FROM z_project c ");
				getprojectidsql.append(" INNER JOIN z_form f ON c.zid = f.project_id ");
				getprojectidsql.append(" INNER JOIN z_form_table ft ON f.zid = ft.pid  ");
				getprojectidsql.append(" WHERE ft.zid = '"+pid_id+"' ");
				String projectid = sqlSession.selectOne("selectone", getprojectidsql);
				
				//创建JAVA源码路径
				String packPath = project_path+"\\src\\main\\java\\com\\futvan\\z\\"+projectid+"\\"+formId;
				FileUtil.mkdirs(packPath);

				//创建文件
				//根据路径获取加载文件
				String tableIdUpper = StringUtil.firstLetterToUpper(table.getTable_id());//大写
				String buttonIdLower = StringUtil.firstLetterToUpper(button_id_id);//
				File f = new File(project_path+"\\src\\main\\java\\com\\futvan\\z\\"+projectid+"\\"+formId+"\\"+tableIdUpper+buttonIdLower+"ButtonAction.java");
				//如果未找到文件
				if (!f.exists()) {
					if (!f.getParentFile().exists()){
						f.getParentFile().mkdirs();
					}
					//创建文件
					f.createNewFile();
					//创建输入流
					OutputStreamWriter write = new OutputStreamWriter(new FileOutputStream(f), "UTF-8");
					BufferedWriter writer = new BufferedWriter(write);
					writer.write("package com.futvan.z."+projectid+"."+formId+";\n");
					writer.write("import java.util.HashMap;\n");
					writer.write("import com.futvan.z.framework.core.SuperAction;\n");
					writer.write("import com.futvan.z.framework.common.bean.Code;\n");
					writer.write("import com.futvan.z.framework.common.bean.Result;\n");
					writer.write("import com.futvan.z.framework.common.service.CommonService;\n");
					writer.write("import org.springframework.stereotype.Controller;\n");
					writer.write("import org.springframework.web.servlet.ModelAndView;\n");
					writer.write("import org.springframework.web.bind.annotation.RequestMapping;\n");
					writer.write("import org.springframework.web.bind.annotation.RequestParam;\n");
					writer.write("import org.springframework.web.bind.annotation.ResponseBody;\n");
					writer.write("import org.springframework.beans.factory.annotation.Autowired;\n");
					writer.write("@Controller\n");
					writer.write("public class "+tableIdUpper+buttonIdLower+"ButtonAction extends SuperAction{\n");
					writer.write("\n");
					writer.write("	@Autowired\n");
					writer.write("	private CommonService commonService;\n");
					writer.write("\n");
					if("0".equals(function_type)) {
						//路径新页面方法
						writer.write("	@RequestMapping(value=\"/"+button_id_id+"\")\n");
						writer.write("	public ModelAndView "+button_id_id+"(@RequestParam HashMap<String,String> bean) throws Exception {\n");
						writer.write("		ModelAndView model = new ModelAndView(\""+projectid+"/"+formId+"/"+button_id_id+"\");\n");
						writer.write("		\n");
						writer.write("		return model;\n");
						writer.write("	}\n");
					}
					if("1".equals(function_type)) {
						//返回json方法
						writer.write("	@RequestMapping(value=\"/"+button_id_id+"\")\n");
						writer.write("	public @ResponseBody Result "+button_id_id+"(@RequestParam HashMap<String,String> bean) throws Exception {\n");
						writer.write("		Result result = new Result();\n");
						writer.write("		\n");
						writer.write("		return result;\n");
						writer.write("	}\n");
					}
					writer.write("}\n");
					writer.close();
					write.close();
				}

				result.setCode(Code.SUCCESS);
				result.setMsg("代码创建成功");
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("function_type is null");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("pid_id is null");
		}
		return result;
	}

	/**
	 * 根据TableId获取表ZID
	 * @param tableId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/getTableZidForTableId")
	public @ResponseBody Result getTableZidForTableId(String tableId) throws Exception {
		Result result = new Result();
		z_form_table table = z.tables.get(tableId);
		if(z.isNotNull(table)) {
			String zid = table.getZid();
			result.setCode(Code.SUCCESS);
			result.setData(zid);
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("根据表ID："+tableId+"  未找到表对象信息。");
		}
		return result;
	}

	/**
	 * 重新生成表
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/AgainCreateDBTable")
	public @ResponseBody Result AgainCreateDBTable(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();
		if(z.isNotNull(bean)) {
			String table_id = bean.get("table_id");
			if(z.isNotNull(table_id)) {
				result = zformService.AgainCreateDBTable(table_id);
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("重新生成表执行失败|table_id is null");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("重新生成表执行失败|bean is null");
		}
		return result;
	}

	@RequestMapping(value="/z_form_insert")
	public @ResponseBody Result z_form_insert(@RequestParam HashMap<String,String> bean) throws Exception {
		return zformService.z_form_insert(bean,request);
	}

	@RequestMapping(value="/z_form_update")
	public @ResponseBody Result z_form_update(@RequestParam HashMap<String,String> bean) throws Exception {
		return zformService.z_form_update(bean,request);
	}

	@RequestMapping(value="/z_form_delete")
	public @ResponseBody Result z_form_delete(@RequestParam HashMap<String,String> bean) throws Exception {
		return zformService.z_form_delete(bean,request);
	}

	@RequestMapping(value="/z_form_table_insert")
	public @ResponseBody Result z_form_table_insert(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = zformService.z_form_table_insert(bean,request);
		//添加基础菜单
		zformService.CreateMenu(bean);
		return result;
	}

	@RequestMapping(value="/z_form_table_update")
	public @ResponseBody Result z_form_table_update(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = zformService.z_form_table_update(bean,request);
		//添加基础菜单
		zformService.CreateMenu(bean);
		
		//判读基本按钮是
		return result;
	}

	@RequestMapping(value="/z_form_table_delete")
	public @ResponseBody Result z_form_table_delete(@RequestParam HashMap<String,String> bean) throws Exception {
		return zformService.z_form_table_delete(bean,request);
	}

	@RequestMapping(value="/z_form_table_column_insert")
	public @ResponseBody Result z_form_table_column_insert(@RequestParam HashMap<String,String> bean) throws Exception {
		if(z.isNull(bean.get("column_help")) && z.isNotNull(bean.get("column_name"))) {
			bean.put("column_help", bean.get("column_name"));
		}
		return zformService.z_form_table_column_insert(bean,request);
	}

	@RequestMapping(value="/z_form_table_column_update")
	public @ResponseBody Result z_form_table_column_update(@RequestParam HashMap<String,String> bean) throws Exception {
		if(z.isNull(bean.get("column_help")) && z.isNotNull(bean.get("column_name"))) {
			bean.put("column_help", bean.get("column_name"));
		}
		return zformService.z_form_table_column_update(bean,request);
	}

	@RequestMapping(value="/z_form_table_column_delete")
	public @ResponseBody Result z_form_table_column_delete(@RequestParam HashMap<String,String> bean) throws Exception {
		return zformService.z_form_table_column_delete(bean,request);
	}

	@RequestMapping(value="/z_form_table_button_insert")
	public @ResponseBody Result z_form_table_button_insert(@RequestParam HashMap<String,String> bean) throws Exception {
		return zformService.z_form_table_button_insert(bean,request);
	}

	@RequestMapping(value="/z_form_table_button_update")
	public @ResponseBody Result z_form_table_button_update(@RequestParam HashMap<String,String> bean) throws Exception {
		return zformService.z_form_table_button_update(bean,request);
	}

	@RequestMapping(value="/z_form_table_button_delete")
	public @ResponseBody Result z_form_table_button_delete(@RequestParam HashMap<String,String> bean) throws Exception {
		return zformService.z_form_table_button_delete(bean,request);
	}
}
