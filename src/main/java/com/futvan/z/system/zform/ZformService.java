package com.futvan.z.system.zform;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.bean.z_form_table;
import com.futvan.z.framework.common.bean.z_form_table_column;
import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.BeanUtil;
import com.futvan.z.framework.util.FileUtil;
import com.futvan.z.framework.util.JsonUtil;
import com.futvan.z.framework.util.StringUtil;
import com.futvan.z.framework.util.SystemUtil;
@Service
public class ZformService extends SuperService{

	public Result z_form_insert(HashMap<String, String> bean, HttpServletRequest request) throws Exception{
		Result result = super.insert(bean,request);
		return result;
	}

	public Result z_form_update(HashMap<String, String> bean, HttpServletRequest request) throws Exception{
		Result result = super.update(bean,request);
		return result;
	}

	public Result z_form_delete(HashMap<String, String> bean, HttpServletRequest request) throws Exception{
		String zformIds = String.valueOf(bean.get("zids"));
		String [] zformIdArray = zformIds.split(",");
		for (int i = 0; i < zformIdArray.length; i++) {
			List<String> zidsList = selectListString("SELECT d.zid FROM z_form c INNER JOIN z_form_table d ON c.zid = d.pid WHERE c.zid = '"+zformIdArray[i]+"'");
			if(zidsList!=null && zidsList.size()>0) {
				String zids = StringUtil.ListToString(zidsList, ",", "", "");
				HashMap<String,String> beanTemp = new HashMap<String, String>();
				beanTemp.put("zids", zids);
				DeleteTableDB(beanTemp);
			}
			//获取功能关联项目ID
			String project_id = z.formzidToProjects.get(zformIdArray[i]);
			//获取FormId
			String form_id = selectString("SELECT c.form_id FROM z_form c WHERE c.zid = '"+zformIdArray[i]+"' ");
			//获取项目路径
			String project_path = z.sp.get("project_path");
			//删除JAVA
			File javaPackage = new File(project_path+"\\src\\main\\java\\com\\futvan\\z\\"+project_id+"\\"+form_id);
			FileUtil.deleteDir(javaPackage);
			//删除Web
			File WebPackage = new File(project_path+"\\src\\main\\webapp\\views\\"+project_id+"\\"+form_id);
			FileUtil.deleteDir(WebPackage);
		}
		Result result = super.delete(bean);
		//删除菜单
		for (int i = 0; i < zformIdArray.length; i++) {
			String menuId = selectString("SELECT zm.zid FROM z_menu zm WHERE zm.formid = '"+zformIdArray[i]+"' ");
			//删除菜单Button
			delete("DELETE FROM z_menu_form_button WHERE pid = '"+menuId+"' ");
			//删除菜单
			delete(" delete from z_menu WHERE zid = '"+menuId+"'");
		}
		return result;
	}

	/**
	 * 
	 * @param bean
	 * @param request
	 * @return
	 * @throws Exception
	 * 
	 * 	关闭自动提交事务
	 * @Transactional(propagation=Propagation.NOT_SUPPORTED)
	 * 
	 * 
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED)
	public Result z_form_table_insert(HashMap<String, String> bean, HttpServletRequest request) throws Exception {
		//判读是否创建多个主表
		CheckIsCTablc(bean);
		//创建表与基础字段
		CreateTableDB(bean);
		Result result = super.insert(bean,request);
		return result;
	}

	public Result z_form_table_update(HashMap<String, String> bean, HttpServletRequest request) throws Exception{
		UpdateTableDB(bean);
		Result result = super.update(bean,request);
		return result;
	}

	public Result z_form_table_delete(HashMap<String, String> bean, HttpServletRequest request) throws Exception{
		DeleteTableDB(bean);
		return super.delete(bean);
	}

	public Result z_form_table_column_insert(HashMap<String, String> bean, HttpServletRequest request) throws Exception{
		CreateTableColumnDB(bean);
		Result result = super.insert(bean,request);
		return result;
	}

	public Result z_form_table_column_update(HashMap<String, String> bean, HttpServletRequest request) throws Exception{
		UpdateTableColumnDB(bean);
		Result result = super.update(bean,request);
		return result;
	}

	public Result z_form_table_column_delete(HashMap<String, String> bean, HttpServletRequest request) throws Exception{
		DeleteTableColumnDB(bean);
		return super.delete(bean);
	}

	public Result z_form_table_button_insert(HashMap<String, String> bean, HttpServletRequest request) throws Exception{
		Result result = super.insert(bean,request);
		return result;
	}

	public Result z_form_table_button_update(HashMap<String, String> bean, HttpServletRequest request) throws Exception{
		Result result = super.update(bean,request);
		return result;
	}

	public Result z_form_table_button_delete(HashMap<String, String> bean, HttpServletRequest request) throws Exception{
		return super.delete(bean);
	}

	private void CreateTableDB(HashMap<String, String> bean) throws Exception {
		String table_id = String.valueOf(bean.get("table_id"));
		String parent_table_id = String.valueOf(bean.get("parent_table_id"));
		//创建表
		StringBuffer CreateTableSQL = new StringBuffer();
		if("".equals(parent_table_id) || parent_table_id==null) {
			CreateTableSQL.append(" CREATE TABLE "+table_id+" ( ");
			CreateTableSQL.append(" zid varchar(128) NOT NULL COMMENT '主健', ");
			CreateTableSQL.append(" number varchar(128) DEFAULT NULL COMMENT '编号',");
			CreateTableSQL.append(" name varchar(128) DEFAULT NULL COMMENT '名称',");
			CreateTableSQL.append(" seq int(10) AUTO_INCREMENT NOT NULL COMMENT '序号', ");
			CreateTableSQL.append(" create_user varchar(128) DEFAULT NULL COMMENT '创建者',");
			CreateTableSQL.append(" create_time datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',");
			CreateTableSQL.append(" update_user varchar(128) DEFAULT NULL COMMENT '修改者',");
			CreateTableSQL.append(" update_time datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',");
			CreateTableSQL.append(" zversion int(10) DEFAULT 0 COMMENT '版本号',");
			CreateTableSQL.append(" orgid varchar(128) DEFAULT NULL COMMENT '所属组织',");
			CreateTableSQL.append(" remarks longtext COMMENT '备注',");
			CreateTableSQL.append(" PRIMARY KEY  (zid), ");
			CreateTableSQL.append(" INDEX number (number), ");
			CreateTableSQL.append(" INDEX name (name), ");
			CreateTableSQL.append(" INDEX seq (seq), ");
			CreateTableSQL.append(" INDEX orgid (orgid), ");
			CreateTableSQL.append(" INDEX create_user (create_user), ");
			CreateTableSQL.append(" INDEX update_user (update_user) ");
			//添加外键约束
			//			CreateTableSQL.append(" ,CONSTRAINT fk_"+table_id+"_create_user_for_z_user_zid FOREIGN KEY (create_user) REFERENCES z_user(zid) ON DELETE RESTRICT ON UPDATE RESTRICT, ");
			//			CreateTableSQL.append(" CONSTRAINT fk_"+table_id+"_update_user_for_z_user_zid FOREIGN KEY (update_user) REFERENCES z_user(zid) ON DELETE RESTRICT ON UPDATE RESTRICT, ");
			//			CreateTableSQL.append(" CONSTRAINT fk_"+table_id+"_orgid_for_z_org_zid FOREIGN KEY (orgid) REFERENCES z_org(zid) ON DELETE RESTRICT ON UPDATE RESTRICT ");
			CreateTableSQL.append(" ) ");
			update(CreateTableSQL.toString());
			//添加主表，字段
			CreateCtableConlumn(bean);
			//添加主表，列表按钮
			CreateCtableListButton(bean);
			//添加主表，卡片列表按钮
			CreateCtableListCardButton(bean);
			//添加主表，编辑按钮
			CreateCtableEditButton(bean);
		}else {
			CreateTableSQL.append(" CREATE TABLE "+table_id+" ( ");
			CreateTableSQL.append(" zid varchar(128) NOT NULL COMMENT '主健', ");
			CreateTableSQL.append(" pid varchar(128) NOT NULL COMMENT '主表主健',");
			CreateTableSQL.append(" number varchar(128) DEFAULT NULL COMMENT '编号',");
			CreateTableSQL.append(" name varchar(128) DEFAULT NULL COMMENT '名称',");
			CreateTableSQL.append(" seq int(10) AUTO_INCREMENT NOT NULL COMMENT '序号', ");
			CreateTableSQL.append(" create_user varchar(128) DEFAULT NULL COMMENT '创建者',");
			CreateTableSQL.append(" create_time datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',");
			CreateTableSQL.append(" update_user varchar(128) DEFAULT NULL COMMENT '修改者',");
			CreateTableSQL.append(" update_time datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',");
			CreateTableSQL.append(" zversion int(10) DEFAULT 0 COMMENT '版本号',");
			CreateTableSQL.append(" orgid varchar(128) DEFAULT NULL COMMENT '所属组织',");
			CreateTableSQL.append(" remarks longtext COMMENT '备注',");
			CreateTableSQL.append(" PRIMARY KEY  (zid), ");
			CreateTableSQL.append(" INDEX pid (pid), ");
			CreateTableSQL.append(" INDEX number (number), ");
			CreateTableSQL.append(" INDEX name (name), ");
			CreateTableSQL.append(" INDEX seq (seq), ");
			CreateTableSQL.append(" INDEX orgid (orgid), ");
			CreateTableSQL.append(" INDEX create_user (create_user), ");
			CreateTableSQL.append(" INDEX update_user (update_user) ");
			//添加外键约束
			//			CreateTableSQL.append(" ,CONSTRAINT fk_"+table_id+"_pid_for_"+parent_table_id+"_zid FOREIGN KEY (pid) REFERENCES "+parent_table_id+"(zid) ON DELETE RESTRICT ON UPDATE RESTRICT, ");
			//			CreateTableSQL.append(" CONSTRAINT fk_"+table_id+"_create_user_for_z_user_zid FOREIGN KEY (create_user) REFERENCES z_user(zid) ON DELETE RESTRICT ON UPDATE RESTRICT, ");
			//			CreateTableSQL.append(" CONSTRAINT fk_"+table_id+"_update_user_for_z_user_zid FOREIGN KEY (update_user) REFERENCES z_user(zid) ON DELETE RESTRICT ON UPDATE RESTRICT, ");
			//			CreateTableSQL.append(" CONSTRAINT fk_"+table_id+"_orgid_for_z_org_zid FOREIGN KEY (orgid) REFERENCES z_org(zid) ON DELETE RESTRICT ON UPDATE RESTRICT ");
			CreateTableSQL.append(" ) ");
			update(CreateTableSQL.toString());
			//添加细表，字段
			CreateDtableConlumn(bean);
			//添加细表列表按钮
			CreateDtableListButton(bean);
			//添加细表编辑页面按钮
			CreateDtableEditButton(bean);
		}
	}

	

	/**
	 * 重新生成表
	 * @param zid 表ZID
	 * @return
	 * @throws Exception 
	 */
	public Result AgainCreateDBTable(String tableId) throws Exception {
		Result result = new Result();
		z_form_table table = z.tables.get(tableId);

		//执行删除原表
		String deleteTableSQL = "drop table "+table.getTable_id();
		delete(deleteTableSQL);

		StringBuffer CreateTableSQL = new StringBuffer();
		//重新创建表
		if(z.isNull(table.getParent_table_id())) {
			//主表
			CreateTableSQL.append(" CREATE TABLE "+table.getTable_id()+" ( ");
			CreateTableSQL.append(" zid varchar(128) NOT NULL COMMENT '主健', ");
			CreateTableSQL.append(" number varchar(128) DEFAULT NULL COMMENT '编号',");
			CreateTableSQL.append(" name varchar(128) DEFAULT NULL COMMENT '名称',");
			CreateTableSQL.append(" seq int(10) AUTO_INCREMENT NOT NULL COMMENT '序号', ");
			CreateTableSQL.append(" create_user varchar(128) DEFAULT NULL COMMENT '创建者',");
			CreateTableSQL.append(" create_time datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',");
			CreateTableSQL.append(" update_user varchar(128) DEFAULT NULL COMMENT '修改者',");
			CreateTableSQL.append(" update_time datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',");
			CreateTableSQL.append(" zversion int(10) DEFAULT 0 COMMENT '版本号',");
			CreateTableSQL.append(" orgid varchar(128) DEFAULT NULL COMMENT '所属组织',");
			CreateTableSQL.append(" remarks longtext COMMENT '备注',");
			CreateTableSQL.append(" PRIMARY KEY  (zid), ");
			CreateTableSQL.append(" INDEX number (number), ");
			CreateTableSQL.append(" INDEX name (name), ");
			CreateTableSQL.append(" INDEX seq (seq), ");
			CreateTableSQL.append(" INDEX orgid (orgid), ");
			CreateTableSQL.append(" INDEX create_user (create_user), ");
			CreateTableSQL.append(" INDEX update_user (update_user), ");
			CreateTableSQL.append(" CONSTRAINT fk_"+table.getTable_id()+"_create_user_for_z_user_zid FOREIGN KEY (create_user) REFERENCES z_user(zid) ON DELETE RESTRICT ON UPDATE RESTRICT, ");
			CreateTableSQL.append(" CONSTRAINT fk_"+table.getTable_id()+"_update_user_for_z_user_zid FOREIGN KEY (update_user) REFERENCES z_user(zid) ON DELETE RESTRICT ON UPDATE RESTRICT, ");
			CreateTableSQL.append(" CONSTRAINT fk_"+table.getTable_id()+"_orgid_for_z_org_zid FOREIGN KEY (orgid) REFERENCES z_org(zid) ON DELETE RESTRICT ON UPDATE RESTRICT ");
			CreateTableSQL.append(" ) ");
			update(CreateTableSQL.toString());
		}else {
			//子表
			CreateTableSQL.append(" CREATE TABLE "+table.getTable_id()+" ( ");
			CreateTableSQL.append(" zid varchar(128) NOT NULL COMMENT '主健', ");
			CreateTableSQL.append(" pid varchar(128) NOT NULL COMMENT '主表主健',");
			CreateTableSQL.append(" number varchar(128) DEFAULT NULL COMMENT '编号',");
			CreateTableSQL.append(" name varchar(128) DEFAULT NULL COMMENT '名称',");
			CreateTableSQL.append(" seq int(10) AUTO_INCREMENT NOT NULL COMMENT '序号', ");
			CreateTableSQL.append(" create_user varchar(128) DEFAULT NULL COMMENT '创建者',");
			CreateTableSQL.append(" create_time datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',");
			CreateTableSQL.append(" update_user varchar(128) DEFAULT NULL COMMENT '修改者',");
			CreateTableSQL.append(" update_time datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',");
			CreateTableSQL.append(" zversion int(10) DEFAULT 0 COMMENT '版本号',");
			CreateTableSQL.append(" orgid varchar(128) DEFAULT NULL COMMENT '所属组织',");
			CreateTableSQL.append(" remarks longtext COMMENT '备注',");
			CreateTableSQL.append(" PRIMARY KEY  (zid), ");
			CreateTableSQL.append(" INDEX pid (pid), ");
			CreateTableSQL.append(" INDEX number (number), ");
			CreateTableSQL.append(" INDEX name (name), ");
			CreateTableSQL.append(" INDEX seq (seq), ");
			CreateTableSQL.append(" INDEX orgid (orgid), ");
			CreateTableSQL.append(" INDEX create_user (create_user), ");
			CreateTableSQL.append(" INDEX update_user (update_user), ");
			CreateTableSQL.append(" CONSTRAINT fk_"+table.getTable_id()+"_pid_for_"+table.getParent_table_id()+"_zid FOREIGN KEY (pid) REFERENCES "+table.getParent_table_id()+"(zid) ON DELETE RESTRICT ON UPDATE RESTRICT, ");
			CreateTableSQL.append(" CONSTRAINT fk_"+table.getTable_id()+"_create_user_for_z_user_zid FOREIGN KEY (create_user) REFERENCES z_user(zid) ON DELETE RESTRICT ON UPDATE RESTRICT, ");
			CreateTableSQL.append(" CONSTRAINT fk_"+table.getTable_id()+"_update_user_for_z_user_zid FOREIGN KEY (update_user) REFERENCES z_user(zid) ON DELETE RESTRICT ON UPDATE RESTRICT, ");
			CreateTableSQL.append(" CONSTRAINT fk_"+table.getTable_id()+"_orgid_for_z_org_zid FOREIGN KEY (orgid) REFERENCES z_org(zid) ON DELETE RESTRICT ON UPDATE RESTRICT ");
			CreateTableSQL.append(" ) ");
			update(CreateTableSQL.toString());
		}

		//添加字段
		List<z_form_table_column> columnList = table.getColumns();
		for (z_form_table_column column : columnList) {
			String columnid = column.getColumn_id();
			if(!"zid".equals(columnid) 
					&& !"pid".equals(columnid)
					&& !"number".equals(columnid)
					&& !"name".equals(columnid)
					&& !"create_user".equals(columnid)
					&& !"update_user".equals(columnid)
					&& !"create_time".equals(columnid)
					&& !"update_time".equals(columnid)
					&& !"seq".equals(columnid)
					&& !"orgid".equals(columnid)
					&& !"zversion".equals(columnid)
					&& !"remarks".equals(columnid)
					&& column!=null){

				HashMap<String,String> bean = BeanUtil.BeanToMap(column);
				CreateTableColumnDB(bean);
			}

		}
		result.setCode(Code.SUCCESS);
		result.setMsg("重新生成表成功");
		return result;
	}

	private void CreateCtableConlumn(HashMap<String, String> bean) throws Exception {
		CreateColumn_zid(bean);
		CreateColumn_number(bean);
		CreateColumn_name(bean);
		CreateColumn_seq(bean);
		CreateColumn_create_user(bean);
		CreateColumn_create_time(bean);
		CreateColumn_update_user(bean);
		CreateColumn_update_time(bean);
		CreateColumn_orgid(bean);
		CreateColumn_zversion(bean);
		CreateColumn_remarks(bean);
	}
	private void CreateDtableConlumn(HashMap<String, String> bean) throws Exception {
		CreateColumn_zid(bean);
		CreateColumn_number(bean);
		CreateColumn_name(bean);
		CreateColumn_pid(bean);
		CreateColumn_seq(bean);
		CreateColumn_create_user(bean);
		CreateColumn_create_time(bean);
		CreateColumn_update_user(bean);
		CreateColumn_update_time(bean);
		CreateColumn_orgid(bean);
		CreateColumn_zversion(bean);
		CreateColumn_remarks(bean);
	}

	private void CreateCtableListButton(HashMap<String, String> bean) {
		CreateListButton_select_button(bean);
		CreateListButton_add_button(bean);
		CreateListButton_edit_button(bean);
		CreateListButton_remove_button(bean);
		CreateListButton_look_button(bean);
		CreateListButton_moveUp_button(bean);
		CreateListButton_moveDown_button(bean);
	}
	
	private void CreateCtableListCardButton(HashMap<String, String> bean) {
		CreateListButton_list_card_edit_button(bean);
		CreateListButton_list_card_delete_button(bean);
		CreateListButton_list_card_save_button(bean);
	}
	
	
	private void CreateCtableEditButton(HashMap<String, String> bean) {
		CreateEditButton_save_button(bean);
		CreateEditButton_save_and_add_button(bean);
		CreateEditButton_save_and_return_button(bean);
		CreateEditButton_return_parent_page_button(bean);
		CreateEditButton_oa_submit_button(bean);
		CreateLookButton_return_parent_page_button(bean);
	}



	private void CreateDtableListButton(HashMap<String, String> bean) {
		CreateDetailListButton_add_detail_button(bean);
		CreateDetailListButton_edit_detail_button(bean);
		CreateDetailListButton_remove_detail_button(bean);
		CreateDetailListButton_look_detail_button(bean);
		CreateListButton_moveUp_button(bean);
		CreateListButton_moveDown_button(bean);
	}

	private void CreateDtableEditButton(HashMap<String, String> bean) {
		CreateEditButton_save_button(bean);
		CreateEditButton_save_and_add_button(bean);
		CreateEditButton_save_and_return_button(bean);
		CreateEditButton_return_parent_page_button(bean);
		CreateLookButton_return_parent_page_button(bean);
	}

	/**
	 * 数据库中创建字段
	 * @param table_name 表名
	 * @param bean 参数
	 * @throws Exception 
	 */
	private void CreateTableColumnDB(HashMap<String,String> bean) throws Exception {
		String table_id = selectString("SELECT t.table_id FROM z_form_table t WHERE t.zid =  '"+String.valueOf(bean.get("pid"))+"'");
		String column_id = String.valueOf(bean.get("column_id"));
		String column_name = String.valueOf(bean.get("column_name"));
		String column_length_db = String.valueOf(bean.get("column_length_db"));
		String column_type = String.valueOf(bean.get("column_type"));
		String column_default = String.valueOf(bean.get("column_default"));
		String is_null = String.valueOf(bean.get("is_null"));

		StringBuffer sql = new StringBuffer();
		sql.append("alter table "+table_id+" add ");
		if("0".equals(column_type)) {//文本
			if(z.isNull(column_length_db)) {
				column_length_db = "128";
			}
			sql.append(column_id+" varchar("+column_length_db+") ");
		}else if("1".equals(column_type)) {//多行文本
			sql.append(column_id+" longtext ");
		}else if("2".equals(column_type)) {//数字
			if(z.isNull(column_length_db)) {
				column_length_db = "10,2";
			}
			sql.append(column_id+" decimal("+column_length_db+") ");
		}else if("3".equals(column_type)) {//文件
			sql.append(column_id+" longtext ");
		}else if("4".equals(column_type)) {//图片
			sql.append(column_id+" longtext ");
		}else if("5".equals(column_type)) {//多选
			sql.append(column_id+" varchar(128) ");
		}else if("6".equals(column_type)) {//单选
			sql.append(column_id+" varchar(128) ");
		}else if("7".equals(column_type)) {//下拉框
			sql.append(column_id+" varchar(128) ");
		}else if("8".equals(column_type)) {//Z5
			sql.append(column_id+" varchar(128) ");
		}else if("9".equals(column_type)) {//日期
			sql.append(column_id+" datetime ");
		}else if("10".equals(column_type)) {//日期时间
			sql.append(column_id+" datetime ");
		}else if("11".equals(column_type)) {//HTML输入框
			sql.append(column_id+" longtext ");
		}else if("12".equals(column_type)) {//源码输入框
			sql.append(column_id+" longtext ");
		}
		//是否可为空 1是可为空
		sql.append(getColumnIsNull(is_null));
		//添加字段名
		sql.append(" comment '"+column_name+"'; ");
		update(sql.toString());

	}

	private void UpdateTableDB(HashMap<String, String> bean) throws Exception {
		String table_id = String.valueOf(bean.get("table_id"));
		String parent_table_id = String.valueOf(bean.get("parent_table_id"));

		String old_table_id = selectString("SELECT table_id FROM z_form_table WHERE zid =  '"+String.valueOf(bean.get("zid"))+"'");
		//更新表名
		update("alter table "+old_table_id+" rename to "+table_id);

		//主表
		if("".equals(parent_table_id) || parent_table_id==null) {
			//添加主表，列表按钮
			CreateCtableListButton(bean);

			//添加主表，编辑按钮
			CreateCtableEditButton(bean);
		}else {
			//添加子表，列表按钮
			CreateDtableListButton(bean);

			//添加子表，编辑按钮
			CreateDtableEditButton(bean);
		}

	}


	private void UpdateTableColumnDB(HashMap<String,String> bean) throws Exception {
		String table_id = selectString("SELECT t.table_id FROM z_form_table_column tc INNER JOIN z_form_table t ON tc.pid = t.zid WHERE tc.zid = '"+String.valueOf(bean.get("zid"))+"'");
		String column_id = String.valueOf(bean.get("column_id"));
		String old_column_id = selectString("SELECT tc.column_id FROM z_form_table_column tc WHERE tc.zid = '"+String.valueOf(bean.get("zid"))+"'");
		String column_name = String.valueOf(bean.get("column_name"));
		String column_length_db = String.valueOf(bean.get("column_length_db"));
		String column_type = String.valueOf(bean.get("column_type"));
		String column_default = String.valueOf(bean.get("column_default"));
		String is_null = String.valueOf(bean.get("is_null"));

		StringBuffer sql = new StringBuffer();
		sql.append("alter table "+table_id+" change "+old_column_id+" ");
		if("0".equals(column_type)) {//文本
			if("".equals(column_length_db) || column_length_db == null) {
				column_length_db = "128";
			}
			sql.append(column_id+" varchar("+column_length_db+") ");
		}else if("1".equals(column_type)) {//多行文本
			sql.append(column_id+" longtext ");
		}else if("2".equals(column_type)) {//数字
			if("".equals(column_length_db) || column_length_db == null) {
				column_length_db = "10,2";
			}
			sql.append(column_id+" decimal("+column_length_db+") ");
		}else if("3".equals(column_type)) {//文件
			sql.append(column_id+" longtext ");
		}else if("4".equals(column_type)) {//图片
			sql.append(column_id+" longtext ");
		}else if("5".equals(column_type)) {//多选
			sql.append(column_id+" varchar(128) ");
		}else if("6".equals(column_type)) {//单选
			sql.append(column_id+" varchar(128) ");
		}else if("7".equals(column_type)) {//下拉框
			sql.append(column_id+" varchar(128) ");
		}else if("8".equals(column_type)) {//Z5
			sql.append(column_id+" varchar(128) ");
		}else if("9".equals(column_type)) {//日期
			sql.append(column_id+" datetime ");
		}else if("10".equals(column_type)) {//日期时间
			sql.append(column_id+" datetime ");
		}else if("11".equals(column_type)) {//HTML输入框
			sql.append(column_id+" longtext ");
		}else if("12".equals(column_type)) {//源码输入框
			sql.append(column_id+" longtext ");
		}
		//是否可为空 1是可为空
		sql.append(getColumnIsNull(is_null));
		//添加字段名
		sql.append(" comment '"+column_name+"'; ");
		update(sql.toString());


	}


	private void DeleteTableDB(HashMap<String, String> bean) throws Exception {
		String zids = String.valueOf(bean.get("zids"));
		String []zidarray = zids.split(",");
		for (int i = 0; i < zidarray.length; i++) {
			String zid = zidarray[i];
			String table_id = selectString("select table_id from z_form_table where zid = '"+zid+"'");
			StringBuffer sql = new StringBuffer();
			sql.append("drop table "+table_id);
			try {
				update(sql.toString());

				//删除字段
				delete("delete from z_form_table_column WHERE pid = '"+zid+"'");

				//删除按钮
				delete("delete from z_form_table_button WHERE pid = '"+zid+"'");

			} catch (Exception e) {
			}
		}

	}


	private void DeleteTableColumnDB(HashMap<String,String> bean) throws Exception {
		String zids = String.valueOf(bean.get("zids"));
		String []zidarray = zids.split(",");
		for (int i = 0; i < zidarray.length; i++) {
			String zid = zidarray[i];
			String column_id = selectString("select column_id from z_form_table_column where zid = '"+zid+"'");
			String table_id = selectString("SELECT t.table_id FROM z_form_table_column tc INNER JOIN z_form_table t ON tc.pid = t.zid WHERE tc.zid = '"+zid+"'");
			try {
				StringBuffer sql = new StringBuffer();
				sql.append("alter table "+table_id+" drop "+column_id);
				update(sql.toString());
			} catch (Exception e) {
				z.Exception("删除数据库字段："+column_id+" 出错！|"+e.getMessage());
			}
		}
	}

	/**
	 * 判读标准按钮是否存在
	 * @param pid
	 * @param button_name
	 * @return
	 */
	private boolean ButtonNotExist(String pid, String button_id) {
		String sql = "SELECT COUNT(*) FROM z_form_table_button WHERE pid = '"+pid+"' AND button_id = '"+button_id+"'";
		int num = selectInt(sql);
		if(num==0) {
			return true;
		}else {
			return false;
		}
	}

	private void CreateListButton_select_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_select_button";
		if(ButtonNotExist(pid,button_id)){
			//查询
			StringBuffer select_button_sql = new StringBuffer();
			select_button_sql.append("INSERT INTO z_form_table_button ");
			select_button_sql.append(" (zid, ");
			select_button_sql.append(" pid, ");
			select_button_sql.append(" page_type, ");
			select_button_sql.append(" function_type, ");
			select_button_sql.append(" js_onclick, ");
			select_button_sql.append(" button_id, ");
			select_button_sql.append(" button_icon, ");
			select_button_sql.append(" button_name) ");
			select_button_sql.append(" VALUES ( ");
			select_button_sql.append(" '"+zid+"', ");
			select_button_sql.append(" '"+pid+"', ");
			select_button_sql.append(" 'list', ");
			select_button_sql.append(" '2', ");
			select_button_sql.append(" 'openSelectWindows();', ");
			select_button_sql.append(" '"+button_id+"', ");
			select_button_sql.append(" 'fa fa-search', ");
			select_button_sql.append(" '查询') ");
			insert(select_button_sql.toString());
		}

	}

	private void CreateListButton_add_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_add_button";
		if(ButtonNotExist(pid,button_id)){
			//新增
			StringBuffer add_button_sql = new StringBuffer();
			add_button_sql.append("INSERT INTO z_form_table_button ");
			add_button_sql.append(" (zid, ");
			add_button_sql.append(" pid, ");
			add_button_sql.append(" page_type, ");
			add_button_sql.append(" function_type, ");
			add_button_sql.append(" js_onclick, ");
			add_button_sql.append(" button_id, ");
			add_button_sql.append(" button_icon, ");
			add_button_sql.append(" button_name) ");
			add_button_sql.append(" VALUES ( ");
			add_button_sql.append(" '"+zid+"', ");
			add_button_sql.append(" '"+pid+"', ");
			add_button_sql.append(" 'list', ");
			add_button_sql.append(" '0', ");
			add_button_sql.append(" 'add();', ");
			add_button_sql.append(" '"+button_id+"', ");
			add_button_sql.append(" 'fa fa-file-text', ");
			add_button_sql.append(" '新增') ");
			insert(add_button_sql.toString());
		}
	}
	private void CreateListButton_edit_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_edit_button";
		if(ButtonNotExist(pid,button_id)){
			//修改
			StringBuffer edit_button_sql = new StringBuffer();
			edit_button_sql.append("INSERT INTO z_form_table_button ");
			edit_button_sql.append(" (zid, ");
			edit_button_sql.append(" pid, ");
			edit_button_sql.append(" page_type, ");
			edit_button_sql.append(" function_type, ");
			edit_button_sql.append(" js_onclick, ");
			edit_button_sql.append(" button_id, ");
			edit_button_sql.append(" button_icon, ");
			edit_button_sql.append(" button_name) ");
			edit_button_sql.append(" VALUES ( ");
			edit_button_sql.append(" '"+zid+"', ");
			edit_button_sql.append(" '"+pid+"', ");
			edit_button_sql.append(" 'list', ");
			edit_button_sql.append(" '0', ");
			edit_button_sql.append(" 'edit();', ");
			edit_button_sql.append(" '"+button_id+"', ");
			edit_button_sql.append(" 'fa fa-pencil-square-o', ");
			edit_button_sql.append(" '修改') ");
			insert(edit_button_sql.toString());
		}
	}
	private void CreateListButton_remove_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_remove_button";
		if(ButtonNotExist(pid,button_id)){
			//删除
			StringBuffer remove_button_sql = new StringBuffer();
			remove_button_sql.append("INSERT INTO z_form_table_button ");
			remove_button_sql.append(" (zid, ");
			remove_button_sql.append(" pid, ");
			remove_button_sql.append(" page_type, ");
			remove_button_sql.append(" function_type, ");
			remove_button_sql.append(" js_onclick, ");
			remove_button_sql.append(" button_id, ");
			remove_button_sql.append(" button_icon, ");
			remove_button_sql.append(" button_name) ");
			remove_button_sql.append(" VALUES ( ");
			remove_button_sql.append(" '"+zid+"', ");
			remove_button_sql.append(" '"+pid+"', ");
			remove_button_sql.append(" 'list', ");
			remove_button_sql.append(" '1', ");
			remove_button_sql.append(" 'remove();', ");
			remove_button_sql.append(" '"+button_id+"', ");
			remove_button_sql.append(" 'fa fa-trash', ");
			remove_button_sql.append(" '删除') ");
			insert(remove_button_sql.toString());
		}
	}

	private void CreateListButton_look_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_look_button";
		if(ButtonNotExist(pid,button_id)){
			//修改
			StringBuffer edit_button_sql = new StringBuffer();
			edit_button_sql.append("INSERT INTO z_form_table_button ");
			edit_button_sql.append(" (zid, ");
			edit_button_sql.append(" pid, ");
			edit_button_sql.append(" page_type, ");
			edit_button_sql.append(" function_type, ");
			edit_button_sql.append(" js_onclick, ");
			edit_button_sql.append(" button_id, ");
			edit_button_sql.append(" button_icon, ");
			edit_button_sql.append(" button_name) ");
			edit_button_sql.append(" VALUES ( ");
			edit_button_sql.append(" '"+zid+"', ");
			edit_button_sql.append(" '"+pid+"', ");
			edit_button_sql.append(" 'list', ");
			edit_button_sql.append(" '0', ");
			edit_button_sql.append(" 'look();', ");
			edit_button_sql.append(" '"+button_id+"', ");
			edit_button_sql.append(" 'fa fa-eye', ");
			edit_button_sql.append(" '查看') ");
			insert(edit_button_sql.toString());
		}
	}

	private void CreateListButton_moveUp_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_moveUp_button";
		if(ButtonNotExist(pid,button_id)){
			//上移
			StringBuffer moveUp_sql = new StringBuffer();
			moveUp_sql.append("INSERT INTO z_form_table_button ");
			moveUp_sql.append(" (zid, ");
			moveUp_sql.append(" pid, ");
			moveUp_sql.append(" page_type, ");
			moveUp_sql.append(" function_type, ");
			moveUp_sql.append(" js_onclick, ");
			moveUp_sql.append(" button_id, ");
			moveUp_sql.append(" button_icon, ");
			moveUp_sql.append(" button_name) ");
			moveUp_sql.append(" VALUES ( ");
			moveUp_sql.append(" '"+zid+"', ");
			moveUp_sql.append(" '"+pid+"', ");
			moveUp_sql.append(" 'list', ");
			moveUp_sql.append(" '1', ");
			moveUp_sql.append(" 'moveUp();', ");
			moveUp_sql.append(" '"+button_id+"', ");
			moveUp_sql.append(" 'fa fa-angle-up', ");
			moveUp_sql.append(" '上移') ");
			insert(moveUp_sql.toString());
		}
	}
	private void CreateListButton_moveDown_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_moveDown_button";
		if(ButtonNotExist(pid,button_id)){
			//下移
			StringBuffer moveDown_sql = new StringBuffer();
			moveDown_sql.append("INSERT INTO z_form_table_button ");
			moveDown_sql.append(" (zid, ");
			moveDown_sql.append(" pid, ");
			moveDown_sql.append(" page_type, ");
			moveDown_sql.append(" function_type, ");
			moveDown_sql.append(" js_onclick, ");
			moveDown_sql.append(" button_id, ");
			moveDown_sql.append(" button_icon, ");
			moveDown_sql.append(" button_name) ");
			moveDown_sql.append(" VALUES ( ");
			moveDown_sql.append(" '"+zid+"', ");
			moveDown_sql.append(" '"+pid+"', ");
			moveDown_sql.append(" 'list', ");
			moveDown_sql.append(" '1', ");
			moveDown_sql.append(" 'moveDown();', ");
			moveDown_sql.append(" '"+button_id+"', ");
			moveDown_sql.append(" 'fa fa-angle-down', ");
			moveDown_sql.append(" '下移') ");
			insert(moveDown_sql.toString());
		}
	}
	
	private void CreateListButton_list_card_save_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_list_card_save_button";
		if(ButtonNotExist(pid,button_id)){
			StringBuffer button_sql = new StringBuffer();
			button_sql.append("INSERT INTO z_form_table_button ");
			button_sql.append(" (zid, ");
			button_sql.append(" pid, ");
			button_sql.append(" page_type, ");
			button_sql.append(" function_type, ");
			button_sql.append(" js_onclick, ");
			button_sql.append(" button_id, ");
			button_sql.append(" button_icon, ");
			button_sql.append(" button_name) ");
			button_sql.append(" VALUES ( ");
			button_sql.append(" '"+zid+"', ");
			button_sql.append(" '"+pid+"', ");
			button_sql.append(" 'list_card', ");
			button_sql.append(" '1', ");
			button_sql.append(" 'list_card_save(this);', ");
			button_sql.append(" '"+button_id+"', ");
			button_sql.append(" 'fa fa-floppy-o', ");
			button_sql.append(" '保存') ");
			insert(button_sql.toString());
		}
		
	}

	private void CreateListButton_list_card_delete_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_list_card_delete_button";
		if(ButtonNotExist(pid,button_id)){
			StringBuffer button_sql = new StringBuffer();
			button_sql.append("INSERT INTO z_form_table_button ");
			button_sql.append(" (zid, ");
			button_sql.append(" pid, ");
			button_sql.append(" page_type, ");
			button_sql.append(" function_type, ");
			button_sql.append(" js_onclick, ");
			button_sql.append(" button_id, ");
			button_sql.append(" button_icon, ");
			button_sql.append(" button_name) ");
			button_sql.append(" VALUES ( ");
			button_sql.append(" '"+zid+"', ");
			button_sql.append(" '"+pid+"', ");
			button_sql.append(" 'list_card', ");
			button_sql.append(" '1', ");
			button_sql.append(" 'list_card_delete(this);', ");
			button_sql.append(" '"+button_id+"', ");
			button_sql.append(" 'fa fa-trash', ");
			button_sql.append(" '删除') ");
			insert(button_sql.toString());
		}
		
	}

	private void CreateListButton_list_card_edit_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_list_card_edit_button";
		if(ButtonNotExist(pid,button_id)){
			StringBuffer button_sql = new StringBuffer();
			button_sql.append("INSERT INTO z_form_table_button ");
			button_sql.append(" (zid, ");
			button_sql.append(" pid, ");
			button_sql.append(" page_type, ");
			button_sql.append(" function_type, ");
			button_sql.append(" js_onclick, ");
			button_sql.append(" button_id, ");
			button_sql.append(" button_icon, ");
			button_sql.append(" button_name) ");
			button_sql.append(" VALUES ( ");
			button_sql.append(" '"+zid+"', ");
			button_sql.append(" '"+pid+"', ");
			button_sql.append(" 'list_card', ");
			button_sql.append(" '0', ");
			button_sql.append(" 'list_card_edit(this);', ");
			button_sql.append(" '"+button_id+"', ");
			button_sql.append(" 'fa fa-pencil-square-o', ");
			button_sql.append(" '修改') ");
			insert(button_sql.toString());
		}
		
	}


	private void CreateDetailListButton_add_detail_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_add_detail_button";
		if(ButtonNotExist(pid,button_id)){
			//新增
			StringBuffer add_button_sql = new StringBuffer();
			add_button_sql.append("INSERT INTO z_form_table_button ");
			add_button_sql.append(" (zid, ");
			add_button_sql.append(" pid, ");
			add_button_sql.append(" page_type, ");
			add_button_sql.append(" function_type, ");
			add_button_sql.append(" js_onclick, ");
			add_button_sql.append(" button_id, ");
			add_button_sql.append(" button_icon, ");
			add_button_sql.append(" button_name) ");
			add_button_sql.append(" VALUES ( ");
			add_button_sql.append(" '"+zid+"', ");
			add_button_sql.append(" '"+pid+"', ");
			add_button_sql.append(" 'list', ");
			add_button_sql.append(" '0', ");
			add_button_sql.append(" 'addDetail();', ");
			add_button_sql.append(" '"+button_id+"', ");
			add_button_sql.append(" 'fa fa-plus-square', ");
			add_button_sql.append(" '新增') ");
			insert(add_button_sql.toString());
		}
	}
	private void CreateDetailListButton_edit_detail_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_edit_detail_button";
		if(ButtonNotExist(pid,button_id)){
			//修改
			StringBuffer edit_button_sql = new StringBuffer();
			edit_button_sql.append("INSERT INTO z_form_table_button ");
			edit_button_sql.append(" (zid, ");
			edit_button_sql.append(" pid, ");
			edit_button_sql.append(" page_type, ");
			edit_button_sql.append(" function_type, ");
			edit_button_sql.append(" js_onclick, ");
			edit_button_sql.append(" button_id, ");
			edit_button_sql.append(" button_icon, ");
			edit_button_sql.append(" button_name) ");
			edit_button_sql.append(" VALUES ( ");
			edit_button_sql.append(" '"+zid+"', ");
			edit_button_sql.append(" '"+pid+"', ");
			edit_button_sql.append(" 'list', ");
			edit_button_sql.append(" '0', ");
			edit_button_sql.append(" 'updateDetail();', ");
			edit_button_sql.append(" '"+button_id+"', ");
			edit_button_sql.append(" 'fa fa-pencil-square', ");
			edit_button_sql.append(" '修改') ");
			insert(edit_button_sql.toString());
		}
	}
	private void CreateDetailListButton_remove_detail_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_remove_detail_button";
		if(ButtonNotExist(pid,button_id)){
			//删除
			StringBuffer remove_button_sql = new StringBuffer();
			remove_button_sql.append("INSERT INTO z_form_table_button ");
			remove_button_sql.append(" (zid, ");
			remove_button_sql.append(" pid, ");
			remove_button_sql.append(" page_type, ");
			remove_button_sql.append(" function_type, ");
			remove_button_sql.append(" js_onclick, ");
			remove_button_sql.append(" button_id, ");
			remove_button_sql.append(" button_icon, ");
			remove_button_sql.append(" button_name) ");
			remove_button_sql.append(" VALUES ( ");
			remove_button_sql.append(" '"+zid+"', ");
			remove_button_sql.append(" '"+pid+"', ");
			remove_button_sql.append(" 'list', ");
			remove_button_sql.append(" '1', ");
			remove_button_sql.append(" 'removeDetail();', ");
			remove_button_sql.append(" '"+button_id+"', ");
			remove_button_sql.append(" 'fa fa-minus-square', ");
			remove_button_sql.append(" '删除') ");
			insert(remove_button_sql.toString());
		}
	}
	private void CreateDetailListButton_look_detail_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_look_detail_button";
		if(ButtonNotExist(pid,button_id)){
			//删除
			StringBuffer remove_button_sql = new StringBuffer();
			remove_button_sql.append("INSERT INTO z_form_table_button ");
			remove_button_sql.append(" (zid, ");
			remove_button_sql.append(" pid, ");
			remove_button_sql.append(" page_type, ");
			remove_button_sql.append(" function_type, ");
			remove_button_sql.append(" js_onclick, ");
			remove_button_sql.append(" button_id, ");
			remove_button_sql.append(" button_icon, ");
			remove_button_sql.append(" button_name) ");
			remove_button_sql.append(" VALUES ( ");
			remove_button_sql.append(" '"+zid+"', ");
			remove_button_sql.append(" '"+pid+"', ");
			remove_button_sql.append(" 'list_look', ");
			remove_button_sql.append(" '1', ");
			remove_button_sql.append(" 'lookDetail();', ");
			remove_button_sql.append(" '"+button_id+"', ");
			remove_button_sql.append(" 'fa fa-eye', ");
			remove_button_sql.append(" '查看') ");
			insert(remove_button_sql.toString());
		}
	}
	private void CreateEditButton_save_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_save_button";
		if(ButtonNotExist(pid,button_id)){
			//保存
			StringBuffer save_button_sql = new StringBuffer();
			save_button_sql.append("INSERT INTO z_form_table_button ");
			save_button_sql.append(" (zid, ");
			save_button_sql.append(" pid, ");
			save_button_sql.append(" page_type, ");
			save_button_sql.append(" function_type, ");
			save_button_sql.append(" js_onclick, ");
			save_button_sql.append(" button_id, ");
			save_button_sql.append(" button_icon, ");
			save_button_sql.append(" button_name) ");
			save_button_sql.append(" VALUES ( ");
			save_button_sql.append(" '"+zid+"', ");
			save_button_sql.append(" '"+pid+"', ");
			save_button_sql.append(" 'edit', ");
			save_button_sql.append(" '1', ");
			save_button_sql.append(" 'SaveForm();', ");
			save_button_sql.append(" '"+button_id+"', ");
			save_button_sql.append(" 'fa fa-floppy-o', ");
			save_button_sql.append(" '保存') ");
			insert(save_button_sql.toString());
		}
	}
	private void CreateEditButton_save_and_add_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_save_and_add_button";
		if(ButtonNotExist(pid,button_id)){
			//保存并新增
			StringBuffer save_and_add_button_sql = new StringBuffer();
			save_and_add_button_sql.append("INSERT INTO z_form_table_button ");
			save_and_add_button_sql.append(" (zid, ");
			save_and_add_button_sql.append(" pid, ");
			save_and_add_button_sql.append(" page_type, ");
			save_and_add_button_sql.append(" function_type, ");
			save_and_add_button_sql.append(" js_onclick, ");
			save_and_add_button_sql.append(" button_id, ");
			save_and_add_button_sql.append(" button_icon, ");
			save_and_add_button_sql.append(" button_name) ");
			save_and_add_button_sql.append(" VALUES ( ");
			save_and_add_button_sql.append(" '"+zid+"', ");
			save_and_add_button_sql.append(" '"+pid+"', ");
			save_and_add_button_sql.append(" 'edit', ");
			save_and_add_button_sql.append(" '1', ");
			save_and_add_button_sql.append(" 'SaveAndAddForm();', ");
			save_and_add_button_sql.append(" '"+button_id+"', ");
			save_and_add_button_sql.append(" 'fa fa-clipboard', ");
			save_and_add_button_sql.append(" '保存并新增') ");
			insert(save_and_add_button_sql.toString());
		}
	}
	private void CreateEditButton_save_and_return_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_save_and_return_button";
		if(ButtonNotExist(pid,button_id)){
			//保存并返回
			StringBuffer save_and_return_button_sql = new StringBuffer();
			save_and_return_button_sql.append("INSERT INTO z_form_table_button ");
			save_and_return_button_sql.append(" (zid, ");
			save_and_return_button_sql.append(" pid, ");
			save_and_return_button_sql.append(" page_type, ");
			save_and_return_button_sql.append(" function_type, ");
			save_and_return_button_sql.append(" js_onclick, ");
			save_and_return_button_sql.append(" button_id, ");
			save_and_return_button_sql.append(" button_icon, ");
			save_and_return_button_sql.append(" button_name) ");
			save_and_return_button_sql.append(" VALUES ( ");
			save_and_return_button_sql.append(" '"+zid+"', ");
			save_and_return_button_sql.append(" '"+pid+"', ");
			save_and_return_button_sql.append(" 'edit', ");
			save_and_return_button_sql.append(" '1', "); 
			save_and_return_button_sql.append(" 'SaveAndRetrunForm();', ");
			save_and_return_button_sql.append(" '"+button_id+"', ");
			save_and_return_button_sql.append(" 'fa fa-reply-all', ");
			save_and_return_button_sql.append(" '保存并返回') ");
			insert(save_and_return_button_sql.toString());
		}
	}

	private void CreateEditButton_return_parent_page_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_return_parent_page_button";
		if(ButtonNotExist(pid,button_id)){
			//返回
			StringBuffer return_parent_page_button_sql = new StringBuffer();
			return_parent_page_button_sql.append("INSERT INTO z_form_table_button ");
			return_parent_page_button_sql.append(" (zid, ");
			return_parent_page_button_sql.append(" pid, ");
			return_parent_page_button_sql.append(" page_type, ");
			return_parent_page_button_sql.append(" function_type, ");
			return_parent_page_button_sql.append(" js_onclick, ");
			return_parent_page_button_sql.append(" button_id, ");
			return_parent_page_button_sql.append(" button_icon, ");
			return_parent_page_button_sql.append(" button_name) ");
			return_parent_page_button_sql.append(" VALUES ( ");
			return_parent_page_button_sql.append(" '"+zid+"', ");
			return_parent_page_button_sql.append(" '"+pid+"', ");
			return_parent_page_button_sql.append(" 'edit', ");
			return_parent_page_button_sql.append(" '0', ");
			return_parent_page_button_sql.append(" 'returnParentPage();', ");
			return_parent_page_button_sql.append(" '"+button_id+"', ");
			return_parent_page_button_sql.append(" 'fa fa-hand-o-left', ");
			return_parent_page_button_sql.append(" '返回') ");
			insert(return_parent_page_button_sql.toString());
		}
	}

	private void CreateEditButton_oa_submit_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_oa_submit_button";
		if(ButtonNotExist(pid,button_id)){
			//提交审批
			StringBuffer oa_submit_button_sql = new StringBuffer();
			oa_submit_button_sql.append("INSERT INTO z_form_table_button ");
			oa_submit_button_sql.append(" (zid, ");
			oa_submit_button_sql.append(" pid, ");
			oa_submit_button_sql.append(" page_type, ");
			oa_submit_button_sql.append(" function_type, ");
			oa_submit_button_sql.append(" js_onclick, ");
			oa_submit_button_sql.append(" button_id, ");
			oa_submit_button_sql.append(" button_icon, ");
			oa_submit_button_sql.append(" is_hidden, ");
			oa_submit_button_sql.append(" button_name) ");
			oa_submit_button_sql.append(" VALUES ( ");
			oa_submit_button_sql.append(" '"+zid+"', ");
			oa_submit_button_sql.append(" '"+pid+"', ");
			oa_submit_button_sql.append(" 'edit', ");
			oa_submit_button_sql.append(" '1', ");
			oa_submit_button_sql.append(" 'oa_submit();', ");
			oa_submit_button_sql.append(" '"+button_id+"', ");
			oa_submit_button_sql.append(" 'fa fa-users', ");
			oa_submit_button_sql.append(" '1', ");
			oa_submit_button_sql.append(" '提交申请') ");
			insert(oa_submit_button_sql.toString());
		}
	}

	private void CreateLookButton_return_parent_page_button(HashMap<String, String> bean) {
		String zid = z.newZid("z_form_table_button");
		String pid = String.valueOf(bean.get("zid"));
		String button_id = bean.get("table_id")+"_look_return_parent_page_button";
		if(ButtonNotExist(pid,button_id)){
			//返回
			StringBuffer return_parent_page_button_sql = new StringBuffer();
			return_parent_page_button_sql.append("INSERT INTO z_form_table_button ");
			return_parent_page_button_sql.append(" (zid, ");
			return_parent_page_button_sql.append(" pid, ");
			return_parent_page_button_sql.append(" page_type, ");
			return_parent_page_button_sql.append(" function_type, ");
			return_parent_page_button_sql.append(" js_onclick, ");
			return_parent_page_button_sql.append(" button_id, ");
			return_parent_page_button_sql.append(" button_icon, ");
			return_parent_page_button_sql.append(" button_name) ");
			return_parent_page_button_sql.append(" VALUES ( ");
			return_parent_page_button_sql.append(" '"+zid+"', ");
			return_parent_page_button_sql.append(" '"+pid+"', ");
			return_parent_page_button_sql.append(" 'look', ");
			return_parent_page_button_sql.append(" '0', ");
			return_parent_page_button_sql.append(" 'returnParentPage();', ");
			return_parent_page_button_sql.append(" '"+button_id+"', ");
			return_parent_page_button_sql.append(" 'fa fa-hand-o-left', ");
			return_parent_page_button_sql.append(" '返回') ");
			insert(return_parent_page_button_sql.toString());
		}

	}

	private void CreateColumn_zid(HashMap<String, String> bean) throws Exception {
		//zid
		StringBuffer zidsql = new StringBuffer();
		zidsql.append("INSERT INTO z_form_table_column ( ");
		zidsql.append("   zid, ");
		zidsql.append("   pid, ");
		zidsql.append("   column_id, ");
		zidsql.append("   column_name, ");
		zidsql.append("   column_type, ");
		zidsql.append("   column_length_db, ");
		zidsql.append("   column_size, ");
		zidsql.append("   colunm_length_list, ");
		zidsql.append("   is_null, ");
		zidsql.append("   is_hidden, ");
		zidsql.append("   is_readonly ");
		zidsql.append(" )  ");
		zidsql.append(" VALUES ");
		zidsql.append("   ( ");
		zidsql.append("     '"+z.newZid("z_form_table_column")+"', ");
		zidsql.append("     '"+String.valueOf(bean.get("zid"))+"', ");
		zidsql.append("     'zid', ");
		zidsql.append("     '主健', ");
		zidsql.append("     '0', ");
		zidsql.append("     '128', ");
		zidsql.append("     '3', ");
		zidsql.append("     '150', ");
		zidsql.append("     '1', ");
		zidsql.append("     '1', ");
		zidsql.append("     '1' ");
		zidsql.append("   ) ; ");
		insert(zidsql.toString());
	}

	private void CreateColumn_number(HashMap<String, String> bean) throws Exception {
		//zid
		StringBuffer zidsql = new StringBuffer();
		zidsql.append("INSERT INTO z_form_table_column ( ");
		zidsql.append("   zid, ");
		zidsql.append("   pid, ");
		zidsql.append("   column_id, ");
		zidsql.append("   column_name, ");
		zidsql.append("   column_type, ");
		zidsql.append("   column_length_db, ");
		zidsql.append("   column_size, ");
		zidsql.append("   colunm_length_list, ");
		zidsql.append("   is_null, ");
		zidsql.append("   is_hidden, ");
		zidsql.append("   is_readonly ");
		zidsql.append(" )  ");
		zidsql.append(" VALUES ");
		zidsql.append("   ( ");
		zidsql.append("     '"+z.newZid("z_form_table_column")+"', ");
		zidsql.append("     '"+String.valueOf(bean.get("zid"))+"', ");
		zidsql.append("     'number', ");
		zidsql.append("     '编号', ");
		zidsql.append("     '0', ");
		zidsql.append("     '128', ");
		zidsql.append("     '3', ");
		zidsql.append("     '150', ");
		zidsql.append("     '0', ");
		zidsql.append("     '1', ");
		zidsql.append("     '0' ");
		zidsql.append("   ) ; ");
		insert(zidsql.toString());
	}
	private void CreateColumn_name(HashMap<String, String> bean) throws Exception {
		//zid
		StringBuffer zidsql = new StringBuffer();
		zidsql.append("INSERT INTO z_form_table_column ( ");
		zidsql.append("   zid, ");
		zidsql.append("   pid, ");
		zidsql.append("   column_id, ");
		zidsql.append("   column_name, ");
		zidsql.append("   column_type, ");
		zidsql.append("   column_length_db, ");
		zidsql.append("   column_size, ");
		zidsql.append("   colunm_length_list, ");
		zidsql.append("   is_null, ");
		zidsql.append("   is_hidden, ");
		zidsql.append("   is_readonly ");
		zidsql.append(" )  ");
		zidsql.append(" VALUES ");
		zidsql.append("   ( ");
		zidsql.append("     '"+z.newZid("z_form_table_column")+"', ");
		zidsql.append("     '"+String.valueOf(bean.get("zid"))+"', ");
		zidsql.append("     'name', ");
		zidsql.append("     '名称', ");
		zidsql.append("     '0', ");
		zidsql.append("     '128', ");
		zidsql.append("     '3', ");
		zidsql.append("     '200', ");
		zidsql.append("     '0', ");
		zidsql.append("     '1', ");
		zidsql.append("     '0' ");
		zidsql.append("   ) ; ");
		insert(zidsql.toString());
	}
	private void CreateColumn_pid(HashMap<String, String> bean)  throws Exception {
		StringBuffer seqsql = new StringBuffer();
		seqsql.append("INSERT INTO z_form_table_column ( ");
		seqsql.append("   zid, ");
		seqsql.append("   pid, ");
		seqsql.append("   column_id, ");
		seqsql.append("   column_name, ");
		seqsql.append("   column_type, ");
		seqsql.append("   column_length_db, ");
		seqsql.append("   column_size, ");
		seqsql.append("   colunm_length_list, ");
		seqsql.append("   is_null, ");
		seqsql.append("   is_hidden, ");
		seqsql.append("   is_readonly ");
		seqsql.append(" )  ");
		seqsql.append(" VALUES ");
		seqsql.append("   ( ");
		seqsql.append("     '"+z.newZid("z_form_table_column")+"', ");
		seqsql.append("     '"+String.valueOf(bean.get("zid"))+"', ");
		seqsql.append("     'pid', ");
		seqsql.append("     '主表主健', ");
		seqsql.append("     '0', ");
		seqsql.append("     '128', ");
		seqsql.append("     '3', ");
		seqsql.append("     '150', ");
		seqsql.append("     '1', ");
		seqsql.append("     '1', ");
		seqsql.append("     '1' ");
		seqsql.append("   ) ; ");
		insert(seqsql.toString());
	}
	private void CreateColumn_seq(HashMap<String, String> bean) throws Exception  {

		//序号
		StringBuffer seqsql = new StringBuffer();
		seqsql.append("INSERT INTO z_form_table_column ( ");
		seqsql.append("   zid, ");
		seqsql.append("   pid, ");
		seqsql.append("   column_id, ");
		seqsql.append("   column_name, ");
		seqsql.append("   column_type, ");
		seqsql.append("   column_length_db, ");
		seqsql.append("   column_size, ");
		seqsql.append("   colunm_length_list, ");
		seqsql.append("   is_null, ");
		seqsql.append("   is_hidden, ");
		seqsql.append("   is_readonly ");
		seqsql.append(" )  ");
		seqsql.append(" VALUES ");
		seqsql.append("   ( ");
		seqsql.append("     '"+z.newZid("z_form_table_column")+"', ");
		seqsql.append("     '"+String.valueOf(bean.get("zid"))+"', ");
		seqsql.append("     'seq', ");
		seqsql.append("     '序号', ");
		seqsql.append("     '2', ");
		seqsql.append("     '10', ");
		seqsql.append("     '3', ");
		seqsql.append("     '50', ");
		seqsql.append("     '1', ");
		seqsql.append("     '1', ");
		seqsql.append("     '1' ");
		seqsql.append("   ) ; ");
		insert(seqsql.toString());
	}
	private void CreateColumn_create_user(HashMap<String, String> bean)  throws Exception {
		//创建者
		StringBuffer create_usersql = new StringBuffer();
		create_usersql.append("INSERT INTO z_form_table_column ( ");
		create_usersql.append("   zid, ");
		create_usersql.append("   pid, ");
		create_usersql.append("   column_id, ");
		create_usersql.append("   column_name, ");
		create_usersql.append("   column_type, ");
		create_usersql.append("   column_length_db, ");
		create_usersql.append("   column_size, ");
		create_usersql.append("   colunm_length_list, ");

		create_usersql.append("   z5_table, ");
		create_usersql.append("   z5_key, ");
		create_usersql.append("   z5_value, ");
		create_usersql.append("   column_default_type, ");

		create_usersql.append("   is_null, ");
		create_usersql.append("   is_hidden, ");
		create_usersql.append("   is_readonly ");
		create_usersql.append(" )  ");
		create_usersql.append(" VALUES ");
		create_usersql.append("   ( ");
		create_usersql.append("     '"+z.newZid("z_form_table_column")+"', ");
		create_usersql.append("     '"+String.valueOf(bean.get("zid"))+"', ");
		create_usersql.append("     'create_user', ");
		create_usersql.append("     '创建者', ");
		create_usersql.append("     '8', ");
		create_usersql.append("     '128', ");
		create_usersql.append("     '3', ");
		create_usersql.append("     '150', ");

		create_usersql.append("   'z_user', ");
		create_usersql.append("   'zid', ");
		create_usersql.append("   'user_name', ");
		create_usersql.append("   '3', ");

		create_usersql.append("     '0', ");
		create_usersql.append("     '1', ");
		create_usersql.append("     '1' ");
		create_usersql.append("   ) ; ");
		insert(create_usersql.toString());
	}
	private void CreateColumn_create_time(HashMap<String, String> bean)  throws Exception {
		//创建时间
		StringBuffer create_timesql = new StringBuffer();
		create_timesql.append("INSERT INTO z_form_table_column ( ");
		create_timesql.append("   zid, ");
		create_timesql.append("   pid, ");
		create_timesql.append("   column_id, ");
		create_timesql.append("   column_name, ");
		create_timesql.append("   column_type, ");
		create_timesql.append("   column_size, ");
		create_timesql.append("   colunm_length_list, ");
		create_timesql.append("   is_null, ");
		create_timesql.append("   is_hidden, ");
		create_timesql.append("   is_readonly ");
		create_timesql.append(" )  ");
		create_timesql.append(" VALUES ");
		create_timesql.append("   ( ");
		create_timesql.append("     '"+z.newZid("z_form_table_column")+"', ");
		create_timesql.append("     '"+String.valueOf(bean.get("zid"))+"', ");
		create_timesql.append("     'create_time', ");
		create_timesql.append("     '创建时间', ");
		create_timesql.append("     '10', ");
		create_timesql.append("     '3', ");
		create_timesql.append("     '150', ");
		create_timesql.append("     '0', ");
		create_timesql.append("     '1', ");
		create_timesql.append("     '1' ");
		create_timesql.append("   ) ; ");
		insert(create_timesql.toString());
	}
	private void CreateColumn_update_user(HashMap<String, String> bean)  throws Exception {

		//修改者
		StringBuffer update_usersql = new StringBuffer();
		update_usersql.append("INSERT INTO z_form_table_column ( ");
		update_usersql.append("   zid, ");
		update_usersql.append("   pid, ");
		update_usersql.append("   column_id, ");
		update_usersql.append("   column_name, ");
		update_usersql.append("   column_type, ");
		update_usersql.append("   column_length_db, ");
		update_usersql.append("   column_size, ");
		update_usersql.append("   colunm_length_list, ");

		update_usersql.append("   z5_table, ");
		update_usersql.append("   z5_key, ");
		update_usersql.append("   z5_value, ");
		update_usersql.append("   column_default_type, ");

		update_usersql.append("   is_null, ");
		update_usersql.append("   is_hidden, ");
		update_usersql.append("   is_readonly ");
		update_usersql.append(" )  ");
		update_usersql.append(" VALUES ");
		update_usersql.append("   ( ");
		update_usersql.append("     '"+z.newZid("z_form_table_column")+"', ");
		update_usersql.append("     '"+String.valueOf(bean.get("zid"))+"', ");
		update_usersql.append("     'update_user', ");
		update_usersql.append("     '修改者', ");
		update_usersql.append("     '8', ");
		update_usersql.append("     '128', ");
		update_usersql.append("     '3', ");
		update_usersql.append("     '150', ");

		update_usersql.append("   'z_user', ");
		update_usersql.append("   'zid', ");
		update_usersql.append("   'user_name', ");
		update_usersql.append("   '3', ");

		update_usersql.append("     '0', ");
		update_usersql.append("     '1', ");
		update_usersql.append("     '1' ");
		update_usersql.append("   ) ; ");
		insert(update_usersql.toString());
	}
	private void CreateColumn_update_time(HashMap<String, String> bean)  throws Exception {
		//修改时间
		StringBuffer update_timesql = new StringBuffer();
		update_timesql.append("INSERT INTO z_form_table_column ( ");
		update_timesql.append("   zid, ");
		update_timesql.append("   pid, ");
		update_timesql.append("   column_id, ");
		update_timesql.append("   column_name, ");
		update_timesql.append("   column_type, ");
		update_timesql.append("   column_size, ");
		update_timesql.append("   colunm_length_list, ");
		update_timesql.append("   is_null, ");
		update_timesql.append("   is_hidden, ");
		update_timesql.append("   is_readonly ");
		update_timesql.append(" )  ");
		update_timesql.append(" VALUES ");
		update_timesql.append("   ( ");
		update_timesql.append("     '"+z.newZid("z_form_table_column")+"', ");
		update_timesql.append("     '"+String.valueOf(bean.get("zid"))+"', ");
		update_timesql.append("     'update_time', ");
		update_timesql.append("     '修改时间', ");
		update_timesql.append("     '10', ");
		update_timesql.append("     '3', ");
		update_timesql.append("     '150', ");
		update_timesql.append("     '0', ");
		update_timesql.append("     '1', ");
		update_timesql.append("     '1' ");
		update_timesql.append("   ) ; ");
		insert(update_timesql.toString());
	}
	private void CreateColumn_zversion(HashMap<String, String> bean) throws Exception  {
		//版本号
		StringBuffer zversionsql = new StringBuffer();
		zversionsql.append("INSERT INTO z_form_table_column ( ");
		zversionsql.append("   zid, ");
		zversionsql.append("   pid, ");
		zversionsql.append("   column_id, ");
		zversionsql.append("   column_name, ");
		zversionsql.append("   column_type, ");
		zversionsql.append("   column_length_db, ");
		zversionsql.append("   column_size, ");
		zversionsql.append("   colunm_length_list, ");
		zversionsql.append("   is_null, ");
		zversionsql.append("   is_hidden, ");
		zversionsql.append("   is_readonly ");
		zversionsql.append(" )  ");
		zversionsql.append(" VALUES ");
		zversionsql.append("   ( ");
		zversionsql.append("     '"+z.newZid("z_form_table_column")+"', ");
		zversionsql.append("     '"+String.valueOf(bean.get("zid"))+"', ");
		zversionsql.append("     'zversion', ");
		zversionsql.append("     '版本号', ");
		zversionsql.append("     '2', ");
		zversionsql.append("     '10', ");
		zversionsql.append("     '3', ");
		zversionsql.append("     '50', ");
		zversionsql.append("     '1', ");
		zversionsql.append("     '1', ");
		zversionsql.append("     '1' ");
		zversionsql.append("   ) ; ");
		insert(zversionsql.toString());
	}
	private void CreateColumn_orgid(HashMap<String, String> bean)  throws Exception {
		//所属组织
		StringBuffer orgidsql = new StringBuffer();
		orgidsql.append("INSERT INTO z_form_table_column ( ");
		orgidsql.append("   zid, ");
		orgidsql.append("   pid, ");
		orgidsql.append("   column_id, ");
		orgidsql.append("   column_name, ");
		orgidsql.append("   column_type, ");
		orgidsql.append("   column_length_db, ");
		orgidsql.append("   column_size, ");
		orgidsql.append("   colunm_length_list, ");

		orgidsql.append("   z5_table, ");
		orgidsql.append("   z5_key, ");
		orgidsql.append("   z5_value, ");
		orgidsql.append("   column_default_type, ");

		orgidsql.append("   is_null, ");
		orgidsql.append("   is_hidden, ");
		orgidsql.append("   is_readonly ");
		orgidsql.append(" )  ");
		orgidsql.append(" VALUES ");
		orgidsql.append("   ( ");
		orgidsql.append("     '"+z.newZid("z_form_table_column")+"', ");
		orgidsql.append("     '"+String.valueOf(bean.get("zid"))+"', ");
		orgidsql.append("     'orgid', ");
		orgidsql.append("     '所属组织', ");
		orgidsql.append("     '8', ");
		orgidsql.append("     '128', ");
		orgidsql.append("     '3', ");
		orgidsql.append("     '150', ");

		orgidsql.append("   'z_org', ");
		orgidsql.append("   'zid', ");
		orgidsql.append("   'full_org_name', ");
		orgidsql.append("   '4', ");

		orgidsql.append("     '0', ");
		orgidsql.append("     '1', ");
		orgidsql.append("     '1' ");
		orgidsql.append("   ) ; ");
		insert(orgidsql.toString());
	}
	private void CreateColumn_remarks(HashMap<String, String> bean) throws Exception  {
		//备注
		StringBuffer remarkssql = new StringBuffer();
		remarkssql.append("INSERT INTO z_form_table_column ( ");
		remarkssql.append("   zid, ");
		remarkssql.append("   pid, ");
		remarkssql.append("   column_id, ");
		remarkssql.append("   column_name, ");
		remarkssql.append("   column_type, ");
		remarkssql.append("   column_size, ");
		remarkssql.append("   colunm_length_list, ");
		remarkssql.append("   is_null, ");
		remarkssql.append("   is_hidden, ");
		remarkssql.append("   is_readonly ");
		remarkssql.append(" )  ");
		remarkssql.append(" VALUES ");
		remarkssql.append("   ( ");
		remarkssql.append("     '"+z.newZid("z_form_table_column")+"', ");
		remarkssql.append("     '"+String.valueOf(bean.get("zid"))+"', ");
		remarkssql.append("     'remarks', ");
		remarkssql.append("     '备注', ");
		remarkssql.append("     '1', ");
		remarkssql.append("     '12', ");
		remarkssql.append("     '150', ");
		remarkssql.append("     '0', ");
		remarkssql.append("     '1', ");
		remarkssql.append("     '0' ");
		remarkssql.append("   ) ; ");
		insert(remarkssql.toString());
	}

	public void CreateMenu(HashMap<String, String> bean) throws Exception  {
		String parent_table_id = String.valueOf(bean.get("parent_table_id"));
		//只有创建主表才创建菜单
		if(z.isNull(parent_table_id)) {
			String form_zid = String.valueOf(bean.get("pid"));
			String table_id = String.valueOf(bean.get("table_id"));
			String table_title = String.valueOf(bean.get("table_title"));
			String list_action = String.valueOf(bean.get("list_action"));
			int MenuCount = selectInt("SELECT COUNT(*) FROM z_menu WHERE url = '"+list_action+"?tableId="+table_id+"' and menu_type='frommenu'");
			if(MenuCount==0) {
				StringBuffer menusql = new StringBuffer();
				menusql.append("INSERT INTO z_menu(zid,parentid,name,url,menu_type,formid,menu_icon) VALUES");
				String zid = z.newZid("z_menu");
				menusql.append(" ('"+zid+"','00000000000000000000000000000000','"+table_title+"','"+list_action+"?tableId="+table_id+"','frommenu','"+form_zid+"','fa fa-file-text-o'); ");
				insert(menusql.toString());
				//根据FormId添加关联Button
				AddButtons(zid,form_zid);
			}
		}

	}

	/**
	 * 根据FormId添加关联Button
	 * @param pid 主表ID
	 * @param formid 表单ID
	 * @throws Exception
	 */
	public void AddButtons(String pid,String formid) throws Exception{

		//获取表单所有功能按钮
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		sql.append("  zft.zid tableid,");
		sql.append("  zftb.page_type,");
		sql.append("  zftb.zid buttonId ");
		sql.append("FROM z_form zf ");
		sql.append("INNER JOIN z_form_table zft  ON zf.zid = zft.pid ");
		sql.append("INNER JOIN z_form_table_button zftb  ON zft.zid = zftb.pid ");
		sql.append("WHERE zf.zid = '"+formid+"' ");
		List<Map<String,String>> buttons = selectList(sql.toString());

		//插入菜单关联Button表
		for (Map<String, String> buttonMap : buttons) {
			String z_menu_form_button_zid = z.newZid("z_menu_form_button");//明细表ID
			String tableid = buttonMap.get("tableid");//表ID
			String page_type = buttonMap.get("page_type");//页面类型
			String buttonId = buttonMap.get("buttonId");//按钮ZID

			StringBuffer insertsql = new StringBuffer();
			insertsql.append("INSERT INTO z_menu_form_button (");
			insertsql.append("  zid,");
			insertsql.append("  pid,");
			insertsql.append("  tableid,");
			insertsql.append("  page_type,");
			insertsql.append("  buttonId");
			insertsql.append(") VALUES(");
			insertsql.append("'"+z_menu_form_button_zid+"',");
			insertsql.append("'"+pid+"',");
			insertsql.append("'"+tableid+"',");
			insertsql.append("'"+page_type+"',");
			insertsql.append("'"+buttonId+"'");
			insertsql.append(") ");
			insert(insertsql.toString());
		}

	}

	/**
	 * 根据数据库字段类型生成java字段类型
	 * @param DbColumnType
	 * @return
	 */
	protected static String getColumnTypeDB(String DbColumnType,String ColumnLength){
		if("0".equals(DbColumnType)) {//文本
			if("".equals(ColumnLength) || ColumnLength == null) {
				ColumnLength = "64";
			}
			return "varchar("+ColumnLength+")";
		}else if("1".equals(DbColumnType)) {//多行文本
			return "longtext";
		}else if("2".equals(DbColumnType)) {//数字
			if("".equals(ColumnLength) || ColumnLength == null) {
				ColumnLength = "20";
			}
			return "decimal("+ColumnLength+")";
		}else if("3".equals(DbColumnType)) {//文件
			return "text";
		}else if("4".equals(DbColumnType)) {//图片
			return "text";
		}else if("5".equals(DbColumnType)) {//多选
			return "char(32)";
		}else if("6".equals(DbColumnType)) {//单选
			return "char(16)";
		}else if("7".equals(DbColumnType)) {//下拉框
			return "char(16)";
		}else if("8".equals(DbColumnType)) {//Z5
			return "varchar(32)";
		}else if("9".equals(DbColumnType)) {//日期
			return "datetime";
		}else if("10".equals(DbColumnType)) {//日期时间
			return "datetime";
		}else if("11".equals(DbColumnType)) {//HTML输入框
			return "longtext";
		}else if("12".equals(DbColumnType)) {//源码输入框
			return "longtext";
		}else {//默认类型
			return "varchar(128)";
		}
	}

	/**
	 * 数据库是否为空
	 * @param isNull
	 * @return
	 */
	protected static String getColumnIsNull(String isNull){
		if("1".equals(isNull)){
			return "NOT NULL";
		}else{
			return "DEFAULT NULL";
		}
	}

	/**
	 * 检查表单是否创建多个主表
	 * @param bean
	 */
	private void CheckIsCTablc(HashMap<String, String> bean) throws Exception{
		String parent_table_id = bean.get("parent_table_id");
		String pid = bean.get("pid");
		if(z.isNull(parent_table_id)) {
			//本次新增的是主表-----判读当前Form是否创建过主表
			int count = selectInt("SELECT COUNT(*) FROM z_form_table WHERE pid = '"+pid+"'");
			if(count!=0) {
				z.Exception("当前表单已创建主表，表单只能创建一个主表。请选择上级表ID。");
			}
		}
	}

}
