package com.futvan.z.system.zhttpservices;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_http_services_parameter extends SuperBean{
	//标识标题
	private String title;

	//接口参数类型
	private String services_parameter_type;

	/**
	* get标识标题
	* @return title
	*/
	public String getTitle() {
		return title;
  	}

	/**
	* set标识标题
	* @return title
	*/
	public void setTitle(String title) {
		this.title = title;
 	}

	/**
	* get接口参数类型
	* @return services_parameter_type
	*/
	public String getServices_parameter_type() {
		return services_parameter_type;
  	}

	/**
	* set接口参数类型
	* @return services_parameter_type
	*/
	public void setServices_parameter_type(String services_parameter_type) {
		this.services_parameter_type = services_parameter_type;
 	}

}
