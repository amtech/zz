package com.futvan.z.system.zhttpservices;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_http_services extends SuperBean{
	//接口标识
	private String serviceid;

	//接口标题
	private String title;

	//接口类型
	private String http_services_type;

	//操作类型
	private String http_services_operating_type;

	//所属项目
	private String projectid;

	//是否启用
	private String isenable;

	//SQL语句
	private String sqlinfo;

	//接口参数
	private List<z_http_services_parameter> z_http_services_parameter_list;

	/**
	* get接口标识
	* @return serviceid
	*/
	public String getServiceid() {
		return serviceid;
  	}

	/**
	* set接口标识
	* @return serviceid
	*/
	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
 	}

	/**
	* get接口标题
	* @return title
	*/
	public String getTitle() {
		return title;
  	}

	/**
	* set接口标题
	* @return title
	*/
	public void setTitle(String title) {
		this.title = title;
 	}

	/**
	* get接口类型
	* @return http_services_type
	*/
	public String getHttp_services_type() {
		return http_services_type;
  	}

	/**
	* set接口类型
	* @return http_services_type
	*/
	public void setHttp_services_type(String http_services_type) {
		this.http_services_type = http_services_type;
 	}

	/**
	* get操作类型
	* @return http_services_operating_type
	*/
	public String getHttp_services_operating_type() {
		return http_services_operating_type;
  	}

	/**
	* set操作类型
	* @return http_services_operating_type
	*/
	public void setHttp_services_operating_type(String http_services_operating_type) {
		this.http_services_operating_type = http_services_operating_type;
 	}

	/**
	* get所属项目
	* @return projectid
	*/
	public String getProjectid() {
		return projectid;
  	}

	/**
	* set所属项目
	* @return projectid
	*/
	public void setProjectid(String projectid) {
		this.projectid = projectid;
 	}

	/**
	* get是否启用
	* @return isenable
	*/
	public String getIsenable() {
		return isenable;
  	}

	/**
	* set是否启用
	* @return isenable
	*/
	public void setIsenable(String isenable) {
		this.isenable = isenable;
 	}

	/**
	* getSQL语句
	* @return sqlinfo
	*/
	public String getSqlinfo() {
		return sqlinfo;
  	}

	/**
	* setSQL语句
	* @return sqlinfo
	*/
	public void setSqlinfo(String sqlinfo) {
		this.sqlinfo = sqlinfo;
 	}

	/**
	* get接口参数
	* @return 接口参数
	*/
	public List<z_http_services_parameter> getZ_http_services_parameter_list() {
		return z_http_services_parameter_list;
  	}

	/**
	* set接口参数
	* @return 接口参数
	*/
	public void setZ_http_services_parameter_list(List<z_http_services_parameter> z_http_services_parameter_list) {
		this.z_http_services_parameter_list = z_http_services_parameter_list;
 	}

}
