package com.futvan.z.system.zworkflow;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_workflow_node_branch extends SuperBean{
	//条件字段
	private String columnid;

	//比较符
	private String column_compare;

	//比较值
	private String column_value;

	//连接符
	private String connector_type;

	//比较符2
	private String column_compare2;

	//比较值2
	private String column_value2;

	//下一节点
	private String nodeid;

	/**
	* get条件字段
	* @return columnid
	*/
	public String getColumnid() {
		return columnid;
  	}

	/**
	* set条件字段
	* @return columnid
	*/
	public void setColumnid(String columnid) {
		this.columnid = columnid;
 	}

	/**
	* get比较符
	* @return column_compare
	*/
	public String getColumn_compare() {
		return column_compare;
  	}

	/**
	* set比较符
	* @return column_compare
	*/
	public void setColumn_compare(String column_compare) {
		this.column_compare = column_compare;
 	}

	/**
	* get比较值
	* @return column_value
	*/
	public String getColumn_value() {
		return column_value;
  	}

	/**
	* set比较值
	* @return column_value
	*/
	public void setColumn_value(String column_value) {
		this.column_value = column_value;
 	}

	/**
	* get连接符
	* @return connector_type
	*/
	public String getConnector_type() {
		return connector_type;
  	}

	/**
	* set连接符
	* @return connector_type
	*/
	public void setConnector_type(String connector_type) {
		this.connector_type = connector_type;
 	}

	/**
	* get比较符2
	* @return column_compare2
	*/
	public String getColumn_compare2() {
		return column_compare2;
  	}

	/**
	* set比较符2
	* @return column_compare2
	*/
	public void setColumn_compare2(String column_compare2) {
		this.column_compare2 = column_compare2;
 	}

	/**
	* get比较值2
	* @return column_value2
	*/
	public String getColumn_value2() {
		return column_value2;
  	}

	/**
	* set比较值2
	* @return column_value2
	*/
	public void setColumn_value2(String column_value2) {
		this.column_value2 = column_value2;
 	}

	/**
	* get下一节点
	* @return nodeid
	*/
	public String getNodeid() {
		return nodeid;
  	}

	/**
	* set下一节点
	* @return nodeid
	*/
	public void setNodeid(String nodeid) {
		this.nodeid = nodeid;
 	}

}
