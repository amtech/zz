package com.futvan.z.system.zdb;
import java.util.HashMap;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
@Controller
public class Z_dbZ_db_save_buttonButtonAction extends SuperAction{

	@Autowired
	private CommonService commonService;

	@RequestMapping(value="/z_db_save_button")
	public @ResponseBody Result z_db_save_button(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();
		String db_type = bean.get("db_type");
		String dbip = bean.get("dbip");
		String db_port = bean.get("db_port");
		String db_name = bean.get("db_name");
		String PageType = bean.get("PageType");
		String url_extend = bean.get("url_extend");
		if(z.isNotNull(db_type)) {
			if(z.isNotNull(dbip)) {
				if(z.isNotNull(db_port)) {
					if(z.isNotNull(db_name)) {
						if(z.isNotNull(PageType)) {
							if("add".equals(bean.get("PageType"))) {
								if(z.isNull(url_extend)) {
									url_extend = "";
								}else {
									url_extend = "?"+url_extend;
								}
								String url = "";
								if("mysql".equals(db_type)) {
									url = "jdbc:"+db_type+"://"+dbip+":"+db_port+"/"+db_name+url_extend;
								}
								if("oracle".equals(db_type)) {
									url = "jdbc:"+db_type+":thin:@"+dbip+":"+db_port+"/"+db_name+url_extend;
								}
								bean.put("url", url);
								result = commonService.insert(bean, request);
							}else if("edit".equals(bean.get("PageType"))) {
								if(z.isNull(url_extend)) {
									url_extend = "";
								}else {
									url_extend = "?"+url_extend;
								}
								String url = "";
								if("mysql".equals(db_type)) {
									url = "jdbc:"+db_type+"://"+dbip+":"+db_port+"/"+db_name+url_extend;
								}
								if("oracle".equals(db_type)) {
									url = "jdbc:"+db_type+":thin:@"+dbip+":"+db_port+"/"+db_name+url_extend;
								}
								bean.put("url", url);
								result = commonService.update(bean, request);
							}else {
								z.Exception("z_db_save_button error | 页面类型参数不正确："+bean.get("PageType"));
							}
						}else {
							result.setCode(Code.ERROR);
							result.setMsg("页面编辑类型不能为空");
						}
					}else {
						result.setCode(Code.ERROR);
						result.setMsg("数据库连接实例名称不能为空");
					}
				}else {
					result.setCode(Code.ERROR);
					result.setMsg("数据库连接端口不能为空");
				}
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("数据库连接地址不能为空");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("数据库类型不能为空");
		}
		return result;
	}
}
