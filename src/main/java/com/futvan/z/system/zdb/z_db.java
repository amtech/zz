package com.futvan.z.system.zdb;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_db extends SuperBean{
	//数据库标识
	private String dbid;

	//数据库类型
	private String db_type;

	//数据库连接驱动
	private String driverClassName;

	//数据库连接地址
	private String dbip;

	//连接端口号
	private String db_port;

	//数据库实例名称
	private String db_name;

	//账号
	private String username;

	//密码
	private String password;

	//初始连接数
	private String initialSize;

	//最大空闲连接
	private String maxIdle;

	//连接超时时间
	private String maxWaitMillis;

	//URL连接串
	private String url;

	//URL扩展属性
	private String url_extend;

	/**
	* get数据库标识
	* @return dbid
	*/
	public String getDbid() {
		return dbid;
  	}

	/**
	* set数据库标识
	* @return dbid
	*/
	public void setDbid(String dbid) {
		this.dbid = dbid;
 	}

	/**
	* get数据库类型
	* @return db_type
	*/
	public String getDb_type() {
		return db_type;
  	}

	/**
	* set数据库类型
	* @return db_type
	*/
	public void setDb_type(String db_type) {
		this.db_type = db_type;
 	}

	/**
	* get数据库连接驱动
	* @return driverClassName
	*/
	public String getDriverClassName() {
		return driverClassName;
  	}

	/**
	* set数据库连接驱动
	* @return driverClassName
	*/
	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
 	}

	/**
	* get数据库连接地址
	* @return dbip
	*/
	public String getDbip() {
		return dbip;
  	}

	/**
	* set数据库连接地址
	* @return dbip
	*/
	public void setDbip(String dbip) {
		this.dbip = dbip;
 	}

	/**
	* get连接端口号
	* @return db_port
	*/
	public String getDb_port() {
		return db_port;
  	}

	/**
	* set连接端口号
	* @return db_port
	*/
	public void setDb_port(String db_port) {
		this.db_port = db_port;
 	}

	/**
	* get数据库实例名称
	* @return db_name
	*/
	public String getDb_name() {
		return db_name;
  	}

	/**
	* set数据库实例名称
	* @return db_name
	*/
	public void setDb_name(String db_name) {
		this.db_name = db_name;
 	}

	/**
	* get账号
	* @return username
	*/
	public String getUsername() {
		return username;
  	}

	/**
	* set账号
	* @return username
	*/
	public void setUsername(String username) {
		this.username = username;
 	}

	/**
	* get密码
	* @return password
	*/
	public String getPassword() {
		return password;
  	}

	/**
	* set密码
	* @return password
	*/
	public void setPassword(String password) {
		this.password = password;
 	}

	/**
	* get初始连接数
	* @return initialSize
	*/
	public String getInitialSize() {
		return initialSize;
  	}

	/**
	* set初始连接数
	* @return initialSize
	*/
	public void setInitialSize(String initialSize) {
		this.initialSize = initialSize;
 	}

	/**
	* get最大空闲连接
	* @return maxIdle
	*/
	public String getMaxIdle() {
		return maxIdle;
  	}

	/**
	* set最大空闲连接
	* @return maxIdle
	*/
	public void setMaxIdle(String maxIdle) {
		this.maxIdle = maxIdle;
 	}

	/**
	* get连接超时时间
	* @return maxWaitMillis
	*/
	public String getMaxWaitMillis() {
		return maxWaitMillis;
  	}

	/**
	* set连接超时时间
	* @return maxWaitMillis
	*/
	public void setMaxWaitMillis(String maxWaitMillis) {
		this.maxWaitMillis = maxWaitMillis;
 	}

	/**
	* getURL连接串
	* @return url
	*/
	public String getUrl() {
		return url;
  	}

	/**
	* setURL连接串
	* @return url
	*/
	public void setUrl(String url) {
		this.url = url;
 	}

	/**
	* getURL扩展属性
	* @return url_extend
	*/
	public String getUrl_extend() {
		return url_extend;
  	}

	/**
	* setURL扩展属性
	* @return url_extend
	*/
	public void setUrl_extend(String url_extend) {
		this.url_extend = url_extend;
 	}

}
