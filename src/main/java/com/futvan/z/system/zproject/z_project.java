package com.futvan.z.system.zproject;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_project extends SuperBean{
	//项目编号
	private String project_id;

	//项目名称
	private String project_name;

	/**
	* get项目编号
	* @return project_id
	*/
	public String getProject_id() {
		return project_id;
  	}

	/**
	* set项目编号
	* @return project_id
	*/
	public void setProject_id(String project_id) {
		this.project_id = project_id;
 	}

	/**
	* get项目名称
	* @return project_name
	*/
	public String getProject_name() {
		return project_name;
  	}

	/**
	* set项目名称
	* @return project_name
	*/
	public void setProject_name(String project_name) {
		this.project_name = project_name;
 	}

}
