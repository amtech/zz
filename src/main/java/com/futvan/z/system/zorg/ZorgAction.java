package com.futvan.z.system.zorg;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.system.zcode.z_code;
import com.futvan.z.system.zorg.z_org;
import com.futvan.z.system.zuser.z_user;
import com.futvan.z.framework.common.service.CommonService;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.StringUtil;
import com.futvan.z.system.zorg.ZorgService;
@Controller
public class ZorgAction extends SuperAction{
	@Autowired
	private ZorgService zorgService;
	@Autowired
	private CommonService commonService;
	
	/**
	 * 根据用户ID获取登录组织列表
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/getUserOrgForUserId")
	public @ResponseBody Result getUserOrgForUserId(@RequestParam String userId) throws Exception {
		Result result = new Result();
		List<z_org> userOrgList = GetUserOrg(userId);
		z_user user = GetDBUser(userId);
		if(z.isNull(user)) {
			result.setMsg("无此用户");
		}else {
			if(z.isNull(userOrgList) || userOrgList.size()<=0) {
				result.setMsg("账号未分配所属部门");
			}
		}
		result.setData(userOrgList);
		result.setCode(Code.SUCCESS);
		return result;
	}

	@RequestMapping(value="/OrgInsert")
	public @ResponseBody Result OrgInsert(@RequestParam HashMap<String,String> bean) throws Exception {
		//获取当前组织的完整组织名称
		fulllevel = "";
		getfulllevel(bean.get("parentid"));
		if(z.isNotNull(fulllevel)){
			bean.put("full_org_name", fulllevel.substring(1)+"_"+bean.get("org_name"));
		}else{
			bean.put("full_org_name",bean.get("org_name"));
		}
		return commonService.insert(bean,request);
	}

	@RequestMapping(value="/OrgUpdate")
	public @ResponseBody Result OrgUpdate(@RequestParam HashMap<String,String> bean) throws Exception {
		//获取当前组织的完整组织名称
		fulllevel = "";
		getfulllevel(bean.get("parentid"));
		if(z.isNotNull(fulllevel)){
			bean.put("full_org_name", fulllevel.substring(1)+"_"+bean.get("org_name"));
		}else{
			bean.put("full_org_name",bean.get("org_name"));
		}
		return commonService.update(bean,request);
	}

	/**
	 * 获取组织完整结构
	 */
	private String fulllevel = "";
	private void getfulllevel(String pid){
		z_org topOrg = z.orgs.get(pid);
		if(topOrg!=null){
			if(!"".equals(topOrg.getParentid()) && topOrg.getParentid()!=null){
				getfulllevel(topOrg.getParentid());
				fulllevel = fulllevel+"_"+topOrg.getOrg_name();
			}else{
				fulllevel = fulllevel+"_"+topOrg.getOrg_name();
			}
		}
	}

	/**
	 * 	打开导入用户页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/OpenImportUsersList")
	public ModelAndView OpenImportUsersList(String orgid) throws Exception {
		ModelAndView  model = new ModelAndView("system/zorg/ImportUsersList");
		model.addObject("userList",zorgService.getUserList(orgid));
		model.addObject("orgid",orgid);
		return model;
	}

	@RequestMapping(value="/ImportUsers")
	public @ResponseBody Result ImportUsers(String userIds,String orgid) throws Exception {
		Result result = new Result();
		if(!"".equals(userIds) && userIds!=null){
			if(!"".equals(orgid) && orgid!=null) {
				zorgService.ImportUser(orgid,userIds);
				result.setCode(Code.SUCCESS);
			}else {
				result.setCode(Code.ERROR);
				result.setData("组织ID为空");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setData("用户ID为空");
		}

		return result;
	}
}
