package com.futvan.z.system.zorg;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_org_user extends SuperBean{
	//用户
	private String userid;

	/**
	* get用户
	* @return userid
	*/
	public String getUserid() {
		return userid;
  	}

	/**
	* set用户
	* @return userid
	*/
	public void setUserid(String userid) {
		this.userid = userid;
 	}

}
