package com.futvan.z.system.zuser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.StringUtil;
import com.futvan.z.system.zuser.ZuserService;
@Controller
public class ZuserAction extends SuperAction{
	@Autowired
	private ZuserService zuserService;

	@Autowired
	private CommonService commonService;

	@RequestMapping(value="/UserInsert")
	public @ResponseBody Result UserInsert(@RequestParam HashMap<String,String> bean) throws Exception {
		//生成密码
		String password = StringUtil.CreatePassword("123456");
		bean.put("login_password", password);
		return commonService.insert(bean,request);
	}

	@RequestMapping(value="/UserUpdate")
	public @ResponseBody Result UserUpdate(@RequestParam HashMap<String,String> bean) throws Exception {
		String password = bean.get("login_password");
		if("".equals(password) || password==null) {
			bean.put("login_password", StringUtil.CreatePassword("123456"));
		}
		return commonService.update(bean,request);
	}

	@RequestMapping(value="/user_settings")
	public ModelAndView user_settings() throws Exception {
		ModelAndView model = new ModelAndView("system/zuser/user_settings");
		//获取用户
		z_user user = GetSessionUser(request);
		model.addObject("user", user);
		return model;
	}
	
	@RequestMapping(value="/user_settings_password")
	public ModelAndView user_settings_password() throws Exception {
		ModelAndView model = new ModelAndView("system/zuser/user_settings_password");
		return model;
	}

	@RequestMapping(value="/SaveUserPassword")
	public @ResponseBody Result SaveUserPassword(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();

		String login_password = bean.get("login_password");
		String new_login_password1 = bean.get("new_login_password1");
		String new_login_password2 = bean.get("new_login_password2");

		if(z.isNotNull(login_password)) {
			if(z.isNotNull(new_login_password1)) {
				if(z.isNotNull(new_login_password2)) {
					if(new_login_password1.equals(new_login_password2)) {
						if(z.isNotNull(request.getSession().getAttribute("zuser")) &&  request.getSession().getAttribute("zuser") instanceof z_user) {
							//获取原用户信息
							z_user user = (z_user) request.getSession().getAttribute("zuser");
							String db_login_password = user.getLogin_password();
							if(db_login_password.equals(StringUtil.CreatePassword(login_password))) {
								//保存新密码
								zuserService.updateLoginPassword(user.getZid(),StringUtil.CreatePassword(new_login_password1));
								
								//更新Session中的用户登录密码
								user.setLogin_password(StringUtil.CreatePassword(new_login_password1));
								request.getSession().setAttribute("zuser", user);
								
								//设置返回结果
								result.setCode(Code.SUCCESS);
								result.setMsg("密码修改成功");
								
							}else {
								result.setCode(Code.ERROR);
								result.setMsg("原密码输入错误，请重新输入。");
							}
						}else {
							result.setCode(Code.ERROR);
							result.setMsg("获取用户信息出错，请退出后重新登录操作。");
						}

					}else {
						result.setCode(Code.ERROR);
						result.setMsg("新密码两次输入不相同，请重新输入新密码。");
					}
				}else {
					result.setCode(Code.ERROR);
					result.setMsg("新密码确认不能为空");
				}
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("新密码不能为空");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("原密码不能为空");
		}

		return result;
	}


}
