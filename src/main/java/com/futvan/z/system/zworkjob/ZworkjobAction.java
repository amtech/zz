package com.futvan.z.system.zworkjob;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.JobUtil;
import com.futvan.z.system.zworkjob.ZworkjobService;
@Controller
public class ZworkjobAction extends SuperAction{
	@Autowired
	private ZworkjobService zworkjobService;
	
	/**
	 * 用户提交工作流程
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/oa_submit")
	public @ResponseBody Result oa_submit(@RequestParam HashMap<String,String> bean) throws Exception {
		//从Session中获取用户与组织信息保存到Bean中
		GetSessionInfoToBean(bean,request);
		return zworkjobService.CreateWorkJob(bean);
	}
	
	/**
	 * 打开工作任务处理页面
	 * @param zid 任务节点ID
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/job_handle")
	public ModelAndView job_handle(String zid) throws Exception {
		ModelAndView  model = new ModelAndView("system/zworkjob/job_handle");
		if(z.isNotNull(zid)) {
			//返回任务处理页面HTML
			model.addObject("workjob_node_html", zworkjobService.CreateWorkJobHandleHtml(zid,request));
			
			model.addObject("now_node", zworkjobService.getNowNode(zid));
		}else {
			z.Exception("打开工作任务处理页面出错：zid is not null");
		}
		return model;
	}
	
	/**
	 * 提交审批信息
	 * @param zid
	 * @param nodeid
	 * @param ExamineState
	 * @param ExamineOpinion
	 * @param newApprovalUser 新审批用户
	 * @return 
	 * @throws Exception
	 */
	@RequestMapping(value="/job_handle_submit")
	public ModelAndView job_handle_submit(String wid,String nodeid,String ExamineState,String ExamineOpinion,String newApprovalUser) throws Exception {
		ModelAndView  model = new ModelAndView("redirect:/rlist");
		String userid = GetSessionUserId(request);
		String orgid = GetSessionUserOrgId(request);
		if(z.isNotNull(wid) && z.isNotNull(userid) && z.isNotNull(orgid) && z.isNotNull(nodeid)) {
			//推动流程节点处理
			zworkjobService.RunWorkJobHandler(wid,nodeid,userid,orgid,ExamineState,ExamineOpinion,newApprovalUser);
			//返回任务处理页面HTML
			//model.addObject("workjob_node_html", zworkjobService.CreateWorkJobHandleHtml(wid,request));
			model.addObject("zid", "2a3810a816b847o2rp4arzd21a2e125178tfc219731e172317241514163008");
		}else {
			z.Exception("提交审批信息出错 :关键参数为空");
		}
		return model;
	}
	
	
	
	
	
}
