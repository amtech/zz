package com.futvan.z.system.zworkjob;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_workjob_node extends SuperBean{
	//节点标题
	private String node_title;

	//节点类型
	private String node_type;

	//是否加签节点
	private String is_temporary;

	//处理人类型
	private String workflow_user_type;

	//审批人为空时
	private String approverisnull;

	//处理人
	private String node_user;

	//处理角色
	private String node_role;

	//审批方式
	private String processing_mode;

	//向上审批层数
	private String approvals_up_num;

	//审批终点角色
	private String end_approval_role;

	//处理方法
	private String node_action;

	//通过_下一节点
	private String node_true;

	//未通过_下一节点
	private String node_false;

	//是否处理
	private String is_approval;

	//审批结果
	private String examine_state;

	//审批意见
	private String examine_opinion;

	//审批人
	private List<z_workjob_approvaluser> z_workjob_approvaluser_list;


	/**
	* get节点标题
	* @return node_title
	*/
	public String getNode_title() {
		return node_title;
  	}

	/**
	* set节点标题
	* @return node_title
	*/
	public void setNode_title(String node_title) {
		this.node_title = node_title;
 	}

	/**
	* get节点类型
	* @return node_type
	*/
	public String getNode_type() {
		return node_type;
  	}

	/**
	* set节点类型
	* @return node_type
	*/
	public void setNode_type(String node_type) {
		this.node_type = node_type;
 	}

	/**
	* get是否加签节点
	* @return is_temporary
	*/
	public String getIs_temporary() {
		return is_temporary;
  	}

	/**
	* set是否加签节点
	* @return is_temporary
	*/
	public void setIs_temporary(String is_temporary) {
		this.is_temporary = is_temporary;
 	}

	/**
	* get处理人类型
	* @return workflow_user_type
	*/
	public String getWorkflow_user_type() {
		return workflow_user_type;
  	}

	/**
	* set处理人类型
	* @return workflow_user_type
	*/
	public void setWorkflow_user_type(String workflow_user_type) {
		this.workflow_user_type = workflow_user_type;
 	}

	/**
	* get审批人为空时
	* @return approverisnull
	*/
	public String getApproverisnull() {
		return approverisnull;
  	}

	/**
	* set审批人为空时
	* @return approverisnull
	*/
	public void setApproverisnull(String approverisnull) {
		this.approverisnull = approverisnull;
 	}

	/**
	* get处理人
	* @return node_user
	*/
	public String getNode_user() {
		return node_user;
  	}

	/**
	* set处理人
	* @return node_user
	*/
	public void setNode_user(String node_user) {
		this.node_user = node_user;
 	}

	/**
	* get处理角色
	* @return node_role
	*/
	public String getNode_role() {
		return node_role;
  	}

	/**
	* set处理角色
	* @return node_role
	*/
	public void setNode_role(String node_role) {
		this.node_role = node_role;
 	}

	/**
	* get审批方式
	* @return processing_mode
	*/
	public String getProcessing_mode() {
		return processing_mode;
  	}

	/**
	* set审批方式
	* @return processing_mode
	*/
	public void setProcessing_mode(String processing_mode) {
		this.processing_mode = processing_mode;
 	}

	/**
	* get向上审批层数
	* @return approvals_up_num
	*/
	public String getApprovals_up_num() {
		return approvals_up_num;
  	}

	/**
	* set向上审批层数
	* @return approvals_up_num
	*/
	public void setApprovals_up_num(String approvals_up_num) {
		this.approvals_up_num = approvals_up_num;
 	}

	/**
	* get审批终点角色
	* @return end_approval_role
	*/
	public String getEnd_approval_role() {
		return end_approval_role;
  	}

	/**
	* set审批终点角色
	* @return end_approval_role
	*/
	public void setEnd_approval_role(String end_approval_role) {
		this.end_approval_role = end_approval_role;
 	}

	/**
	* get处理方法
	* @return node_action
	*/
	public String getNode_action() {
		return node_action;
  	}

	/**
	* set处理方法
	* @return node_action
	*/
	public void setNode_action(String node_action) {
		this.node_action = node_action;
 	}

	/**
	* get通过_下一节点
	* @return node_true
	*/
	public String getNode_true() {
		return node_true;
  	}

	/**
	* set通过_下一节点
	* @return node_true
	*/
	public void setNode_true(String node_true) {
		this.node_true = node_true;
 	}

	/**
	* get未通过_下一节点
	* @return node_false
	*/
	public String getNode_false() {
		return node_false;
  	}

	/**
	* set未通过_下一节点
	* @return node_false
	*/
	public void setNode_false(String node_false) {
		this.node_false = node_false;
 	}

	/**
	* get是否处理
	* @return is_approval
	*/
	public String getIs_approval() {
		return is_approval;
  	}

	/**
	* set是否处理
	* @return is_approval
	*/
	public void setIs_approval(String is_approval) {
		this.is_approval = is_approval;
 	}

	/**
	* get审批结果
	* @return examine_state
	*/
	public String getExamine_state() {
		return examine_state;
  	}

	/**
	* set审批结果
	* @return examine_state
	*/
	public void setExamine_state(String examine_state) {
		this.examine_state = examine_state;
 	}

	/**
	* get审批意见
	* @return examine_opinion
	*/
	public String getExamine_opinion() {
		return examine_opinion;
  	}

	/**
	* set审批意见
	* @return examine_opinion
	*/
	public void setExamine_opinion(String examine_opinion) {
		this.examine_opinion = examine_opinion;
 	}

	/**
	* get审批人
	* @return 审批人
	*/
	public List<z_workjob_approvaluser> getZ_workjob_approvaluser_list() {
		return z_workjob_approvaluser_list;
  	}

	/**
	* set审批人
	* @return 审批人
	*/
	public void setZ_workjob_approvaluser_list(List<z_workjob_approvaluser> z_workjob_approvaluser_list) {
		this.z_workjob_approvaluser_list = z_workjob_approvaluser_list;
 	}

}
