package com.futvan.z.system.zworkjob;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.bean.z_form_table;
import com.futvan.z.framework.common.bean.z_form_table_column;
import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.BeanUtil;
import com.futvan.z.framework.util.DateUtil;
import com.futvan.z.framework.util.EmailUtil;
import com.futvan.z.framework.util.HttpUtil;
import com.futvan.z.framework.util.StringUtil;
import com.futvan.z.system.zcode.z_code;
import com.futvan.z.system.zcode.z_code_detail;
import com.futvan.z.system.zhttpservices.z_http_services;
import com.futvan.z.system.zorg.z_org;
import com.futvan.z.system.zuser.z_user;
import com.futvan.z.system.zworkflow.z_workflow;
import com.futvan.z.system.zworkflow.z_workflow_node;
import com.futvan.z.system.zworkflow.z_workflow_node_branch;
import com.sun.org.apache.bcel.internal.generic.Select;
@Service
public class ZworkjobService extends SuperService{

	/**
	 * 根据流程ZID获取 所有流程节点
	 * @param zid
	 * @param request 
	 * @return
	 * @throws Exception 
	 */
	public String CreateWorkJobHandleHtml(String zid, HttpServletRequest request) throws Exception {
		StringBuffer result = new StringBuffer("");
		//准备生成流程图页面数据
		//工作流程对象
		z_workjob wj = getWorkJob(zid);
		//节点列表
		List<HashMap<String,String>> workjobList = selectList("select * from z_workjob_node where pid = '"+zid+"'");
		//开始节点
		String start_node = getStartNodeId(workjobList);
		//所有节点
		Map<String,z_workjob_node> allNode = getAllNode(workjobList);
	
	
		//创建HTML代码====================================================================================
		result.append("<div id=\"ButtonNorth\" data-options=\"region:'north',border:false\">").append("\r\n");
		result.append("	<div id=\"ButtonHR\" class=\"btn-group\" role=\"group\">").append("\r\n");
		//当前登录人
		String session_userid = GetSessionUserId(request);
		
		
		boolean isApprovaluser = ifSessionUserIsApprovaluser(wj,allNode,session_userid);
		if(isApprovaluser) {
			result.append("		<button  type=\"button\" class=\"btn btn-light\" onclick=\"agree()\"><i class=\"fa fa-check\"></i> 同意</button>").append("\r\n");
			result.append("		<button  type=\"button\" class=\"btn btn-light\" onclick=\"disagree()\"><i class=\"fa fa-close\"></i> 不同意</button>").append("\r\n");
			result.append("		<button  type=\"button\" class=\"btn btn-light\" onclick=\"addTempNode()\"><i class=\"fa fa-user-plus\"></i> 转审</button>").append("\r\n");
		}
		result.append("		<button  type=\"button\" class=\"btn btn-light\" onclick=\"window.location.href='rlist?zid=2a3810a816b847o2rp4arzd21a2e125178tfc219731e172317241514163008';\"><i class=\"fa fa-hand-o-left\"></i> 返回</button>").append("\r\n");
		result.append("	</div>").append("\r\n");
		result.append("</div>").append("\r\n");
		result.append("<div data-options=\"region:'center',border:false\">").append("\r\n");
		result.append("	<div class=\"container-fluid\">").append("\r\n");
		result.append("		<div class=\"row\">").append("\r\n");
		//创建流程页面
		CreateWorkJobNode(result,wj,allNode.get(start_node),allNode,request);
		result.append("		</div>").append("\r\n");
		result.append("	</div>").append("\r\n");
		result.append("</div>").append("\r\n");
		//创建HTML代码====================================================================================
	
		return result.toString();
	}


	/**
	 * 创建流程节点HTML
	 * @param wj 
	 * @param wj
	 * @param allNode 
	 * @param request 
	 * @return
	 * @throws Exception 
	 */
	private void CreateWorkJobNode(StringBuffer result,z_workjob wj, z_workjob_node node, Map<String, z_workjob_node> allNode, HttpServletRequest request) throws Exception {
		if(z.isNotNull(node)) {
			if("0".equals(node.getNode_type())) {//开始节点
				result.append("<div class=\"col-sm-12\">").append("\r\n");
				result.append("	<div class=\"card\" >").append("\r\n");
				result.append("		<div class=\"card-header fs20\" >").append("\r\n");
				result.append("			<div class=\"row\">").append("\r\n");
				result.append("				<div class=\"col-sm-7\" >").append("标题："+wj.getName()).append("</div>").append("\r\n");
				result.append("				<div class=\"col-sm-5\" >").append("发起人："+getUserName(wj.getUser_id())).append("</div>").append("\r\n");
				result.append("			</div>").append("\r\n");
				result.append("		</div>").append("\r\n");
				result.append("		<div class=\"card-body\" >").append("\r\n");
				HashMap<String,String> parameter = selectMap("select * from "+wj.getTable_id()+"  where zid = '"+wj.getBiz_id()+"'");
				parameter.put("tableId", wj.getTable_id());
				z_form_table table = z.tables.get(wj.getTable_id());
				CreateBillForm(result, table, parameter, "update");
				result.append("		</div>").append("\r\n");
				result.append("	</div>").append("\r\n");
				result.append("</div>").append("\r\n");
				//调用下一节点
				CreateWorkJobNode(result,wj,allNode.get(node.getNode_true()),allNode,request);
			}else if("1".equals(node.getNode_type())) {//分支节点
				result.append("<div class=\"col-sm-12\">").append("\r\n");
				result.append("<img src=\"img/system/workjob/workjob_jiantou0.png\" class=\"Wj_jiantou\"/>").append("\r\n");
				result.append("<div class=\"card-group\">").append("\r\n");
					result.append("	<div class=\"card\" >").append("\r\n");
					result.append("		<div class=\"card-header fs20\" >").append("\r\n");
					result.append("			<div class=\"row\">").append("\r\n");
					result.append("				<div class=\"col-sm-12\" >").append("标题："+node.getNode_title()).append("</div>").append("\r\n");
					result.append("			</div>").append("\r\n");
					result.append("		</div>").append("\r\n");
					result.append("		<div class=\"card-body\" >").append("\r\n");
					result.append("			<textarea class=\"form-control\" rows=\"5\" disabled>"+StringUtil.print(node.getExamine_opinion())+"</textarea>").append("\r\n");
					result.append("		</div>").append("\r\n");
					result.append("	</div>").append("\r\n");
				result.append("</div>").append("\r\n");
				result.append("</div>").append("\r\n");
				//调用下一节点
				CreateWorkJobNode(result,wj,allNode.get(node.getNode_true()),allNode,request);
			}else if("2".equals(node.getNode_type())) {//人工节点
				result.append("<div class=\"col-sm-12\">").append("\r\n");
				if(wj.getNow_node().equals(node.getZid())) {
					result.append("<img src=\"img/system/workjob/workjob_jiantou1.png\" class=\"Wj_jiantou\"/>").append("\r\n");
				}else {
					result.append("<img src=\"img/system/workjob/workjob_jiantou0.png\" class=\"Wj_jiantou\"/>").append("\r\n");
				}
	
				result.append("<div class=\"card-group\">").append("\r\n");
				List<z_workjob_approvaluser> aulist = node.getZ_workjob_approvaluser_list();
				//循环审批人
				for (z_workjob_approvaluser au : aulist) {
					result.append("	<div class=\"card\" >").append("\r\n");
	
					String session_userid = GetSessionUserId(request);
					if(wj.getNow_node().equals(node.getZid())) {
						result.append("		<div class=\"card-header fs20 ts1\" >").append("\r\n");
					}else {
						result.append("		<div class=\"card-header fs20\" >").append("\r\n");
					}
	
					result.append("			<div class=\"row\">").append("\r\n");
					result.append("				<div class=\"col-sm-7\" >").append("标题："+node.getNode_title()).append("</div>").append("\r\n");
					result.append("				<div class=\"col-sm-5\" >").append("审批人："+getUserName(au.getUserzid())).append("</div>").append("\r\n");
					result.append("			</div>").append("\r\n");
					result.append("		</div>").append("\r\n");
					result.append("		<div class=\"card-body\" >").append("\r\n");
	
					//审批结果
					result.append("			<textarea class=\"form-control\" rows=\"5\" disabled>"+StringUtil.print(au.getExamine_opinion())+"</textarea>").append("\r\n");
	
					//审批图章
					if("0".equals(au.getExamine_state())) {
						result.append("			<img alt=\"不同意\" src=\"img/system/workjob/workjob_disagree.png\" class=\"Wj_tuzhang\"/>").append("\r\n");
					}
					if("1".equals(au.getExamine_state())) {
						result.append("			<img alt=\"同意\" src=\"img/system/workjob/workjob_agree.png\" class=\"Wj_tuzhang\"/>").append("\r\n");
					}
					if("2".equals(au.getExamine_state())) {
						result.append("			<img alt=\"转审\" src=\"img/system/workjob/workjob_shift.png\" class=\"Wj_tuzhang\"/>").append("\r\n");
					}
					result.append("		</div>").append("\r\n");
					result.append("	</div>").append("\r\n");
				}
				result.append("</div>").append("\r\n");
				result.append("</div>").append("\r\n");
				//调用下一节点
				CreateWorkJobNode(result,wj,allNode.get(node.getNode_true()),allNode,request);
			}else if("3".equals(node.getNode_type())) {//抄送节点
				result.append("<div class=\"col-sm-12\">").append("\r\n");
				result.append("<img src=\"img/system/workjob/workjob_jiantou0.png\" class=\"Wj_jiantou\"/>").append("\r\n");
				result.append("<div class=\"card-group\">").append("\r\n");
				List<z_workjob_approvaluser> aulist = node.getZ_workjob_approvaluser_list();
				//循环抄送人
				for (z_workjob_approvaluser au : aulist) {
					result.append("	<div class=\"card\" >").append("\r\n");
					result.append("		<div class=\"card-header fs20\" >").append("\r\n");
					result.append("			<div class=\"row\">").append("\r\n");
					result.append("				<div class=\"col-sm-7\" >").append("标题："+node.getNode_title()).append("</div>").append("\r\n");
					result.append("				<div class=\"col-sm-5\" >").append("接收人："+getUserName(au.getUserzid())).append("</div>").append("\r\n");
					result.append("			</div>").append("\r\n");
					result.append("		</div>").append("\r\n");
					result.append("		<div class=\"card-body\" >").append("\r\n");
					//抄送信息
					result.append("			<textarea class=\"form-control\" rows=\"5\" disabled>"+StringUtil.print(node.getExamine_opinion())+"</textarea>").append("\r\n");
					result.append("		</div>").append("\r\n");
					result.append("	</div>").append("\r\n");
				}
				result.append("</div>").append("\r\n");
				result.append("</div>").append("\r\n");
				//调用下一节点
				CreateWorkJobNode(result,wj,allNode.get(node.getNode_true()),allNode,request);
			}else if("4".equals(node.getNode_type())) {//自动执行节点
				result.append("<div class=\"col-sm-12\">").append("\r\n");
				result.append("<img src=\"img/system/workjob/workjob_jiantou0.png\" class=\"Wj_jiantou\"/>").append("\r\n");
				result.append("<div class=\"card-group\">").append("\r\n");
					result.append("	<div class=\"card\" >").append("\r\n");
					result.append("		<div class=\"card-header fs20\" >").append("\r\n");
					result.append("			<div class=\"row\">").append("\r\n");
					result.append("				<div class=\"col-sm-12\" >").append("标题："+node.getNode_title()).append("</div>").append("\r\n");
					result.append("			</div>").append("\r\n");
					result.append("		</div>").append("\r\n");
					result.append("		<div class=\"card-body\" >").append("\r\n");
					
					result.append("			<textarea class=\"form-control\" rows=\"5\" disabled>"+StringUtil.print(node.getExamine_opinion())+"</textarea>").append("\r\n");
					result.append("		</div>").append("\r\n");
					result.append("	</div>").append("\r\n");
				result.append("</div>").append("\r\n");
				result.append("</div>").append("\r\n");
				//调用下一节点
				CreateWorkJobNode(result,wj,allNode.get(node.getNode_true()),allNode,request);
			}else if("9".equals(node.getNode_type())) {//结束节点
				result.append("<div class=\"col-sm-12\">").append("\r\n");
				if(wj.getNow_node().equals(node.getZid())) {
					result.append("<img src=\"img/system/workjob/workjob_jiantou1.png\" class=\"Wj_jiantou\"/>").append("\r\n");
					result.append("<h1 style=\"margin-top: 30px; margin-bottom:120px; margin-left: 42%;color:#4032ae;\" >流程结束</h1>").append("\r\n");
				}else {
					result.append("<img src=\"img/system/workjob/workjob_jiantou0.png\" class=\"Wj_jiantou\"/>").append("\r\n");
					result.append("<h1 style=\"margin-top: 30px; margin-bottom:120px; margin-left: 42%;color:#d8d8d8;\" >流程结束</h1>").append("\r\n");
				}
	
				result.append("</div>").append("\r\n");
			}
		}
	}


	/**
	 * 根据用户信息、业务表名、业务ID创建工作流程实例
	 * @param bean
	 * @param request 
	 * @return
	 * @throws Exception 
	 */
	public Result CreateWorkJob(HashMap<String,String> bean) throws Exception {
		Result result = new Result();
		if(z.isNotNull(bean)) {
			//业务ID
			String biz_id = bean.get("zid");
			//流程ID
			String wid = bean.get("wid");
			//表名
			String tableId = bean.get("tableId");
			//申请人ZID
			z_user user = z.users.get(bean.get("session_userid"));
			//申请人登录组织
			z_org org = z.orgs.get(bean.get("session_orgid"));
			if(z.isNotNull(biz_id) && z.isNotNull(wid) && z.isNotNull(tableId) && z.isNotNull(user) && z.isNotNull(org)) {
				//获取工作流模板对象
				z_workflow wf = z.wf.get(wid);
				if(z.isNotNull(wf)) {
					//准备用于生成工作流程实例的模板对象
					String wj_zid = z.newZid("z_workjob");//创建工作流程主表主键
					//开始节点对象
					z_workflow_node start_node = null;
					//节点集合<节点ID，节点对象>
					Map<String,z_workflow_node> nodes = new HashMap<String, z_workflow_node>();
					for (int i = 0; i < wf.getZ_workflow_node_list().size(); i++) {
						z_workflow_node now_node = wf.getZ_workflow_node_list().get(i);

						//如果节点类型等于0 就是开始节点
						if("0".equals(now_node.getNode_type())){
							start_node = now_node;
						}
						nodes.put(now_node.getZid(), now_node);
					}

					//根据工作流程模板节点对象创建工作流程实例节点对象【递归生成】
					boolean result_status = CreateWorkJobNodeForWorkflowNode(wf,start_node,nodes,biz_id,org,user,tableId,wj_zid);
					if(result_status) {
						//推动一次流程执行 当前节点为开始节点，当前用户/当前用户组织，审批结果 为通过
						RunWorkJobHandler(wj_zid,start_node.getZid(),user.getZid(),org.getZid(),null,null,null);
						result.setCode(Code.SUCCESS);
						result.setMsg("申请成功");
					}else {
						z.Exception("提交申请失败：工作流程模板设置出现，请联系系统管理员排查。流程模板ID："+wid);
					}
				}else {
					result.setCode(Code.ERROR);
					result.setMsg("工作流程错误：无效流程模板ID，未找到工作流程模板。wid:"+wid);
				}
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("工作流程错误：关键参数为空 |业务ID:"+biz_id+"  |流程模板ID:"+wid+"  |表ID:"+tableId+"  |申请人ID:"+user+"  |所属组织ID:"+org);
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("工作流程错误：参数对象为空");
		}
		return result;
	}


	/**
	 * 根据流程模板节点对象创建工作流程实例节点对象【递归生成】
	 * @param wf 模板对象
	 * @param wf_now_node 当前模板节点
	 * @param nodes 所有模板节点
	 * @param biz_id 业务ID
	 * @param org 组织对象
	 * @param user 当前用户对象
	 * @param wj_zid 工作流程实例主表主键
	 * @param wj_zid 
	 * @return
	 * @throws Exception
	 */
	private boolean CreateWorkJobNodeForWorkflowNode(z_workflow wf,z_workflow_node wf_now_node,Map<String,z_workflow_node> nodes,String biz_id,z_org org,z_user user,String tableId, String wj_zid) throws Exception {
		boolean result = false;
		//判读节点是否创建，如果创建进行下一节点
		if(isCreateNode(wf_now_node)) {
			if(!"9".equals(wf_now_node.getNode_type())) {
				//调用下一节点
				result =  CreateWorkJobNodeForWorkflowNode(wf, nodes.get(wf_now_node.getNode_true()), nodes, biz_id, org, user,tableId,wj_zid);
			}else {
				result = true;
			}
		}else {
			//获取通过的下一结点对象
			z_workflow_node next_node = nodes.get(wf_now_node.getNode_true());
			if("0".equals(wf_now_node.getNode_type())) {
				//创建开始节点
				//创建工作流程实例主表对象z_workjob
				HashMap<String,String> z_workjobMap = new HashMap<String, String>();
				z_workjobMap.put("tableId", "z_workjob");
				z_workjobMap.put("zid", wj_zid);//主键
				z_workjobMap.put("number", z.newNumber());//编号
				z_workjobMap.put("name", org.getFull_org_name()+"_"+user.getUser_name()+"_"+wf.getW_name());//标题
				z_workjobMap.put("biz_id", biz_id);//业务数据ID
				z_workjobMap.put("user_id", user.getZid());//提交申请人
				z_workjobMap.put("table_id", tableId);//业务表ID
				insert(z_workjobMap);

				//创建工作流程实例节点中的开始节点
				HashMap<String,String> WorkJobStartNodeMap = new HashMap<String, String>();
				WorkJobStartNodeMap.put("tableId", "z_workjob_node");
				WorkJobStartNodeMap.put("zid", wf_now_node.getZid());//节点ID
				WorkJobStartNodeMap.put("pid", wj_zid);//工作流程实例主表主键
				WorkJobStartNodeMap.put("node_title", "提交申请");//标题
				WorkJobStartNodeMap.put("node_type", "0");//节点类型 0 ：开始节点
				WorkJobStartNodeMap.put("node_user", user.getZid());//提交人
				WorkJobStartNodeMap.put("node_true", wf_now_node.getNode_true());//通过_下一节点 
				WorkJobStartNodeMap.put("node_false", wf_now_node.getNode_false());//通过_下一节点 
				insert(WorkJobStartNodeMap);
				//生成通过、下一节点对象
				result =  CreateWorkJobNodeForWorkflowNode(wf, next_node, nodes, biz_id, org, user,tableId,wj_zid);
			}else if("1".equals(wf_now_node.getNode_type())) {
				//创建分支节点
				HashMap<String,String> WorkJobNodeMap = new HashMap<String, String>();
				WorkJobNodeMap.put("tableId", "z_workjob_node");
				WorkJobNodeMap.put("zid", wf_now_node.getZid());//节点ID
				WorkJobNodeMap.put("pid", wj_zid);//工作流程实例主表主键
				WorkJobNodeMap.put("node_title", wf_now_node.getNode_title());//标题
				WorkJobNodeMap.put("node_type", wf_now_node.getNode_type());//节点类型
				z_workflow_node_branch okbranch = getOkBranch(wf_now_node, tableId, biz_id);
				WorkJobNodeMap.put("node_true", okbranch.getNodeid());//通过_下一节点 
				WorkJobNodeMap.put("node_false", okbranch.getNodeid());//通过_下一节点 
				
				//获取字段-创建显示标题
				z_form_table_column column = z.columnsForColumnZid.get(okbranch.getColumnid());
				String codevalue = z.codeValue.get("oa_column_compare_"+okbranch.getColumn_compare());
				String branchTitle = column.getColumn_name()+" "+codevalue+" "+okbranch.getColumn_value();
				if(z.isNotNull(okbranch.getColumn_compare2()) && z.isNotNull(okbranch.getColumn_value2()) && z.isNotNull(okbranch.getConnector_type())) {
					String connectortype_value = z.codeValue.get("connector_type_"+okbranch.getConnector_type());
					String column_compare2value = z.codeValue.get("oa_column_compare_"+okbranch.getColumn_compare2());
					branchTitle = branchTitle+" "+connectortype_value+" "+column.getColumn_name()+" "+column_compare2value+" "+okbranch.getColumn_value2();
				}
				branchTitle = branchTitle+" "+"调用 【"+nodes.get(okbranch.getNodeid()).getNode_title()+"】";
				WorkJobNodeMap.put("examine_opinion", branchTitle);//审批意见
				insert(WorkJobNodeMap);

				//生成通过、下一节点对象
				next_node = nodes.get(okbranch.getNodeid());
				result =  CreateWorkJobNodeForWorkflowNode(wf, next_node, nodes, biz_id, org, user,tableId,wj_zid);

			}else if("2".equals(wf_now_node.getNode_type())) {
				//创建人工节点
				HashMap<String,String> WorkJobNodeMap = new HashMap<String, String>();
				WorkJobNodeMap.put("tableId", "z_workjob_node");
				WorkJobNodeMap.put("zid", wf_now_node.getZid());//节点ID
				WorkJobNodeMap.put("pid", wj_zid);//工作流程实例主表主键
				WorkJobNodeMap.put("node_title", wf_now_node.getNode_title());//标题
				WorkJobNodeMap.put("node_type", wf_now_node.getNode_type());//节点类型
				WorkJobNodeMap.put("workflow_user_type", wf_now_node.getWorkflow_user_type());//处理人类型
				WorkJobNodeMap.put("node_user", wf_now_node.getNode_user());//处理人
				WorkJobNodeMap.put("node_role", wf_now_node.getNode_role());//处理角色
				WorkJobNodeMap.put("processing_mode", wf_now_node.getProcessing_mode());//审批方式
				WorkJobNodeMap.put("end_approval_role", wf_now_node.getEnd_approval_role());//审批终点角色
				WorkJobNodeMap.put("approverisnull", wf_now_node.getApproverisnull());//审批人为空时
				WorkJobNodeMap.put("approvals_up_num", wf_now_node.getApprovals_up_num());//向上审批层数
				WorkJobNodeMap.put("node_true", wf_now_node.getNode_true());//通过_下一节点 
				WorkJobNodeMap.put("node_false", wf_now_node.getNode_false());//通过_下一节点 
				insert(WorkJobNodeMap);

				//判读处理人类型
				if("0".equals(wf_now_node.getWorkflow_user_type())) {
					//指定成员
					HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
					WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
					WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
					WorkJobNodeApprovaluserMap.put("userzid", wf_now_node.getNode_user());//审批人
					WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
					insert(WorkJobNodeApprovaluserMap);
				}else if("1".equals(wf_now_node.getWorkflow_user_type())){
					//指定角色
					//获取角色用户列表
					List<String> roleUserList = selectListString("select userid from z_role_user where pid = '"+wf_now_node.getNode_role()+"'");
					if(roleUserList.size()==0) {
						z.Exception("工作流程错误：审批节点中包括角色用户审批，但未获取到该角色下用户。");
					}else {
						for (String RoleUserId : roleUserList) {
							HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
							WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
							WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
							WorkJobNodeApprovaluserMap.put("userzid", RoleUserId);//审批人
							WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
							insert(WorkJobNodeApprovaluserMap);
						}
					}
				}else if("2".equals(wf_now_node.getWorkflow_user_type())){
					//申请人直接主管
					HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
					WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
					WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
					//获取申请人直接领导
					String leaderId = getLeaderId(user.getZid(),org.getZid());
					if(z.isNull(leaderId)) {
						z.Exception("工作流程错误：审批节点中包括直接领导审批，但未获取到申请人直接领导。");
					}
					WorkJobNodeApprovaluserMap.put("userzid", leaderId);//审批人
					WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
					insert(WorkJobNodeApprovaluserMap);

				}else if("3".equals(wf_now_node.getWorkflow_user_type())){
					//连续多级主管审批
					//获取多级审批领导ID
					int approvals_up_num = new Integer(wf_now_node.getApprovals_up_num());//向上审批层数
					List<String> LeaderIdList = new ArrayList<String>();
					getMultistageLeaderId(user.getZid(),org.getZid(),wf_now_node.getEnd_approval_role(),approvals_up_num,LeaderIdList);
					if(LeaderIdList.size()==0) {
						z.Exception("工作流程错误：审批节点中包括连续多级主管审批，但未获取到多级主管ID。");
					}else {
						for (String leaderId : LeaderIdList) {
							HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
							WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
							WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
							WorkJobNodeApprovaluserMap.put("userzid", leaderId);//审批人
							WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
							insert(WorkJobNodeApprovaluserMap);
						}
					}
				}else if("4".equals(wf_now_node.getWorkflow_user_type())){
					//表单指定部门负责人--根据表单中指定部门字段所保存的部门信息，找到该部门的负责人
					StringBuffer getOrgManagerUserzidSQL = new StringBuffer();
					getOrgManagerUserzidSQL.append("SELECT o.user_head ");
					getOrgManagerUserzidSQL.append("FROM "+tableId+" c ");
					getOrgManagerUserzidSQL.append("LEFT JOIN z_org o ON c."+z.columnsForColumnZid.get(wf_now_node.getForm_org()).getColumn_id()+" = o.zid ");
					getOrgManagerUserzidSQL.append("WHERE c.zid = '"+biz_id+"' ");

					//部门负责人
					String OrgManagerUserzid = selectString(getOrgManagerUserzidSQL.toString());
					HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
					WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
					WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
					WorkJobNodeApprovaluserMap.put("userzid", OrgManagerUserzid);//审批人
					WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
					insert(WorkJobNodeApprovaluserMap);
				}else if("5".equals(wf_now_node.getWorkflow_user_type())){
					//主管副总
					String userid = GetLeader(org.getZid(),2);
					if(z.isNotNull(userid)) {
						HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
						WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
						WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
						WorkJobNodeApprovaluserMap.put("userzid", userid);//审批人
						WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
						insert(WorkJobNodeApprovaluserMap);
					}else {
						z.Exception("工作流程错误：节点中包括副总，但未获取到组织中副总级别的用户");
					}

				}else if("6".equals(wf_now_node.getWorkflow_user_type())){
					//主管总经理
					String userid = GetLeader(org.getZid(),1);
					if(z.isNotNull(userid)) {
						HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
						WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
						WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
						WorkJobNodeApprovaluserMap.put("userzid", userid);//审批人
						WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
						insert(WorkJobNodeApprovaluserMap);
					}else {
						z.Exception("工作流程错误：节点中包括副总，但未获取到组织中副总级别的用户");
					}

				}else if("7".equals(wf_now_node.getWorkflow_user_type())){
					//流程发起人
					HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
					WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
					WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
					WorkJobNodeApprovaluserMap.put("userzid", user.getZid());//审批人
					WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
					insert(WorkJobNodeApprovaluserMap);
				}
				//生成通过、下一节点对象
				result =  CreateWorkJobNodeForWorkflowNode(wf, next_node, nodes, biz_id, org, user,tableId,wj_zid);
			}else if("3".equals(wf_now_node.getNode_type())) {
				//创建抄送节点
				HashMap<String,String> WorkJobNodeMap = new HashMap<String, String>();
				WorkJobNodeMap.put("tableId", "z_workjob_node");
				WorkJobNodeMap.put("zid", wf_now_node.getZid());//节点ID
				WorkJobNodeMap.put("pid", wj_zid);//工作流程实例主表主键
				WorkJobNodeMap.put("node_title", wf_now_node.getNode_title());//标题
				WorkJobNodeMap.put("node_type", wf_now_node.getNode_type());//节点类型
				WorkJobNodeMap.put("workflow_user_type", wf_now_node.getWorkflow_user_type());//处理人类型
				WorkJobNodeMap.put("node_user", wf_now_node.getNode_user());//处理人
				WorkJobNodeMap.put("node_role", wf_now_node.getNode_role());//处理角色
				WorkJobNodeMap.put("processing_mode", wf_now_node.getProcessing_mode());//审批方式
				WorkJobNodeMap.put("end_approval_role", wf_now_node.getEnd_approval_role());//审批终点角色
				WorkJobNodeMap.put("approverisnull", wf_now_node.getApproverisnull());//审批人为空时
				WorkJobNodeMap.put("approvals_up_num", wf_now_node.getApprovals_up_num());//向上审批层数
				WorkJobNodeMap.put("node_true", wf_now_node.getNode_true());//通过_下一节点 
				WorkJobNodeMap.put("node_false", wf_now_node.getNode_false());//通过_下一节点 
				insert(WorkJobNodeMap);


				//判读处理人【被通知人】类型--与审批人共用一个表。
				if("0".equals(wf_now_node.getWorkflow_user_type())) {
					//指定成员
					HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
					WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
					WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
					WorkJobNodeApprovaluserMap.put("userzid", wf_now_node.getNode_user());//【被通知人】
					WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
					insert(WorkJobNodeApprovaluserMap);
				}else if("1".equals(wf_now_node.getWorkflow_user_type())){
					//指定角色
					//获取角色用户列表
					List<String> roleUserList = selectListString("select userid from z_role_user where pid = '"+wf_now_node.getNode_role()+"'");
					if(roleUserList.size()==0) {
						z.Exception("工作流程错误：抄送节点中包括角色用户，但未获取到该角色下有效用户。");
					}else {
						for (String RoleUserId : roleUserList) {
							HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
							WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
							WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
							WorkJobNodeApprovaluserMap.put("userzid", RoleUserId);//【被通知人】
							WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
							insert(WorkJobNodeApprovaluserMap);
						}
					}
				}else if("2".equals(wf_now_node.getWorkflow_user_type())){
					//申请人直接主管
					HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
					WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
					WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
					//获取申请人直接领导
					String leaderId = getLeaderId(user.getZid(),org.getZid());
					if(z.isNull(leaderId)) {
						z.Exception("工作流程错误：抄送节点中包括直接领导，但未获取到申请人直接领导。");
					}
					WorkJobNodeApprovaluserMap.put("userzid", leaderId);//审批人
					WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
					insert(WorkJobNodeApprovaluserMap);

				}else if("3".equals(wf_now_node.getWorkflow_user_type())){
					//连续多级主管审批
					//获取多级审批领导ID
					int approvals_up_num = new Integer(wf_now_node.getApprovals_up_num());//向上审批层数
					List<String> LeaderIdList = new ArrayList<String>();
					getMultistageLeaderId(user.getZid(),org.getZid(),wf_now_node.getEnd_approval_role(),approvals_up_num,LeaderIdList);
					if(LeaderIdList.size()==0) {
						z.Exception("工作流程错误：抄送节点中包括连续多级主管，但未获取到多级主管ID。");
					}else {
						for (String leaderId : LeaderIdList) {
							HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
							WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
							WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
							WorkJobNodeApprovaluserMap.put("userzid", leaderId);//【被通知人】
							WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
							insert(WorkJobNodeApprovaluserMap);
						}
					}
				}else if("4".equals(wf_now_node.getWorkflow_user_type())){
					//表单指定部门负责人--根据表单中指定部门字段所保存的部门信息，找到该部门的负责人
					StringBuffer getOrgManagerUserzidSQL = new StringBuffer();
					getOrgManagerUserzidSQL.append("SELECT o.user_head ");
					getOrgManagerUserzidSQL.append("FROM "+tableId+" c ");
					getOrgManagerUserzidSQL.append("LEFT JOIN z_org o ON c."+z.columnsForColumnZid.get(wf_now_node.getForm_org()).getColumn_id()+" = o.zid ");
					getOrgManagerUserzidSQL.append("WHERE c.zid = '"+biz_id+"' ");

					//部门负责人
					String OrgManagerUserzid = selectString(getOrgManagerUserzidSQL.toString());
					HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
					WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
					WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
					WorkJobNodeApprovaluserMap.put("userzid", OrgManagerUserzid);//【被通知人】
					WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
					insert(WorkJobNodeApprovaluserMap);
				}else if("5".equals(wf_now_node.getWorkflow_user_type())){
					//主管副总
					String userid = GetLeader(org.getZid(),2);
					if(z.isNotNull(userid)) {
						HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
						WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
						WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
						WorkJobNodeApprovaluserMap.put("userzid", userid);//审批人
						WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
						insert(WorkJobNodeApprovaluserMap);
					}else {
						z.Exception("工作流程错误：节点中包括副总，但未获取到组织中副总级别的用户");
					}

				}else if("6".equals(wf_now_node.getWorkflow_user_type())){
					//主管总经理
					String userid = GetLeader(org.getZid(),1);
					if(z.isNotNull(userid)) {
						HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
						WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
						WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
						WorkJobNodeApprovaluserMap.put("userzid", userid);//审批人
						WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
						insert(WorkJobNodeApprovaluserMap);
					}else {
						z.Exception("工作流程错误：节点中包括副总，但未获取到组织中副总级别的用户");
					}

				}else if("7".equals(wf_now_node.getWorkflow_user_type())){
					//流程发起人
					HashMap<String,String> WorkJobNodeApprovaluserMap = new HashMap<String, String>();
					WorkJobNodeApprovaluserMap.put("tableId", "z_workjob_approvaluser");
					WorkJobNodeApprovaluserMap.put("pid", wf_now_node.getZid());//工作流程实例主表主键
					WorkJobNodeApprovaluserMap.put("userzid", user.getZid());//审批人
					WorkJobNodeApprovaluserMap.put("is_approval", "0");//是否处理
					insert(WorkJobNodeApprovaluserMap);
				}

				result =  CreateWorkJobNodeForWorkflowNode(wf, next_node, nodes, biz_id, org, user,tableId,wj_zid);

			}else if("4".equals(wf_now_node.getNode_type())) {
				//创建自动执行程序节点
				HashMap<String,String> WorkJobNodeMap = new HashMap<String, String>();
				WorkJobNodeMap.put("tableId", "z_workjob_node");
				WorkJobNodeMap.put("zid", wf_now_node.getZid());//节点ID
				WorkJobNodeMap.put("pid", wj_zid);//工作流程实例主表主键
				WorkJobNodeMap.put("node_title", wf_now_node.getNode_title());//标题
				WorkJobNodeMap.put("node_type", wf_now_node.getNode_type());//节点类型
				WorkJobNodeMap.put("node_action", wf_now_node.getNode_action());//处理方法
				WorkJobNodeMap.put("node_true", wf_now_node.getNode_true());//通过_下一节点 
				WorkJobNodeMap.put("node_false", wf_now_node.getNode_false());//通过_下一节点 
				insert(WorkJobNodeMap);
				result =  CreateWorkJobNodeForWorkflowNode(wf, next_node, nodes, biz_id, org, user,tableId,wj_zid);
			}else if("9".equals(wf_now_node.getNode_type())) {
				//创建结束节点
				HashMap<String,String> WorkJobNodeMap = new HashMap<String, String>();
				WorkJobNodeMap.put("tableId", "z_workjob_node");
				WorkJobNodeMap.put("zid", wf_now_node.getZid());//节点ID
				WorkJobNodeMap.put("pid", wj_zid);//工作流程实例主表主键
				WorkJobNodeMap.put("node_title", wf_now_node.getNode_title());//标题
				WorkJobNodeMap.put("node_type", wf_now_node.getNode_type());//节点类型
				insert(WorkJobNodeMap);

				//结束执行
				result =  true;
			}
		}


		return result;
	}

	//创建Form代码
	private void CreateBillForm(StringBuffer out_html,z_form_table table,HashMap<String,String> parameter,String editType) throws Exception{
	
		out_html.append("<div class=\"container-fluid\" style=\"padding:10px;\" >").append("\r\n");
		out_html.append("<div class=\"row\">").append("\r\n");
	
		for (z_form_table_column column : table.getColumns()) {
	
			if(!"zid".equals(column.getColumn_id()) && !"pid".equals(column.getColumn_id()))
	
				if("0".equals(column.getIs_hidden())) {//判读是否隐藏
	
					out_html.append("<div class=\"col-md-"+column.getColumn_size()+"\">").append("\r\n");
					out_html.append("<fieldset class=\"form-group\">").append("\r\n");
					out_html.append("<label data-toggle=\"tooltip\" data-placement=\"top\" title=\""+column.getColumn_help()+"\">【"+column.getColumn_name()+"】</label>").append("\r\n");
					if("0".equals(column.getColumn_type())) {//文本
						out_html.append("<input placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),editType)+"\" "+ColumnIsNull(column.getIs_null())+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
					}else if("1".equals(column.getColumn_type())) {//多行文本
						String textarea_height = "300";
						if(column.getTextarea_height()!=null && !"".equals(column.getTextarea_height())) {
							textarea_height = column.getTextarea_height();
						}
						out_html.append("<textarea placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" style=\"height: "+textarea_height+"px\" class=\"form-control\" "+ColumnIsNull(column.getIs_null())+ColumnIsReadonly(column.getIs_readonly())+">"+getColumnValue(parameter,column.getColumn_id(),editType)+"</textarea>").append("\r\n");
					}else if("2".equals(column.getColumn_type())) {//数字
						out_html.append("<input placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"number\" class=\"form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),editType)+"\"  "+ColumnIsNull(column.getIs_null())+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
					}else if("3".equals(column.getColumn_type())) {//文件
						out_html.append("<div class=\"input-group\">").append("\r\n");
						String ColumnValue = getColumnValue(parameter,column.getColumn_id(),editType);
						out_html.append("<input  placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" value=\""+getZ5DisplayValue(column,ColumnValue)+"\" type=\"url\" class=\"form-control\" "+ColumnIsNull(column.getIs_null())+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
						out_html.append("<span class=\"input-group-btn btn-group btn-group-sm\">").append("\r\n");
						out_html.append("<button class=\"btn btn-light\" onclick=\"uploadFile('"+column.getColumn_id()+"','"+parameter.get("zid")+"')\" type=\"button\"><i class=\"fa fa-upload\"></i></button>").append("\r\n");
						out_html.append("<button class=\"btn btn-light\" onclick=\"downloadFile('"+column.getColumn_id()+"')\" type=\"button\"><i class=\"fa fa-download\"></i></button>").append("\r\n");
						out_html.append("</span>").append("\r\n");
						out_html.append("</div>").append("\r\n");
					}else if("4".equals(column.getColumn_type())) {//图片
						out_html.append("<div class=\"input-group\">").append("\r\n");
						String ColumnValue = getColumnValue(parameter,column.getColumn_id(),editType);
						out_html.append("<input  placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" value=\""+getZ5DisplayValue(column,ColumnValue)+"\" type=\"url\" class=\"form-control\" "+ColumnIsNull(column.getIs_null())+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
						out_html.append("<span class=\"input-group-btn btn-group btn-group-sm\">").append("\r\n");
						out_html.append("<button class=\"btn btn-light\" onclick=\"uploadFile('"+column.getColumn_id()+"','"+parameter.get("zid")+"')\" type=\"button\"><i class=\"fa fa-upload\"></i></button>").append("\r\n");
						out_html.append("<button class=\"btn btn-light\" onclick=\"lookupImg('"+column.getColumn_id()+"')\" type=\"button\"><i class=\"fa fa-picture-o\"></i></button>").append("\r\n");
						out_html.append("</span>").append("\r\n");
						out_html.append("</div>").append("\r\n");
					}else if("5".equals(column.getColumn_type())) {//多选
						//获取Code
						if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
							z_code code = z.code.get(column.getP_code_id());
							if(code!=null && code.getZ_code_detail_list().size()>0) {
								List<z_code_detail> delailList = code.getZ_code_detail_list();
								out_html.append("<div>").append("\r\n");
								out_html.append("<label>").append("\r\n");
								String value = getColumnValue(parameter,column.getColumn_id(),editType);
								out_html.append("<input id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"hidden\"  value=\""+value+"\"/>").append("\r\n");
								for (z_code_detail code_d : delailList) {
									boolean ic = isChecked(value,code_d.getZ_key());
									if(ic) {
										out_html.append("<input onchange=\"getCheckedValue('"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_checked_id')\" type=\"checkbox\" checked=\"checked\" id=\""+column.getColumn_id()+"_checked_id\" value=\""+code_d.getZ_key()+"\" "+ColumnIsNull(column.getIs_null())+ColumnIsReadonly(column.getIs_readonly())+"> "+code_d.getZ_value()+" ").append("\r\n");
									}else {
										out_html.append("<input onchange=\"getCheckedValue('"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_checked_id')\" type=\"checkbox\"                     id=\""+column.getColumn_id()+"_checked_id\" value=\""+code_d.getZ_key()+"\" "+ColumnIsNull(column.getIs_null())+ColumnIsReadonly(column.getIs_readonly())+"> "+code_d.getZ_value()+" ").append("\r\n");
									}
	
								}
								out_html.append("</label>").append("\r\n");
								out_html.append("</div>").append("\r\n");
							}
	
						}
					}else if("6".equals(column.getColumn_type())) {//单选
						//获取Code
						if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
							z_code code = z.code.get(column.getP_code_id());
							if(code!=null && code.getZ_code_detail_list().size()>0) {
								List<z_code_detail> delailList = code.getZ_code_detail_list();
								out_html.append("<div>").append("\r\n");
								out_html.append("<label>").append("\r\n");
								for (z_code_detail code_d : delailList) {
									String value = getColumnValue(parameter,column.getColumn_id(),editType);
									if(value.equals(code_d.getZ_key())) {
										out_html.append("<input type=\"radio\" checked=\"checked\" name=\""+column.getColumn_id()+"\" id=\""+column.getColumn_id()+"_id\" value=\""+code_d.getZ_key()+"\" "+ColumnIsNull(column.getIs_null())+ColumnIsReadonly(column.getIs_readonly())+"> "+code_d.getZ_value()+" ").append("\r\n");
									}else {
										out_html.append("<input type=\"radio\" name=\""+column.getColumn_id()+"\" id=\""+column.getColumn_id()+"_id\" value=\""+code_d.getZ_key()+"\" "+ColumnIsNull(column.getIs_null())+ColumnIsReadonly(column.getIs_readonly())+"> "+code_d.getZ_value()+" ").append("\r\n");
									}
	
								}
								out_html.append("</label>").append("\r\n");
								out_html.append("</div>").append("\r\n");
							}
	
						}
					}else if("7".equals(column.getColumn_type())) {//下拉框
						out_html.append("<select class=\"form-control\" name=\""+column.getColumn_id()+"\" id=\""+column.getColumn_id()+"_id\" "+ColumnIsNull(column.getIs_null())+ColumnIsReadonly(column.getIs_readonly())+">").append("\r\n");
						out_html.append("<option value=\"\">请选择</option>").append("\r\n");
						//获取Code
						if(!"".equals(column.getP_code_id()) && column.getP_code_id()!=null) {
							z_code code = z.code.get(column.getP_code_id());
							if(code!=null && code.getZ_code_detail_list().size()>0) {
								List<z_code_detail> delailList = code.getZ_code_detail_list();
								for (z_code_detail code_d : delailList) {
									String value = getColumnValue(parameter,column.getColumn_id(),editType);
									if(value.equals(code_d.getZ_key())) {
										out_html.append("<option value=\""+code_d.getZ_key()+"\" selected=\"selected\">"+code_d.getZ_value()+"</option>").append("\r\n");
									}else {
										out_html.append("<option value=\""+code_d.getZ_key()+"\" >"+code_d.getZ_value()+"</option>").append("\r\n");
									}
								}
							}
						}
						out_html.append("</select>").append("\r\n");
					}else if("8".equals(column.getColumn_type())) {//Z5
						out_html.append("<div class=\"input-group\">").append("\r\n");
						String ColumnValue = getColumnValue(parameter,column.getColumn_id(),editType);
						String SelectDataOnClick = "onclick=\"Z5list('"+table.getTable_id()+"_"+column.getColumn_id()+"','"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_display',0)\"";
						out_html.append("<input id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"hidden\"  value=\""+ColumnValue+"\"/>").append("\r\n");
						out_html.append("<input  placeholder=\""+column.getColumn_help()+"\" readonly=\"true\" id=\""+column.getColumn_id()+"_display\" "+SelectDataOnClick+" name=\""+column.getColumn_id()+"_display\" value=\""+getZ5DisplayValue(column,ColumnValue)+"\" type=\"text\" class=\"form-control\" "+ColumnIsNull(column.getIs_null())+ColumnIsDisabled(column.getIs_readonly())+"/>").append("\r\n");
						out_html.append("<span class=\"input-group-append\">").append("\r\n");
						out_html.append("<button class=\"btn btn-light\" onclick=\"Z5Clear('"+table.getTable_id()+"_"+column.getColumn_id()+"','"+column.getColumn_id()+"_id','"+column.getColumn_id()+"_display')\" type=\"button\" "+ColumnIsDisabled(column.getIs_readonly())+"><i class=\"fa fa-trash-o\"></i></button>").append("\r\n");
						out_html.append("<button id=\""+column.getColumn_id()+"_selectbutton_id\" class=\"btn btn-light\" "+SelectDataOnClick+" type=\"button\" "+ColumnIsDisabled(column.getIs_readonly())+"><i class=\"fa fa-search\"></i></button>").append("\r\n");
						out_html.append("</span>").append("\r\n");
						out_html.append("</div>").append("\r\n");
	
					}else if("9".equals(column.getColumn_type())) {//日期
						out_html.append("<input  placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"Wdate form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),editType)+"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd'})\" "+ColumnIsNull(column.getIs_null())+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
					}else if("10".equals(column.getColumn_type())) {//日期时间
						out_html.append("<input  placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" type=\"text\" class=\"Wdate form-control\" value=\""+getColumnValue(parameter,column.getColumn_id(),editType)+"\" onFocus=\"WdatePicker({isShowClear:true,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm:ss'})\" "+ColumnIsNull(column.getIs_null())+ColumnIsReadonly(column.getIs_readonly())+"/>").append("\r\n");
					}else if("11".equals(column.getColumn_type())) {//HTML输入框
						String textarea_height = "300";
						if(column.getTextarea_height()!=null && !"".equals(column.getTextarea_height())) {
							textarea_height = column.getTextarea_height();
						}
						out_html.append("<textarea  placeholder=\""+column.getColumn_help()+"\" id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" style=\"height: "+textarea_height+"px\" "+ColumnIsNull(column.getIs_null())+ColumnIsReadonly(column.getIs_readonly())+">"+getColumnValue(parameter,column.getColumn_id(),editType)+"</textarea>").append("\r\n");
						out_html.append("<script type=\"text/javascript\">InitHTMLColumn('"+column.getColumn_id()+"_id');</script>").append("\r\n");
					}else if("12".equals(column.getColumn_type())) {//源码输入框
						String textarea_height = "300";
						if(column.getTextarea_height()!=null && !"".equals(column.getTextarea_height())) {
							textarea_height = column.getTextarea_height();
						}
						out_html.append("<textarea  placeholder=\""+column.getColumn_help()+"\"  id=\""+column.getColumn_id()+"_id\" name=\""+column.getColumn_id()+"\" style=\"height: "+textarea_height+"px\" "+ColumnIsNull(column.getIs_null())+ColumnIsReadonly(column.getIs_readonly())+">"+getColumnValue(parameter,column.getColumn_id(),editType)+"</textarea>").append("\r\n");
						out_html.append("<script type=\"text/javascript\">InitCodeColumn('"+column.getColumn_id()+"_id','"+column.getMode_type()+"','"+textarea_height+"');</script>").append("\r\n");
					}
					out_html.append("</fieldset>").append("\r\n");
					out_html.append("</div>").append("\r\n");
	
					//添加换行
					if("1".equals(column.getIsbr())) {
						out_html.append("<div class=\"col-md-12\"></div>").append("\r\n");
					}
	
					//添加分割线
					if("1".equals(column.getIshr())) {
						out_html.append("<div class=\"col-md-12\"><hr/></div>").append("\r\n");
					}
	
				}
		}
		out_html.append("</div>").append("\r\n");
		out_html.append("</div>").append("\r\n");
	
	}


	/**
		 * 运行节点处理
		 * @param now_node
		 * @param nodes
		 * @param newApprovalUser 新的审批用户
		 * @param biz_id 
		 * @param tableId 
		 * @throws Exception
		 */
		private void WorkJobNodeHandler(z_workjob_node now_node,Map<String,z_workjob_node> nodes,String UserId,String OrgId,String ExamineState,String ExamineOpinion, String newApprovalUser, String tableId, String biz_id) throws Exception {
			if(z.isNotNull(now_node) && z.isNotNull(nodes) && nodes.size()>0) {
				//判读当前节点是否处理，如果处理了，调用下一级节点
				if(!"2".equals(now_node.getIs_approval())) {
					//0:未处理    1：处理中  2：已处理
	
					//判读节点类型
					if("0".equals(now_node.getNode_type())) {
						//处理开始节点
						z_workjob_node updateNode = new z_workjob_node();
						updateNode.setZid(now_node.getZid());
						updateNode.setIs_approval("2");//已处理
						update("z_workjob_node_update_zid",updateNode);
	
						//自动处理下一节点
						z_workjob_node next_node = nodes.get(now_node.getNode_true());
						if(z.isNull(next_node)) {
							next_node = nodes.get(now_node.getNode_false());
						}
						WorkJobNodeHandler(next_node,nodes,UserId,OrgId,null,null,null,tableId,biz_id);
	
					}else if("1".equals(now_node.getNode_type())) {
						//处理分支节点
						
						z_workjob_node updateNode = new z_workjob_node();
						updateNode.setZid(now_node.getZid());
						updateNode.setIs_approval("2");//已处理
						update("z_workjob_node_update_zid",updateNode);
	
						//自动处理下一节点
						z_workjob_node next_node = nodes.get(now_node.getNode_true());
						if(z.isNull(next_node)) {
							next_node = nodes.get(now_node.getNode_false());
						}
						WorkJobNodeHandler(next_node,nodes,UserId,OrgId,null,null,null,tableId,biz_id);
	
					}else if("2".equals(now_node.getNode_type())) {
						//处理人工节点
	
						//更新流程主表当前处理节点
						z_workjob wj = new z_workjob();
						wj.setZid(now_node.getPid());
						wj.setNow_node(now_node.getZid());
						update("z_workjob_update_zid", wj);
						
						//通知审批人执行审批
						List<z_workjob_approvaluser> userList = now_node.getZ_workjob_approvaluser_list();
						for (z_workjob_approvaluser user : userList) {
							SentOaMassage("sa", user.getUserzid(),now_node.getNode_title());
						}
	
						if(z.isNotNull(ExamineState)) {
							//获取所有审批人员
							List<z_workjob_approvaluser> approvaluserList = now_node.getZ_workjob_approvaluser_list();
							if(z.isNull(approvaluserList) || approvaluserList.size()==0) {
								z.Exception("运行推动工作流程处理出错|人工节点审批，审批人员为空。节点ID："+now_node.getZid());
							}
	
							if(UserId.equals("sa")) {
								ExamineOpinion = "【系统管理员代审】："+ExamineOpinion;
							}
	
							//是否审批结束
							boolean isapprovalend = false;
	
							if("2".equals(ExamineState)) {
								//转审
								for (z_workjob_approvaluser au : approvaluserList) {
									//判读当前审批用户是否禁用如果禁用，并且当前登录人为sa就通过审批【管理员代审】
									z_user au_user = selectBean("z_user_select_zid", au.getUserzid());
									if(UserId.equals(au.getUserzid())  ||   ( !"1".equals(au_user.getIs_start()) && "sa".equals(UserId)  )) {
	
										//处理审批人明细表信息
										z_workjob_approvaluser auupdate = new z_workjob_approvaluser();
										auupdate.setZid(au.getZid());
										auupdate.setIs_approval("2");
										auupdate.setUserzid(UserId);
										auupdate.setExamine_state(ExamineState);
										auupdate.setExamine_opinion(ExamineOpinion);
										update("z_workjob_approvaluser_update_zid", auupdate);
	
	
										//处理主表信息
										z_workjob_node updateNode = new z_workjob_node();
										updateNode.setZid(now_node.getZid());
										updateNode.setIs_approval("1");//处理中
										updateNode.setIs_temporary("1");//是否加签
										updateNode.setProcessing_mode("1");//会签
										updateNode.setExamine_state(ExamineState);//处理结果
										updateNode.setExamine_opinion(ExamineOpinion);//处理意见
										update("z_workjob_node_update_zid", updateNode);
	
	
										//新增一名审批人
										z_workjob_approvaluser new_auupdate = new z_workjob_approvaluser();
										new_auupdate.setZid(z.newZid("z_workjob_approvaluser"));
										new_auupdate.setPid(au.getPid());
										new_auupdate.setIs_approval("0");
										new_auupdate.setUserzid(newApprovalUser);//新的审批人
										insert("z_workjob_approvaluser_insert", new_auupdate);
	
										break;
									}
	
								}
	
							}else {
								
								if(approvaluserList.size()==1) {
									//如果审批人只有一人，直接审批
									z_user au_user = selectBean("z_user_select_zid", approvaluserList.get(0).getUserzid());
									
									if(UserId.equals(approvaluserList.get(0).getUserzid())  ||   ( !"1".equals(au_user.getIs_start()) && "sa".equals(UserId)  )) {
	
										//处理审批人明细表信息
										z_workjob_approvaluser auupdate = new z_workjob_approvaluser();
										auupdate.setZid(approvaluserList.get(0).getZid());
										auupdate.setIs_approval("2");
										auupdate.setUserzid(UserId);
										auupdate.setExamine_state(ExamineState);
										auupdate.setExamine_opinion(ExamineOpinion);
										update("z_workjob_approvaluser_update_zid", auupdate);
	
										//处理主表信息
										z_workjob_node updateNode = new z_workjob_node();
										updateNode.setZid(now_node.getZid());
										updateNode.setIs_approval("2");//已处理
										updateNode.setExamine_state(ExamineState);//处理结果
										updateNode.setExamine_opinion(ExamineOpinion);//处理意见
										update("z_workjob_node_update_zid", updateNode);
	
										//设置人工审批结束
										isapprovalend = true;
									}
									
									
									
								}else {
									//判读审批方式 0：或签  1：会签
									if("0".equals(now_node.getProcessing_mode())) {
										for (z_workjob_approvaluser au : approvaluserList) {
	
											//判读当前审批用户是否禁用如果禁用，并且当前登录人为sa就通过审批【管理员代审】
											z_user au_user = selectBean("z_user_select_zid", au.getUserzid());
	
											if(UserId.equals(au.getUserzid())  ||   ( !"1".equals(au_user.getIs_start()) && "sa".equals(UserId)  )) {
	
												//处理审批人明细表信息
												z_workjob_approvaluser auupdate = new z_workjob_approvaluser();
												auupdate.setZid(au.getZid());
												auupdate.setIs_approval("2");
												auupdate.setUserzid(UserId);
												auupdate.setExamine_state(ExamineState);
												auupdate.setExamine_opinion(ExamineOpinion);
												update("z_workjob_approvaluser_update_zid", auupdate);
	
												//处理主表信息
												z_workjob_node updateNode = new z_workjob_node();
												updateNode.setZid(now_node.getZid());
												updateNode.setIs_approval("2");//已处理
												updateNode.setExamine_state(ExamineState);//处理结果
												updateNode.setExamine_opinion(ExamineOpinion);//处理意见
												update("z_workjob_node_update_zid", updateNode);
	
												//设置人工审批结束
												isapprovalend = true;
												//如果是或签，有用户审批就跳出循环
												break;
											}
										}
									}else if("1".equals(now_node.getProcessing_mode())) {
										for (z_workjob_approvaluser au : approvaluserList) {
	
											//判读当前审批用户是否禁用如果禁用，并且当前登录人为sa就通过审批【管理员代审】
											z_user au_user = selectBean("z_user_select_zid", au.getUserzid());
	
											if(UserId.equals(au.getUserzid()) ||   ( !"1".equals(au_user.getIs_start()) && "sa".equals(UserId)  )) {
	
												//处理审批人明细表信息
												z_workjob_approvaluser auupdate = new z_workjob_approvaluser();
												auupdate.setZid(au.getZid());
												auupdate.setIs_approval("2");
												auupdate.setUserzid(UserId);
												auupdate.setExamine_state(ExamineState);
												auupdate.setExamine_opinion(ExamineOpinion);
												update("z_workjob_approvaluser_update_zid", auupdate);
	
												//处理主表信息
												z_workjob_node updateNode = new z_workjob_node();
												updateNode.setZid(now_node.getZid());
												updateNode.setIs_approval("1");//已处理
												updateNode.setExamine_state(ExamineState);//处理结果
												updateNode.setExamine_opinion(ExamineOpinion);//处理意见
												update("z_workjob_node_update_zid", updateNode);
	
											}
										}
	
										int not_approval_user = selectInt("select count(*) from z_workjob_approvaluser where is_approval !=2 and pid = '"+now_node.getZid()+"'");
										if(not_approval_user==0) {
											//设置人工审批结束
											isapprovalend = true;
										}
	
	
	
									}else{
										z.Exception("运行推动工作流程处理出错|人工节点审批，未设置审批方式。节点ID："+now_node.getZid());
									}
								}
								
							}
	
	
							//如果人工审批结束进行下一节点
							if(isapprovalend) {
								z_workjob_node new_now_node = selectBean("z_workjob_node_select_zid", now_node.getZid());
								if(z.isNotNull(new_now_node) 
										&& z.isNotNull(new_now_node.getIs_approval()) 
										&& z.isNotNull(new_now_node.getExamine_state())) {
	
									if("1".equals(new_now_node.getExamine_state())) {
										//通过
										z_workjob_node next_node = nodes.get(now_node.getNode_true());
										WorkJobNodeHandler(next_node,nodes,UserId,OrgId,null,null,null,tableId,biz_id);
									}else if("0".equals(new_now_node.getExamine_state())) {
										//未通过
										z_workjob_node next_node = nodes.get(now_node.getNode_false());
										WorkJobNodeHandler(next_node,nodes,UserId,OrgId,null,null,null,tableId,biz_id);
									}else {
										z.Exception("运行推动工作流程处理出错|人工节点审批完成，下一节点处理出错,审批不为0或1。节点ID："+now_node.getZid());
									}
								}else {
									z.Exception("运行推动工作流程处理出错|人工节点审批完成，下一节点处理出错,参数为空。节点ID："+now_node.getZid());
								}
							}
						}else {
							//审批人为空时处理方式  0:自动通过    1:自动转交管理员
							if("0".equals(now_node.getApproverisnull())) {
								//如果当前节点为人工节点，需要判读人工节点中是否包括已禁用的账户。
								List<z_workjob_approvaluser> approvaluserList = now_node.getZ_workjob_approvaluser_list();
								for (z_workjob_approvaluser au : approvaluserList) {
									z_user au_user = selectBean("z_user_select_zid", au.getUserzid());
									//判读用户是否可用。
									if(!"1".equals(au_user.getIs_start())) {
										//处理审批人明细表信息
										z_workjob_approvaluser auupdate = new z_workjob_approvaluser();
										auupdate.setZid(au.getZid());
										auupdate.setIs_approval("2");
										auupdate.setUserzid(au_user.getZid());
										auupdate.setExamine_state("1");
										auupdate.setExamine_opinion("当前节点审批人【"+au_user.getUser_name()+"】未启用，根据流程模板设置自动通过审批");
										update("z_workjob_approvaluser_update_zid", auupdate);
									}
								}
								//如果所有审批人都审批完成，进行下一节点执行
								int not_approval_user = selectInt("select count(*) from z_workjob_approvaluser where is_approval !=2 and pid = '"+now_node.getZid()+"'");
								if(not_approval_user==0) {
									//如果不设置审批人为空时处理方式，默认不处理。等待审批
									z_workjob_node updateNode = new z_workjob_node();
									updateNode.setZid(now_node.getZid());
									updateNode.setIs_approval("2");//已完成
									update("z_workjob_node_update_zid",updateNode);
	
									//调用下一节点
									z_workjob_node next_node = nodes.get(now_node.getNode_true());
									WorkJobNodeHandler(next_node,nodes,UserId,OrgId,null,null,null,tableId,biz_id);
	
								}
							}else if("1".equals(now_node.getApproverisnull())) {
								//如果当前节点为人工节点，需要判读人工节点中是否包括已禁用的账户。
								List<z_workjob_approvaluser> approvaluserList = now_node.getZ_workjob_approvaluser_list();
								for (z_workjob_approvaluser au : approvaluserList) {
									z_user au_user = selectBean("z_user_select_zid", au.getUserzid());
									//判读用户是否可用。
									if(!"1".equals(au_user.getIs_start())) {
										//通知管理员审批
										SentOaMassage(au_user.getZid(), "sa", "【"+now_node.getNode_title()+"】|"+"当前节点审批人【"+au_user.getUser_name()+"】未启用，根据流程模板设置当前审批交由管理员审批");
									}
								}
	
							}else {
								//如果不设置审批人为空时处理方式，默认不处理。等待审批
								z_workjob_node updateNode = new z_workjob_node();
								updateNode.setZid(now_node.getZid());
								updateNode.setIs_approval("1");//处理中
								update("z_workjob_node_update_zid",updateNode);
							}
	
	
	
						}
	
					}else if("3".equals(now_node.getNode_type())) {
						//处理抄送节点
	
						//获取所有抄送人
						List<z_workjob_approvaluser> userList = now_node.getZ_workjob_approvaluser_list();
						StringBuffer SendState = new StringBuffer();//发送结果信息
						for (z_workjob_approvaluser user : userList) {
							Result sr = SentOaMassage("sa", user.getUserzid(),now_node.getNode_title());
							z_user u = z.users.get(user.getUserzid());
							if(Code.SUCCESS.equals(sr.getCode())) {
								SendState.append("【接收人："+u.getUser_name()+"|信息发送成功】").append("\r\n");
							}else {
								SendState.append("【接收人："+u.getUser_name()+"|信息发送失败,请以线下方式通知该用户。|"+sr.getMsg()+"】").append("\r\n");
							}
						}
	
						//变更所有接收人接收状态
						z_workjob_approvaluser auupdate = new z_workjob_approvaluser();
						auupdate.setPid(now_node.getZid());
						auupdate.setIs_approval("2");
						update("z_workjob_approvaluser_update_pid", auupdate);
						
						z_workjob_node updateNode = new z_workjob_node();
						updateNode.setZid(now_node.getZid());
						updateNode.setIs_approval("2");//已完成
						updateNode.setExamine_opinion("消息发送结果："+SendState);
						update("z_workjob_node_update_zid",updateNode);
	
						//调用下一节点
						z_workjob_node next_node = nodes.get(now_node.getNode_true());
						WorkJobNodeHandler(next_node,nodes,UserId,OrgId,null,null,null,tableId,biz_id);
	
					}else if("4".equals(now_node.getNode_type())) {
						//处理自动执行程序节点
						//获取要执行的接口
						z_http_services service = z.httpservices_zid.get(now_node.getNode_action());
						if(z.isNotNull(service)) {
							Map<String,String> parameters = new HashMap<String, String>();
							parameters.put("biz_id", biz_id);
							parameters.put("tableId", tableId);
							//调用
							Result service_result = HttpUtil.doPostServices(service,parameters);
							if(z.isNotNull(service_result)) {
								if(Code.SUCCESS.equals(service_result.getCode())) {
									//执行成功
									z_workjob_node updateNode = new z_workjob_node();
									updateNode.setZid(now_node.getZid());
									updateNode.setIs_approval("2");//已完成
									updateNode.setExamine_opinion("自动调用接口:"+service.getTitle()+"|执行结果：成功|执行调用【"+nodes.get(now_node.getNode_true()).getNode_title()+"】");
									update("z_workjob_node_update_zid",updateNode);
									
									//调用下一节点
									z_workjob_node next_node = nodes.get(now_node.getNode_true());
									WorkJobNodeHandler(next_node,nodes,UserId,OrgId,null,null,null,tableId,biz_id);
								}
								if(Code.ERROR.equals(service_result.getCode())) {
									//执行出错
									z_workjob_node updateNode = new z_workjob_node();
									updateNode.setZid(now_node.getZid());
									updateNode.setIs_approval("2");//已完成
									updateNode.setExamine_opinion("自动调用接口:"+service.getTitle()+"|执行结果：失败|执行调用【"+nodes.get(now_node.getNode_false()).getNode_title()+"】");
									update("z_workjob_node_update_zid",updateNode);
									
									//调用下一节点
									z_workjob_node next_node = nodes.get(now_node.getNode_false());
									WorkJobNodeHandler(next_node,nodes,UserId,OrgId,null,null,null,tableId,biz_id);
								}
							}else {
								z.Exception("运行推动工作流程处理出错|处理自动执行程序节点，接口调用返回结果为空，请检查接口代码是否正确。service_result is null  节点ID："+now_node.getZid()+"|"+now_node.getNode_title());
							}
	
						}else {
							z.Exception("运行推动工作流程处理出错|处理自动执行程序节点，未找到关联接口  节点ID："+now_node.getZid()+"|"+now_node.getNode_title());
						}
	
					}else if("9".equals(now_node.getNode_type())) {
						//处理结束节点
	
						//更新流程主表当前处理节点
						z_workjob wj = new z_workjob();
						wj.setZid(now_node.getPid());
						wj.setNow_node(now_node.getZid());
						update("z_workjob_update_zid", wj);
	
	
					}
				}else{
					//如果当前节点已处理，调用下一节点[调用前需要判读，是调用通过节点，还是未通过节点]
					z_workjob_node next_node = null;
					if("1".equals(ExamineState)) {
						next_node = nodes.get(now_node.getNode_true());
					}else if("0".equals(ExamineState)) {
						next_node = nodes.get(now_node.getNode_false());
					}
					if(z.isNotNull(next_node)) {
						WorkJobNodeHandler(next_node,nodes,UserId,OrgId,null,null,null,tableId,biz_id);
					}
				}
			}else {
				z.Exception("运行推动工作流程处理出错|节点ID："+now_node.getZid()+"|"+now_node.getNode_title());
			}
		}


	/**
	 * 运行推动工作流程处理
	 * @param wj_zid 工作流程ID
	 * @param NodeId 节点ID
	 * @param UserId 用户ID
	 * @param OrgId 组织ID
	 * @param ExamineState 审批结果 0：未通过  1：通过 2:转审
	 * @param ExamineOpinion 审批意见
	 * @param newApprovalUser 新的审批用户
	 * @param biz_id 
	 * @param tableId 
	 * @throws Exception
	 */
	protected void RunWorkJobHandler(String wj_zid,String NodeId,String UserId,String OrgId,String ExamineState,String ExamineOpinion, String newApprovalUser) throws Exception{
		//获取流程对象
		z_workjob wj = selectBean("z_workjob_select_zid", wj_zid);
		//获取所有流程节点
		z_workjob_node query_parameter = new z_workjob_node();
		z_workjob_node start_node = null;
		z_workjob_node now_node = null;
		query_parameter.setPid(wj_zid);
		List<z_workjob_node> nodeList = selectList("z_workjob_node_select",query_parameter);
		Map<String,z_workjob_node> nodes = new HashMap<String, z_workjob_node>();
		for (z_workjob_node node : nodeList) {
	
			//获取审批人员
			List<z_workjob_approvaluser> auserList = selectList("z_workjob_approvaluser_select_pid", node.getZid());
			node.setZ_workjob_approvaluser_list(auserList);
	
			//获取开始节点
			if("0".equals(node.getNode_type())){
				start_node = node;
			}
	
			//获取当前节点
			if(NodeId.equals(node.getZid())){
				now_node = node;
			}
	
			nodes.put(node.getZid(), node);
		}
		//开始执行
		if(z.isNotNull(NodeId)) {
			WorkJobNodeHandler(now_node,nodes,UserId,OrgId,ExamineState,ExamineOpinion,newApprovalUser,wj.getTable_id(),wj.getBiz_id());
		}else {
			//如果当前节点为空，就从开始节点开始执行
			WorkJobNodeHandler(start_node,nodes,UserId,OrgId,ExamineState,ExamineOpinion,null,wj.getTable_id(),wj.getBiz_id());
		}
	
	
	}


	/**
	 * 	获取通过分支
	 * @param wf_now_node
	 * @param tableId
	 * @param biz_id
	 * @return
	 * @throws Exception
	 */
	private z_workflow_node_branch getOkBranch(z_workflow_node wf_now_node, String tableId, String biz_id) throws Exception {
		z_workflow_node_branch okbranch = null;
		//获取有效分支，返回下一节点
		List<z_workflow_node_branch> branchList = wf_now_node.getZ_workflow_node_branch_list();
		for (z_workflow_node_branch branch : branchList) {
			//验证通过条件
			//判读第一条件是否为空，如果为空，直接返回
			if(z.isNotNull(branch.getColumn_compare()) && z.isNotNull(branch.getColumn_value())) {
				//获取字段
				z_form_table_column column = z.columnsForColumnZid.get(branch.getColumnid());
		
				//form表单字段值
				String formColumnValue = selectString("select "+column.getColumn_id()+" from "+tableId+" where zid = '"+biz_id+"'");
		
				//条件比较结果
				boolean isOK = false;
				boolean isOK2 = false;
		
				//根据字段类型，限定比较方式
				if("2".equals(column.getColumn_type())) {//数字
		
					//将表单字段值转为BigDecimail
					BigDecimal formColumnValueBigDecimal = new BigDecimal(formColumnValue);
					BigDecimal column_value_BigDecimal = new BigDecimal(branch.getColumn_value());
					
		
		
					//判读第一条件
					if("0".equals(branch.getColumn_compare())) {
						//等于
						if(formColumnValueBigDecimal.compareTo(column_value_BigDecimal)==0) {
							isOK = true;
						}
					}else if("1".equals(branch.getColumn_compare())) {
						//大于
						if(formColumnValueBigDecimal.compareTo(column_value_BigDecimal)>0) {
							isOK = true;
						}
					}else if("2".equals(branch.getColumn_compare())) {
						//大于等于
						if(formColumnValueBigDecimal.compareTo(column_value_BigDecimal)>=0) {
							isOK = true;
						}
					}else if("3".equals(branch.getColumn_compare())) {
						//小于
						if(formColumnValueBigDecimal.compareTo(column_value_BigDecimal)<0) {
							isOK = true;
						}
					}else if("4".equals(branch.getColumn_compare())) {
						//小于等于
						if(formColumnValueBigDecimal.compareTo(column_value_BigDecimal)<=0) {
							isOK = true;
						}
					}
		
		
					//如查第二条件与比较符不为空，比较第二条件
					if(z.isNotNull(branch.getColumn_compare2()) && z.isNotNull(branch.getColumn_value2())){
						BigDecimal column_value2_BigDecimal = new BigDecimal(branch.getColumn_value2());
						if("0".equals(branch.getColumn_compare2())) {
							//等于
							if(formColumnValueBigDecimal.compareTo(column_value2_BigDecimal)==0) {
								isOK2 = true;
							}
						}else if("1".equals(branch.getColumn_compare2())) {
							//大于
							if(formColumnValueBigDecimal.compareTo(column_value2_BigDecimal)>0) {
								isOK2 = true;
							}
						}else if("2".equals(branch.getColumn_compare2())) {
							//大于等于
							if(formColumnValueBigDecimal.compareTo(column_value2_BigDecimal)>=0) {
								isOK2 = true;
							}
						}else if("3".equals(branch.getColumn_compare2())) {
							//小于
							if(formColumnValueBigDecimal.compareTo(column_value2_BigDecimal)<0) {
								isOK2 = true;
							}
						}else if("4".equals(branch.getColumn_compare2())) {
							//小于等于
							if(formColumnValueBigDecimal.compareTo(column_value2_BigDecimal)<=0) {
								isOK2 = true;
							}
						}
					}
		
		
		
				}else if("9".equals(column.getColumn_type())) {//日期
					//将表单字段值转为BigDecimail
					BigDecimal formColumnValueBigDecimal = new BigDecimal(formColumnValue);
					Date formColumnValueBigDate = DateUtil.StringToDate(formColumnValue, "yyyy-MM-dd");
					Date column_value_date = DateUtil.StringToDate(branch.getColumn_value(), "yyyy-MM-dd");
					Date column_value2_date = DateUtil.StringToDate(branch.getColumn_value2(), "yyyy-MM-dd");
		
					//判读第一条件
					if("0".equals(branch.getColumn_compare())) {
						//等于
						if(formColumnValueBigDate.compareTo(column_value_date)==0) {
							isOK = true;
						}
					}else if("1".equals(branch.getColumn_compare())) {
						//大于
						if(formColumnValueBigDate.compareTo(column_value_date)>0) {
							isOK = true;
						}
					}else if("2".equals(branch.getColumn_compare())) {
						//大于等于
						if(formColumnValueBigDate.compareTo(column_value_date)>=0) {
							isOK = true;
						}
					}else if("3".equals(branch.getColumn_compare())) {
						//小于
						if(formColumnValueBigDate.compareTo(column_value_date)<0) {
							isOK = true;
						}
					}else if("4".equals(branch.getColumn_compare())) {
						//小于等于
						if(formColumnValueBigDate.compareTo(column_value_date)<=0) {
							isOK = true;
						}
					}
		
		
					//如查第二条件与比较符不为空，比较第二条件
					if(z.isNotNull(branch.getColumn_compare2()) && z.isNotNull(branch.getColumn_value2())){
						if("0".equals(branch.getColumn_compare2())) {
							//等于
							if(formColumnValueBigDate.compareTo(column_value2_date)==0) {
								isOK2 = true;
							}
						}else if("1".equals(branch.getColumn_compare2())) {
							//大于
							if(formColumnValueBigDate.compareTo(column_value2_date)>0) {
								isOK2 = true;
							}
						}else if("2".equals(branch.getColumn_compare2())) {
							//大于等于
							if(formColumnValueBigDate.compareTo(column_value2_date)>=0) {
								isOK2 = true;
							}
						}else if("3".equals(branch.getColumn_compare2())) {
							//小于
							if(formColumnValueBigDate.compareTo(column_value2_date)<0) {
								isOK2 = true;
							}
						}else if("4".equals(branch.getColumn_compare2())) {
							//小于等于
							if(formColumnValueBigDate.compareTo(column_value2_date)<=0) {
								isOK2 = true;
							}
						}
					}
				}else if("10".equals(column.getColumn_type())) {//日期时间
					//将表单字段值转为BigDecimail
					BigDecimal formColumnValueBigDecimal = new BigDecimal(formColumnValue);
					Date formColumnValueBigDate = DateUtil.StringToDate(formColumnValue, "yyyy-MM-dd HH:mm:ss");
					Date column_value_date = DateUtil.StringToDate(branch.getColumn_value(), "yyyy-MM-dd HH:mm:ss");
					Date column_value2_date = DateUtil.StringToDate(branch.getColumn_value2(), "yyyy-MM-dd HH:mm:ss");
		
					//判读第一条件
					if("0".equals(branch.getColumn_compare())) {
						//等于
						if(formColumnValueBigDate.compareTo(column_value_date)==0) {
							isOK = true;
						}
					}else if("1".equals(branch.getColumn_compare())) {
						//大于
						if(formColumnValueBigDate.compareTo(column_value_date)>0) {
							isOK = true;
						}
					}else if("2".equals(branch.getColumn_compare())) {
						//大于等于
						if(formColumnValueBigDate.compareTo(column_value_date)>=0) {
							isOK = true;
						}
					}else if("3".equals(branch.getColumn_compare())) {
						//小于
						if(formColumnValueBigDate.compareTo(column_value_date)<0) {
							isOK = true;
						}
					}else if("4".equals(branch.getColumn_compare())) {
						//小于等于
						if(formColumnValueBigDate.compareTo(column_value_date)<=0) {
							isOK = true;
						}
					}
		
		
					//如查第二条件与比较符不为空，比较第二条件
					if(z.isNotNull(branch.getColumn_compare2()) && z.isNotNull(branch.getColumn_value2())){
						if("0".equals(branch.getColumn_compare2())) {
							//等于
							if(formColumnValueBigDate.compareTo(column_value2_date)==0) {
								isOK2 = true;
							}
						}else if("1".equals(branch.getColumn_compare2())) {
							//大于
							if(formColumnValueBigDate.compareTo(column_value2_date)>0) {
								isOK2 = true;
							}
						}else if("2".equals(branch.getColumn_compare2())) {
							//大于等于
							if(formColumnValueBigDate.compareTo(column_value2_date)>=0) {
								isOK2 = true;
							}
						}else if("3".equals(branch.getColumn_compare2())) {
							//小于
							if(formColumnValueBigDate.compareTo(column_value2_date)<0) {
								isOK2 = true;
							}
						}else if("4".equals(branch.getColumn_compare2())) {
							//小于等于
							if(formColumnValueBigDate.compareTo(column_value2_date)<=0) {
								isOK2 = true;
							}
						}
					}
				}else {
					//判读第一条件
					if(formColumnValue.equals(branch.getColumn_value())) {
						isOK = true;
					}
					//如查第二条件与比较符不为空，比较第二条件
					if(z.isNotNull(branch.getColumn_compare2()) && z.isNotNull(branch.getColumn_value2()) && formColumnValue.equals(branch.getColumn_value2())) {
						isOK2 = true;
					}
				}
		
		
		
				//如果连接不为空-根据连接符比较条件一与条件与结果值 
				if("1".equals(branch.getConnector_type())) {
					//并且连接符
					if(isOK && isOK2) {
						okbranch = branch;
					}
				}else if("2".equals(branch.getConnector_type())) {
					//或者连接符
					if(isOK || isOK2) {
						okbranch = branch;
					}
				}else {
					//其它情况，只判读条件1是否成功
					if(isOK) {
						okbranch = branch;
					}
				}
		
		
		
			}
			
			if(z.isNotNull(okbranch)) {
				//跳出循环体
				break;
			}
		}
		//判读下一节点ID是否为空
		if(z.isNull(okbranch)) {
			z.Exception("工作流程错误：无有效分支流程，请检查模板定义情况。");
		}
		return okbranch;
	}


	/**
	 * 	获取直接领导
	 * @param orgid 当前组织 
	 * @param role_level 目标领导级别 
	 *  0
		董事长

		1
		总裁

		2
		总经理

		3
		副总

		4
		总监

		5
		经理

		6
		主管

		7
		员工

		8
		实习

	 * @return
	 */
	private String GetLeader(String orgid, int role_level) {
		String result = "";
		if(z.isNotNull(orgid)) {
			//获取当前组织
			z_org org = z.orgs.get(orgid);
			if(z.isNotNull(org)) {
				//获取当前组织负责人
				String userid = org.getUser_head();
				z_user hu = z.users.get(userid);
				//当前负责人不为空，并且用户级别不为空
				if(z.isNotNull(hu) && z.isNotNull(hu.getUser_role_level())) {
					//判读当前组织负责人级别是否大于等于需要获取的用户级别
					int rolelevel = new Integer(hu.getUser_role_level());
					if(rolelevel>=role_level) {
						//返回当前负责人
						result = hu.getZid();
					}else {
						//级别不够，继续向上级查找 
						result = GetLeader(org.getParentid(),role_level);
					}
				}else {
					//调用上用组织继续查找 
					result = GetLeader(org.getParentid(),role_level);
				}
			}
		}
		return result;
	}


	/**
	 * 获取用户直接领导ID
	 * @param zid 用户ZID
	 * @param orgId 用户登录组织ZID
	 * @return
	 */
	private String getLeaderId(String userid,String orgId) {
		String result = "";
		if(z.isNotNull(userid) && z.isNotNull(orgId)) {
			z_org org = z.orgs.get(orgId);
			if(z.isNotNull(org)) {
				//获取部门负责人
				String orgHeadUserid = org.getUser_head();
				
				if(z.isNotNull(orgHeadUserid)) {
					//如果部门负责人和当前用户是一个人，向上获取上级部门负责人
					if(orgHeadUserid.equals(userid)) {
						//如果上级部门，不为空就获取上级组织部门领导
						if(z.isNotNull(org.getParentid())) {
							result = getLeaderId(userid,org.getParentid());
						}
					}else {
						result = orgHeadUserid;
					}
				}else {
					//获取上级部门领导
					if(z.isNotNull(org.getParentid())) {
						result = getLeaderId(userid,org.getParentid());
					}
				}
				
			}
		}
		return result;
	}

	/**
	 * 获取用户多级领导ID
	 * @param zid 用户ZID
	 * @param orgId 用户登录组织ZID
	 * @return
	 */
	private void getMultistageLeaderId(String userid,String orgId,String end_approval_role,int approvals_up_num,List<String> UserList) {
		if(z.isNotNull(userid) && z.isNotNull(orgId) && z.isNotNull(end_approval_role) && approvals_up_num>0 && z.isNotNull(UserList) ) {
			if(UserList.size()<approvals_up_num) {
				//获取本级组织负责人
				z_org org = z.orgs.get(orgId);
				if(z.isNotNull(org)) {
					//获取部门负责人
					String orgHeadUserid = org.getUser_head();
					if(z.isNotNull(orgHeadUserid)) {
						//判读，当前部门负责人是否到了最后审批角色，如果是，不添加当前用户到审批列表中。【避免董事长审批事假情况发生】
						int end_role_count = selectInt("SELECT count(*) from z_role_user  where userid = '"+orgHeadUserid+"' and pid = '"+end_approval_role+"'");
						if(end_role_count==0) {
							//如果部门负责人和当前用户不是一个人,并且，部门负责人也没有出现在审核列表中，就把当前负责人添加到结果List中，并向上查找上级组织负责人
							if(!orgHeadUserid.equals(userid) && !UserList.contains(orgHeadUserid)) {
								UserList.add(orgHeadUserid);
							}
							getMultistageLeaderId(userid,org.getParentid(),end_approval_role,approvals_up_num,UserList);
						}
					}else {
						getMultistageLeaderId(userid,org.getParentid(),end_approval_role,approvals_up_num,UserList);
					}

				}
			}
		}
	}


	/**
	 * 获取流程对象
	 * @param zid
	 * @return
	 * @throws Exception 
	 */
	private z_workjob getWorkJob(String zid) throws Exception {
		if(z.isNull(zid)) {
			z.Exception("生成流程图页面出错，zid is null");
		}
		HashMap<String,String> wjMap = selectMap("select * from z_workjob where zid = '"+zid+"'");
		z_workjob wj = BeanUtil.MapToBean(wjMap, z_workjob.class);
		if(z.isNull(wjMap) || wjMap.size()==0) {
			z.Exception("生成流程图页面出错，未找到工作流程 wjMap is null");
		}
		return wj;
	}

	/**
	 * 获取开始节点编辑
	 * @param workjobList
	 * @return
	 * @throws Exception 
	 */
	private static String getStartNodeId(List<HashMap<String,String>> workjobList) throws Exception {
		if(z.isNull(workjobList) || workjobList.size()==0) {
			z.Exception("生成流程图页面出错，未查询到流程节点信息。");
		}
		String StartNodeId = "";
		for (HashMap<String, String> WorkJobMap : workjobList) {
			if("0".equals(WorkJobMap.get("node_type"))) {
				StartNodeId = WorkJobMap.get("zid");
			}
		}
		if(z.isNull(StartNodeId)) {
			z.Exception("生成流程图页面出错，未找到开始节点，请查看流程模板设置是否正确。");
		}
		return StartNodeId;
	}

	/**
	 * 获取所有节点信息
	 * @param workjobList
	 * @return
	 * @throws Exception
	 */
	private Map<String,z_workjob_node> getAllNode(List<HashMap<String,String>> workjobList) throws Exception {
		if(z.isNull(workjobList) || workjobList.size()==0) {
			z.Exception("生成流程图页面出错，未查询到流程节点信息。");
		}
		Map<String,z_workjob_node> allNode = new HashMap<String,z_workjob_node>();//所有节点
		for (HashMap<String,String> wjnmap : workjobList) {
			z_workjob_node wjn = BeanUtil.MapToBean(wjnmap, z_workjob_node.class);

			//获取审批人
			List<HashMap<String,String>> approvaluserList = selectList("select * from z_workjob_approvaluser where pid = '"+wjn.getZid()+"' order by seq");
			List<z_workjob_approvaluser> z_workjob_approvaluser_list = new ArrayList<z_workjob_approvaluser>();
			for (HashMap<String, String> approvaluserMap : approvaluserList) {
				z_workjob_approvaluser au = BeanUtil.MapToBean(approvaluserMap, z_workjob_approvaluser.class);
				z_workjob_approvaluser_list.add(au);
			}
			wjn.setZ_workjob_approvaluser_list(z_workjob_approvaluser_list);


			allNode.put(wjn.getZid(), wjn);
		}
		return allNode;
	}

	private String getUserName(String user_id) {
		z_user user =  z.users.get(user_id);
		if(z.isNotNull(user)) {
			return user.getUser_name();
		}else {
			return user_id;
		}
	}

	/**
	 * 获取Bean中的内容
	 * @param bean 
	 * @param column_name
	 * @return
	 */
	protected String getColumnValue(HashMap<String,String> bean, String column_name,String editType) {
		String result = "";
		try {
			if(bean!=null) {
				result = String.valueOf(bean.get(column_name));
				if(z.isNull(result)) {
					//如果，当前是新增页面，给字段返回默认值，获取字段中的默认值
					if("insert".equals(editType)) {
						//获取当前字段
						z_form_table_column column = z.columns.get(bean.get("tableId")+"_"+column_name);
						if("0".equals(column.getColumn_default_type())) {//固定值
							result = column.getColumn_default();
						}else if("1".equals(column.getColumn_default_type())) {//系统时间
							result = DateUtil.getDateTime();
						}else if("2".equals(column.getColumn_default_type())) {//系统日期
							result = DateUtil.getDate();
						}else if("3".equals(column.getColumn_default_type())) {//当前用户
							result = bean.get("session_userid");
						}else if("4".equals(column.getColumn_default_type())) {//当前组织
							result = bean.get("session_orgid");
						}else if("5".equals(column.getColumn_default_type())) {//当前组织
							result = z.newNumber();
						}else if("6".equals(column.getColumn_default_type())) {//当前组织
							result = z.newZid(bean.get("tableId"));
						}else {
							result = "";
						}
					}else {
						result = "";
					}
				}
			}
		} catch (Exception e) {
			z.Error("获取Bean中的内容出错："+e.getMessage());
		}
		return result;
	}






	/**
	 * 根据流程ID获取当前流程节点
	 * @param wid
	 * @return
	 */
	public z_workjob_node getNowNode(String wid) {
		z_workjob_node result = selectBean("select zd.* from z_workjob_node zd left join z_workjob zc on zd.zid = zc.now_node where zc.zid = '"+wid+"'", z_workjob_node.class);
		return result;
	}


	/**
	 * 	判读节点是否已创建
	 * @param wf_now_node
	 * @return
	 */
	private boolean isCreateNode(z_workflow_node wf_now_node) {
		boolean result = false;
		if(z.isNotNull(wf_now_node)) {
			int count = selectInt("SELECT COUNT(*) FROM z_workjob_node WHERE zid = '"+wf_now_node.getZid()+"'");
			if(count>0) {
				result = true;
			}
		}
		return result;
	}


	/**
	 * 判读当前用户是否是审批人员
	 * @param wj 
	 * @param allNode
	 * @param request
	 * @return
	 */
	private boolean ifSessionUserIsApprovaluser(z_workjob wj, Map<String, z_workjob_node> allNode, String session_userid) {
		boolean result = false;
		List<z_workjob_approvaluser> auserList =  allNode.get(wj.getNow_node()).getZ_workjob_approvaluser_list();
		for (z_workjob_approvaluser au : auserList) {
			if(au.getUserzid().equals(session_userid) && !"2".equals(au.getIs_approval())) {
				result = true;
			}else {
				if("sa".equals(session_userid)) {
					result = true;
				}
			}
		}
		return result;
	}

}
