package com.futvan.z.system.zjob;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;

import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.system.zjob.ZjobService;
@Controller
public class ZjobAction extends SuperAction{
	@Autowired
	private ZjobService zjobService;
	/**
	 * 启动任务
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/start_job")
	public @ResponseBody Result start_job(@RequestParam HashMap<String,String> bean) throws Exception {
		return zjobService.start_job(bean);
	}
	
	/**
	 * 关闭任务
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/close_job")
	public @ResponseBody Result close_job(@RequestParam HashMap<String,String> bean) throws Exception {
		return zjobService.close_job(bean);
	}
	
	/**
	 * 立即执行任务
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/run_job")
	public @ResponseBody Result run_job(@RequestParam HashMap<String,String> bean) throws Exception {
		return zjobService.run_job(bean);
	}
}
