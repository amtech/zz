package com.futvan.z.system.zjob;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_job extends SuperBean{
	//任务
	private String jobname;

	//是否启动
	private String isstart;

	//执行时间
	private String jobtime;

	//任务执行文件
	private String jobclass;

	/**
	* get任务
	* @return jobname
	*/
	public String getJobname() {
		return jobname;
  	}

	/**
	* set任务
	* @return jobname
	*/
	public void setJobname(String jobname) {
		this.jobname = jobname;
 	}

	/**
	* get是否启动
	* @return isstart
	*/
	public String getIsstart() {
		return isstart;
  	}

	/**
	* set是否启动
	* @return isstart
	*/
	public void setIsstart(String isstart) {
		this.isstart = isstart;
 	}

	/**
	* get执行时间
	* @return jobtime
	*/
	public String getJobtime() {
		return jobtime;
  	}

	/**
	* set执行时间
	* @return jobtime
	*/
	public void setJobtime(String jobtime) {
		this.jobtime = jobtime;
 	}

	/**
	* get任务执行文件
	* @return jobclass
	*/
	public String getJobclass() {
		return jobclass;
  	}

	/**
	* set任务执行文件
	* @return jobclass
	*/
	public void setJobclass(String jobclass) {
		this.jobclass = jobclass;
 	}

}
