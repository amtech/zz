package com.futvan.z.system.zrole;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.system.zrole.ZroleService;
@Controller
public class ZroleAction extends SuperAction{
	@Autowired
	private ZroleService zroleService;
	
	/**
	 * 	打开导入用户页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/OpenImportUsersListForRole")
	public ModelAndView OpenImportUsersListForRole(String roleid) throws Exception {
		ModelAndView  model = new ModelAndView("system/zrole/ImportUsersListForRole");
		model.addObject("userList",zroleService.getUserList(roleid));
		model.addObject("roleid",roleid);
		return model;
	}
	
	/**
	 * 	导入用户
	 * @param userIds
	 * @param roleid
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/ImportUsersForRole")
	public @ResponseBody Result ImportUsersForRole(String userIds,String roleid) throws Exception {
		Result result = new Result();
		if(!"".equals(userIds) && userIds!=null){
			if(!"".equals(roleid) && roleid!=null) {
				zroleService.ImportUser(roleid,userIds);
				result.setCode(Code.SUCCESS);
			}else {
				result.setCode(Code.ERROR);
				result.setData("角色ID为空");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setData("用户ID为空");
		}
		
		return result;
	}
	
	
	/**
	 * 	打开导入菜单页面
	 * @param roleid
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/OpenImportMenuListForRole")
	public ModelAndView OpenImportMenuListForRole(String roleid) throws Exception {
		ModelAndView  model = new ModelAndView("system/zrole/ImportMenusListForRole");
		model.addObject("roleid",roleid);
		return model;
	}
	
	@RequestMapping(value="/getMenuListForRole")
	public @ResponseBody Result getMenuListForRole(String roleid) throws Exception {
		Result result = new Result();
		result.setCode(Code.SUCCESS);
		HashMap dateMap = new HashMap();
		dateMap.put("menuList", zroleService.getMenuList(roleid));
		dateMap.put("roleid",roleid);
		result.setData(dateMap);
		return result;
	}
	
	
	
	
	/**
	 * 	导入菜单
	 * @param userIds
	 * @param roleid
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/ImportMenuForRole")
	public @ResponseBody Result ImportMenuForRole(String menuIds,String roleid) throws Exception {
		Result result = new Result();
		if(!"".equals(menuIds) && menuIds!=null){
			if(!"".equals(roleid) && roleid!=null) {
				zroleService.ImportMenu(roleid,menuIds);
				result.setCode(Code.SUCCESS);
			}else {
				result.setCode(Code.ERROR);
				result.setData("角色ID为空");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setData("菜单ID为空");
		}
		
		return result;
	}
	
	
	
}
