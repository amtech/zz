package com.futvan.z.system.customer;
import java.util.HashMap;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.util.StringUtil;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
@Controller
public class Crm_customerReset_customer_loginpasswordButtonAction extends SuperAction{

	@Autowired
	private CommonService commonService;

	@RequestMapping(value="/reset_customer_loginpassword")
	public @ResponseBody Result reset_customer_loginpassword(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();
		try {
			//获取所有客户主键
			String zids = bean.get("zids");
			String[]zid_array = zids.split(",");
			//遍历所有客户主键
			for (int i = 0; i < zid_array.length; i++) {
				String zid = zid_array[i];
				String new_password = StringUtil.CreatePassword("123456");
				String sql = "update crm_customer set password_login = '"+new_password+"' where zid = '"+zid+"'";
				//重置客户密码
				sqlSession.update("update", sql);
			}
			result.setCode(Code.SUCCESS);
			result.setMsg("重置成功");
			
		} catch (Exception e) {
			result.setCode(Code.ERROR);
			result.setMsg("重置登录密码出错|"+e.getMessage());
		}
		return result;
	}
}
