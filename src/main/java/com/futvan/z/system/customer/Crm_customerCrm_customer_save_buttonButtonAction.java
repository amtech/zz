package com.futvan.z.system.customer;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.futvan.z.framework.core.SuperAction;
import java.util.HashMap;
@Controller
public class Crm_customerCrm_customer_save_buttonButtonAction extends SuperAction{
	@Autowired
	private CommonService commonService;
	
	@RequestMapping(value="/crm_customer_save_button")
	public @ResponseBody Result crm_customer_save_button(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();
		
		//获取客户推荐人ID
		String parentid = bean.get("parentid");
		
		//根据客户推荐人ID获取推荐人的网体节点序号
		int new_node = sqlSession.selectOne("selectoneint", "SELECT node + 1 new_node FROM crm_customer WHERE zid = '"+parentid+"'");
		
		//设置新客户的网体节点序号
		bean.put("node", String.valueOf(new_node));
		
		//执行保存过程
		String editType = bean.get("editType");
		if("insert".equals(editType)) {
			result = commonService.insert(bean, request);
		}else if("update".equals(editType)) {
			result = commonService.update(bean, request);
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("editType is null");
		}
		return result;
	}
}
