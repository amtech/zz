package com.futvan.z.system.zlog;
import com.futvan.z.framework.core.SuperBean;
public class z_log extends SuperBean{
	//日志编号
	private String logid;

	//日志类型
	private String logtype;

	//标题
	private String title;

	//日志信息
	private String loginfo;

	//服务端标识
	private String systemid;

	//组织
	private String userorgid;

	//用户
	private String userid;

	//SESSIONID
	private String sessionid;

	//客户端IP
	private String customIP;

	//服务器IP
	private String serverIP;

	//Action
	private String packagepath;

	//方法
	private String functionname;

	//参数
	private String parameter;

	//耗时
	private String runtime;

	/**
	* get日志编号
	* @return logid
	*/
	public String getLogid() {
		return logid;
  	}

	/**
	* set日志编号
	* @return logid
	*/
	public void setLogid(String logid) {
		this.logid = logid;
 		}

	/**
	* get日志类型
	* @return logtype
	*/
	public String getLogtype() {
		return logtype;
  	}

	/**
	* set日志类型
	* @return logtype
	*/
	public void setLogtype(String logtype) {
		this.logtype = logtype;
 		}

	/**
	* get标题
	* @return title
	*/
	public String getTitle() {
		return title;
  	}

	/**
	* set标题
	* @return title
	*/
	public void setTitle(String title) {
		this.title = title;
 		}

	/**
	* get日志信息
	* @return loginfo
	*/
	public String getLoginfo() {
		return loginfo;
  	}

	/**
	* set日志信息
	* @return loginfo
	*/
	public void setLoginfo(String loginfo) {
		this.loginfo = loginfo;
 		}

	/**
	* get服务端标识
	* @return systemid
	*/
	public String getSystemid() {
		return systemid;
  	}

	/**
	* set服务端标识
	* @return systemid
	*/
	public void setSystemid(String systemid) {
		this.systemid = systemid;
 		}

	/**
	* get组织
	* @return userorgid
	*/
	public String getUserorgid() {
		return userorgid;
  	}

	/**
	* set组织
	* @return userorgid
	*/
	public void setUserorgid(String userorgid) {
		this.userorgid = userorgid;
 		}

	/**
	* get用户
	* @return userid
	*/
	public String getUserid() {
		return userid;
  	}

	/**
	* set用户
	* @return userid
	*/
	public void setUserid(String userid) {
		this.userid = userid;
 		}

	/**
	* getSESSIONID
	* @return sessionid
	*/
	public String getSessionid() {
		return sessionid;
  	}

	/**
	* setSESSIONID
	* @return sessionid
	*/
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
 		}

	/**
	* get客户端IP
	* @return customIP
	*/
	public String getCustomIP() {
		return customIP;
  	}

	/**
	* set客户端IP
	* @return customIP
	*/
	public void setCustomIP(String customIP) {
		this.customIP = customIP;
 		}

	/**
	* get服务器IP
	* @return serverIP
	*/
	public String getServerIP() {
		return serverIP;
  	}

	/**
	* set服务器IP
	* @return serverIP
	*/
	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
 		}

	/**
	* getAction
	* @return packagepath
	*/
	public String getPackagepath() {
		return packagepath;
  	}

	/**
	* setAction
	* @return packagepath
	*/
	public void setPackagepath(String packagepath) {
		this.packagepath = packagepath;
 		}

	/**
	* get方法
	* @return functionname
	*/
	public String getFunctionname() {
		return functionname;
  	}

	/**
	* set方法
	* @return functionname
	*/
	public void setFunctionname(String functionname) {
		this.functionname = functionname;
 		}

	/**
	* get参数
	* @return parameter
	*/
	public String getParameter() {
		return parameter;
  	}

	/**
	* set参数
	* @return parameter
	*/
	public void setParameter(String parameter) {
		this.parameter = parameter;
 		}

	/**
	* get耗时
	* @return runtime
	*/
	public String getRuntime() {
		return runtime;
  	}

	/**
	* set耗时
	* @return runtime
	*/
	public void setRuntime(String runtime) {
		this.runtime = runtime;
 		}

}
