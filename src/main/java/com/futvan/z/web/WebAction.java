package com.futvan.z.web;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.CommonServicesUtil;
import com.futvan.z.system.zproduct.z_product;
@Controller
public class WebAction extends SuperAction {
	@Autowired
	private WebService webService;
	@Autowired
	private CommonService commonService;
	
	@RequestMapping(value="/index")
	public ModelAndView index(String cityId) throws Exception {
		ModelAndView  model = new ModelAndView("web/index");
		return model;
	}
	
	
	/**
	 * 	查看产品列表
	 * @param zid
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */
	@RequestMapping(value="/product_view_list")
	public ModelAndView product_view_list(@RequestParam HashMap<String,String> bean) throws Exception {
		return commonService.select(bean,"web/product_list");
	}
	
	/**
	 * 	查看产品
	 * @param zid
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/product_view")
	public ModelAndView product_view(String zid) throws Exception {
		ModelAndView  model = new ModelAndView("web/product");
		if(z.isNotNull(zid)) {
			z_product zp = sqlSession.selectOne("z_product_select_zid", zid);
			model.addObject("zp", zp);
		}
		return model;
	}
	
}
