package com.futvan.z.erp.erp_account;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.erp.erp_account.Erp_accountService;
@Controller
public class Erp_accountAction extends SuperAction{
	@Autowired
	private Erp_accountService erp_accountService;
	
	@RequestMapping(value="/erp_account_list")
	public ModelAndView erp_account_list(@RequestParam HashMap<String,String> bean) throws Exception {
		//从Session中获取用户与组织信息保存到Bean中
		GetSessionInfoToBean(bean,request);
		return erp_accountService.select(bean,"common/form/list");

	}
}
