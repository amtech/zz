<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>${sp.system_title} ${zuser.user_name}</title>
<%@include file="/views/common/common.jsp"%>
<%@include file="/views/common/main/main_head.jsp"%>
</head>
<body class="easyui-layout" id="zmain_easyui_layout">
	<!-- 菜单区域 -->
	<div id="menu_title_div" data-options="region:'west',title:'我 的 主 页 ',hideCollapsedContent:false,border:true,split:true" style="width: 306px;">
		<div class="easyui-layout" data-options="fit:true"> 
				<div data-options="region:'north',border:true" style="height:100px;"> 
					<!-- 用户信息 -->
					<div class="row" style="padding: 5px;margin:0px;"> 
		  				<div class="col-md-5" style="padding: 0px;margin:0px;width: 80px;height: 80px">
		  					<c:if test='${not empty zuser.photo}'>
		  						<img class="img-thumbnail" width="80px" height="80px" src="${zuser.photo}" />
							</c:if>
							<c:if test="${empty zuser.photo}" >
		  						<img class="img-thumbnail" width="80px" height="80px" src="./img/system/touxiang.jpg" /> 
							</c:if>
						</div>
		  				<div class="col-md-7" >
		  					<div class="row" >
		  						<h5>${zuser.user_name}</h5>
		  					</div>
		  					<div class="row">
		  						<h6>${zorg.org_name}</h6>
		  					</div>
						</div>
						
		  			</div>
				</div>
				
				<div data-options="region:'center',border:true">
					<!-- 菜单目录 -->
					<div class="row" style="padding: 10px;margin:0px;overflow-x:hidden;">
  						<div id="SideMenuTree" class="easyui-sidemenu" data-options="data:menuTreeList,border:false,onSelect:MenuTreeSelect" style="width: 100%" ></div>
  					</div>
				</div>
		</div>
	</div>
	<!-- 主页面区域 -->
	<div data-options="region:'center',border:false">
		<div id="myTab" class="easyui-tabs"  data-options="tools:'#myTab-tools'"  style="height: 100%">
			<div title="首页"  iconCls="fa fa-home">
				<iframe id="index_iframe" name="MainIndex" scrolling="auto" frameborder="0"  src="https://blog.csdn.net/qq_38056435/article/details/70212001" style="width:100%;height:100%;"></iframe>
				<!-- <iframe id="index_iframe" name="MainIndex" scrolling="auto" frameborder="0"  src="main_index" style="width:100%;height:100%;"></iframe> -->
			</div>
		</div>
	</div>
	<%@include file="/views/common/main/main_body.jsp"%>	
	<%@include file="/views/common/body.jsp"%>
</body>
</html>