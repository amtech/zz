<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>${sp.system_title}</title>
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">
$(document).ready(function(){ 
	$("#userid").focus();
}); 
function LoginSystem(){
	var userid = $("#userid").val();
	var passwordid = $("#passwordid").val();
	if(!isNull(userid)){
		if(!isNull(passwordid)){
				//判读账号对应分配组织是否这多个
				$.ajax({
					type : "POST",
					url : "getUserOrgForUserId",
					data:{userId:userid},
					success : function(data) {
						if(data.code=="SUCCESS"){
							var orgCount = data.data.length;
							if(orgCount==1){
								$("#orgid_id").val(data.data[0].zid);
								$("#LoginForm").submit();
							}else if(orgCount>1){
								$('#MainDiv').empty();
								//设置多组织选择
								var orgListHtml = '<div class="col-md-12"><ul>';
								orgListHtml = orgListHtml+'<form id="LoginForm" method="post" action="UserLogin">';
								orgListHtml = orgListHtml+'<input type="hidden" id="orgid_id" name="orgid"/>';
								orgListHtml = orgListHtml+'<input type="hidden" name="user_id" value=\''+userid+'\'/>';
								orgListHtml = orgListHtml+'<input type="hidden" name="password" value=\''+passwordid+'\'/>'; 
								orgListHtml = orgListHtml+'</form>';
								for(var i=0;i<orgCount;i++){
									orgListHtml = orgListHtml+'<li style="background:url(\'img/system/login_jiantou0.png\') no-repeat left 7px;" onmouseout="li_onmouseout(this)" onmouseover="li_onmouseover(this)"><a href="javascript:void(0);" class="text3" onclick="SetOrgid(\''+data.data[i].zid+'\')" >'+data.data[i].full_org_name+'</a></li>';
								}
								orgListHtml = orgListHtml+'</ul></div>';
								$('#MainDiv').append(orgListHtml);
								
								//设置返回登录页面按钮
								 $('#ButtonDiv').empty();
								var ReturnLoginPageHtml = '<div class="col-md-12"><a href="login" class="text3" style="padding-left: 21px">返回登录</a></div>';
								$('#ButtonDiv').append(ReturnLoginPageHtml);
								
							}else if(orgCount==0){
								if(userid=='sa'){
									$("#LoginForm").submit();
								}else{
									$("#UserLoginAlert").html(data.msg);
								}
								
							}
						}
					}
				});
		}else{
			$("#UserLoginAlert").html("密码不能为空");
			document.getElementById('passwordid').focus(); 
		}
	}else{
		$("#UserLoginAlert").html("账号不能为空");
		document.getElementById('userid').focus(); 
	}
	
	
}

function SetOrgid(orgid){
	$("#orgid_id").val(orgid);
	$("#LoginForm").submit();
}

function Login_onkeydown(){
	if(event.keyCode==13) {
		var userid = $("#userid").val();
		var passwordid = $("#passwordid").val();
		if (isNull(userid)){ 
			$("#userid").focus();
		    return; 
		}else if (isNull(passwordid)){ 
			$("#passwordid").focus();
		    return; 
		}else{
			LoginSystem(); 
		}
	}
	
}

/**
 * 多组织选择.li鼠标移入
 * @param obj
 */
function li_onmouseover(obj){
    obj.style.background = "url('img/system/login_jiantou1.png') no-repeat left 7px";
}

/**
 * 多组织选择.li鼠标移出
 * @param obj
 */
function li_onmouseout(obj){
    obj.style.background = "url('img/system/login_jiantou0.png') no-repeat left 7px";
}
</script>
<style type="text/css">
        body{
            margin: 0;
            padding: 0
        }/*清除默认样式*/

        .LoginMainImg{
            position:relative;
            top: 15%;
            left: 15%;
        }
        .main{
            position:fixed; /*生成绝对定位的元素，相对于浏览器窗口进行定位。*/
            width: 100%;
            height: 100%;
        }

        .left{
            width: 50%;
            height: 95%;
            float: left;

        }
        .right{
            width: 50%;
            height: 95%;
            float: right;

        }
        .foot{
        	text-align: center;
        	color: #213477;
        	font-size: 16px;
        }
        ul{
            padding-left: 11px;
        }
        li{
            list-style:none;
            padding-left:30px;
            background:url('img/system/login_jiantou2.png') no-repeat left 7px;
            margin-bottom: 20px;
            margin-left: 0px;
        }
        
        .text1{
            color:#213477;
            font-size:30px;
            font-family: 微软雅黑;
            font-weight:900;
        }

        .text2{
            color:#213477;
            font-size:28px;
            font-family: 微软雅黑;
            font-weight:500;
        }

        .text3{
            color: #BAB9B8;
            font-size:20px;
            font-family: 微软雅黑;
        }

        a:hover {
            color: #213477;
            text-decoration: none;
        }

    </style>
</head>
<body>
<div class="main">
    <div class="left">
    	<%-- <button type="button" class="btn btn-light" onclick="window.location.href='index'">${sp.system_title}首页</button>  --%>
        <img src="img/system/login_0.png"  class="LoginMainImg"/>
    </div>
    <div class="right">
        <div class="row" style="padding-top: 20%;padding-left: 5%;">
            <div class="col-md-12"><samp class="text1">您好，尊敬的用户！</samp></div>
            <div class="col-md-12"><samp class="text2">欢迎登录 | ${sp.system_title}</samp></div>
        </div>
        <div id="MainDiv" class="row" style="padding-top: 80px;padding-left: 4%;">
	        <div class="col-md-4">
	        	<form action="UserLogin" method="post" id="LoginForm">
	        		<input type="hidden" id="orgid_id" name="orgid"/>
	        		<div class="col-md-12"><samp style="color:#6d6c67;font-size:20px;font-family: 微软雅黑;">账 号</samp></div>
	            	<div class="col-md-12" style="padding-top: 10px;"><input type="text" id="userid" name="user_id" value="${user_id}" onkeydown="Login_onkeydown();" maxlength="16" autofocus="autofocus" style="width: 200px;border:0px;font-size: 20px;outline: none;"/></div>
	        		<div class="col-md-12" style="padding-top: 30px;"><samp style="color:#6d6c67;font-size:20px;font-family: 微软雅黑;">密 码</samp></div>
	                <div class="col-md-12" style="padding-top: 10px;"><input type="password" id="passwordid" name="password" placeholder="********" onkeydown="Login_onkeydown();" style="width: 200px;border:0px;font-size: 20px;outline: none;"/></div>
	        	</form> 
	        </div>
	        <div class="col-md-8"> 
	        	<div class="row" style="padding-top:115px;padding-left: 0%; ">
	        		<button onclick="LoginSystem()" style="border:none ;font-size: 20px;border-radius: 12px;background-color: #213477;color: white;width: 250px;height: 60px;">登录</button>
	        	</div>
            </div>
        </div>
        <div id="ButtonDiv" class="row" style="padding-top: 20px;padding-left: 6%; ">
        	<div class="col-md-12">
				<label id="UserLoginAlert" style="color: red;">${UserLoginAlert}</label>
			</div>
        </div>
    </div>
    
    <div class="foot">版本号：${sp.zversion}</div>
    
</div>
<%@include file="/views/common/body.jsp"%>
</body>
</html>