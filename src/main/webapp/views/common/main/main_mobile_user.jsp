<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>${sp.system_title}</title>
<%@include file="/views/common/common.jsp"%>
<style type="text/css">
</style>

<script type="text/javascript">
</script>
</head>
<body>
	<div class="mpage">
		<div class="mhead">
			<div class="mhead_title row justify-content-start align-items-center">
				<div class="col-4"></div>
				<div class="col-4">个人中心</div>
			</div>
		</div>
		<div class="mbody">

		</div>
		<div class="mfoot">
			<div class="m_bottomrow">
				<div class="m_bottomrowColumn" onclick="OpenMain()">
					<div><img src="img/zlogin.png"></div>
					<div>首页</div>
				</div>
				<div class="m_bottomrowColumn" onclick="OpenFunction()">
					<div><img src="img/zlogin.png"></div>
					<div>业务功能</div>
				</div>
				<div class="m_bottomrowColumn" onclick="OpenUser()">
					<div><img src="img/zlogin.png"></div>
					<div class="RedText">个人中心</div>
				</div>
			</div>

		</div>
	</div>
</body>
</html>