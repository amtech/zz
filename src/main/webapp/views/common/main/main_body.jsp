<%@ page pageEncoding="utf-8"%>
<div id="myTab-tools">
	<!-- 超级管理员功能 -->
	<c:if test='${zuser.zid==sp.super_user}'>
		<button type="button" class="easyui-linkbutton" data-options="plain:true,iconCls:'fa fa-gears'" onclick="SetSystemParameter();">设置系统参数</button>
		<button type="button" class="easyui-linkbutton" data-options="plain:true,iconCls:'fa fa-refresh'" onclick="LoadParameter();">更新系统缓存</button>
	</c:if>
	<button type="button" class="easyui-linkbutton" data-options="plain:true,iconCls:'fa fa-tags'" onclick="my_workjob_list();">待办任务</button>
	<button type="button" class="easyui-linkbutton" data-options="plain:true,iconCls:'fa fa-sliders'" onclick="user_settings();">设置</button>
	<button type="button" class="easyui-linkbutton" data-options="plain:true,iconCls:'fa fa-power-off'" onclick="UserExit();">退出</button>
</div>
<div id="UserMessagesList" data-options="region:'east',hideCollapsedContent:false,collapsible:true,collapsed:true,split:true" title="系统用户：${user_messages_list.size()}人" style="width:200px;">
	<c:forEach items="${user_messages_list}" var="users_message">
		<c:if test='${users_message.zid!= zuser.zid}'>
			<c:if test='${users_message.isonline==0}'>
				<button id="message_user_${users_message.zid}" type="button" class="btn btn-success btn-block" onclick="OpenMessageWindows('${users_message.zid}','${users_message.user_name}')">${users_message.user_name} <c:if test='${users_message.messagecount>0}'>[${users_message.messagecount}]</c:if></button>
			</c:if>
			<c:if test='${users_message.isonline==1}'>
				<button id="message_user_${users_message.zid}" type="button" class="btn btn-light btn-block" onclick="OpenMessageWindows('${users_message.zid}','${users_message.user_name}')">${users_message.user_name} <c:if test='${users_message.messagecount>0}'>[${users_message.messagecount}]</c:if></button>
			</c:if>
		</c:if> 
	</c:forEach>
</div>
	
<!-- 图片展示框 -->
<div id="img_windows" class="easyui-window" title="图片" style="width:700px;height:500px;text-align:center" data-options="iconCls:'fa fa-picture-o',closed:true,modal:true,shadow:false,minimizable:false,collapsible:false">
</div>
	
<!-- 删除信息提示框 -->
<div id="error_windows" class="easyui-window" title="错误信息" style="width:700px;height:500px" data-options="iconCls:'fa fa-exclamation-triangle',closed:true,modal:true,shadow:false,minimizable:false,collapsible:false">
</div>
	
<!-- 信息提示2 -->
<div id="alert_windows" class="easyui-window" title="提示信息" style="width:700px;height:500px" data-options="iconCls:'fa fa-bullhorn',closed:true,modal:true,shadow:false,minimizable:false,collapsible:false">
</div>
	
<!-- 聊天窗口 -->
<div id="message_windows" class="easyui-window" title="系统管理员" style="width:800px;height:500px" data-options="closed:true,shadow:false,minimizable:false,cache:false">
	<div id="MessageTab" class="easyui-tabs"  border="false" fit="true" tabPosition="left"></div>
</div>
	
<!-- 加载等待提示框 -->
<div id="loading_windows" class="easyui-window" title=" "   data-options="resizable:false,draggable:false,closable:false,closed:true,modal:true,shadow:false,maximizable:false,minimizable:false,collapsible:false,border:false">
	<img alt="" src="<%=request.getContextPath()%>/img/system/loading.gif" width="70px" height="10px"/>
</div>
	
<!-- myTab右键菜单 -->
<div id="my_tab_menu" class="easyui-menu" style="width:120px;">
	<div data-options="name:'tab_refresh',iconCls:'fa fa-refresh'">刷新</div>
	<div class="menu-sep"></div>
	<div data-options="name:'tab_close_other_all',iconCls:'fa fa-window-close-o'">关闭其它功能</div>
    <div data-options="name:'tab_close_all',iconCls:'fa fa-window-close'">关闭全部功能</div>
</div>
