<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>${sp.system_title}</title>
<%@include file="/views/common/common.jsp"%>
<style type="text/css">
.m_function_box {
	font-size: 14px;
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	justify-content: space-between;
	align-items: center;
	padding-top: 10px;
	padding-bottom: 10px;
}
.m_function_box img {
	width: 40px;
	height: 40px;
}
.m_function {
	width: 30%;
	display: flex;
	flex-direction: column;
	align-items: center;
	margin-bottom: 20px;
}
</style>
<script type="text/javascript">
function OpenList(name,url){
	window.location.href=url;
}
</script>
</head>
<body>
	<div class="mpage">
		<div class="mhead">
			<div class="mhead_title row justify-content-start align-items-center">
				<div class="col-4"></div>
				<div class="col-4">业务功能</div>
			</div>
		</div>
		<div class="mbody">
			<!-- 输出功能按钮 -->
			<div class="m_function_box">
				<c:forEach items="${menuList}" var="menu">
					<div class="m_function" onclick="OpenList('${menu.name}','${menu.murl}')">
						<div><img src="img/zlogin.png"></div>
						<div>${menu.name}</div>
					</div>
				</c:forEach>
			</div>

		</div>
		<div class="mfoot">
			<div class="m_bottomrow">
				<div class="m_bottomrowColumn" onclick="OpenMain()">
					<div><img src="img/zlogin.png"></div>
					<div>首页</div>
				</div>
				<div class="m_bottomrowColumn" onclick="OpenFunction()">
					<div><img src="img/zlogin.png"></div>
					<div class="RedText">业务功能</div>
				</div>
				<div class="m_bottomrowColumn" onclick="OpenUser()">
					<div><img src="img/zlogin.png"></div>
					<div>个人中心</div>
				</div>
			</div>

		</div>
	</div>
</body>
</html>