<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Z平台</title>
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">

$(function () {
    $("#MainTable").datagrid({
        //双击事件
        onDblClickRow: function (index, row) {
        	getReturnValue();
        }
    });
})

/**
 * Z5List行双击事件
 * @param index
 * @param row
 * @returns
 */
function onDblClickRowZ5List(index, row){
	getReturnValue();
}

/**
 * 获取返回值
 */
function getReturnValue(){
	
	//获取返回字段Id
	var ColumnId = '${querybean.ColumnId}';
	var z5_key_ColumnId = '${querybean.z5_key}';
	var z5_value_ColumnId = '${querybean.z5_value}';
	var ReturnKeyColumnId = '${querybean.ReturnKeyColumnId}';
	var ReturnValueColumnId = '${querybean.ReturnValueColumnId}';
	
	var row = $("#MainTable").datagrid("getSelected");
	if(!isNull(row)){
		window.opener.Z5listReturn(ColumnId,row[z5_key_ColumnId],row[z5_value_ColumnId],ReturnKeyColumnId,ReturnValueColumnId);
		window.close();
	}else{
		alert('请选择记录');
	}
}


</script>
</head>
<body class="easyui-layout">
<z:Z5ListPage list="${list}" parameter="${querybean}"/>
<%@include file="/views/common/body.jsp"%>
</body>
</html>