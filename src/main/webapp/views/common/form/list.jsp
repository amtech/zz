<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Z平台</title>
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">
</script>
</head>
<body class="easyui-layout">
<z:ListPage list="${list}" parameter="${bean}" userid="${zuser.zid}"/>
<%@include file="/views/common/body.jsp"%>
</body>
</html>