<%@ page pageEncoding="utf-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- 盗链图片 -->
<meta name="referrer" content="no-referrer" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- 系统ICO图标 -->
<link href="<%=request.getContextPath()%>/img/zico.ico" rel="shortcut icon" type="image/x-icon"  media="screen" />

<!-- 引用小图标 -->
<link  href="<%=request.getContextPath()%>/css/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

<!-- 添加jQuery -->
<script src="<%=request.getContextPath()%>/js/jquery-3.3.1.min.js"></script>

<!-- 移动端上拉刷新触底加载控件 -->
<%-- <link  href="<%=request.getContextPath()%>/js/mescroll/mescroll.min.css" rel="stylesheet" type="text/css"/>
<script src="<%=request.getContextPath()%>/js/mescroll/mescroll.min.js"></script> --%>

<!-- 添加BootStrap -->
<link  href="<%=request.getContextPath()%>/css/bootstrap-4.0.0/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<script src="<%=request.getContextPath()%>/css/bootstrap-4.0.0/js/bootstrap.min.js"></script>

<!-- EasyUI -->
<script src="<%=request.getContextPath()%>/js/jquery-easyui-1.8.8/jquery.easyui.min.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-easyui-1.8.8/locale/easyui-lang-zh_CN.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-easyui-1.8.8/plugins/jquery.datagrid.js"></script>
<link href="<%=request.getContextPath()%>/js/jquery-easyui-1.8.8/themes/default/easyui.css" rel="stylesheet" type="text/css"/>

<!-- 日期时间选择控件 -->
<script src="<%=request.getContextPath()%>/js/My97DatePicker/WdatePicker.js" type="text/javascript" ></script>

<!-- 文件上传控件 -->
<link href="<%=request.getContextPath()%>/js/uploadifive/uploadifive.css" type="text/css" rel="stylesheet">
<script src="<%=request.getContextPath()%>/js/uploadifive/jquery.uploadifive.min.js" type="text/javascript"></script>

<!-- 树型控件 -->
<%-- <link href="<%=request.getContextPath()%>/js/zTree_v3/css/metroStyle/metroStyle.css?version=${sp.zversion}" type="text/css" rel="stylesheet">
<script src="<%=request.getContextPath()%>/js/zTree_v3/js/jquery.ztree.core-3.5.js?version=${sp.zversion}" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/zTree_v3/js/jquery.ztree.excheck-3.5.js?version=${sp.zversion}" type="text/javascript" ></script>
<script src="<%=request.getContextPath()%>/js/zTree_v3/js/jquery.ztree.exedit-3.5.js?version=${sp.zversion}" type="text/javascript"></script> --%>

<!-- 加载UMEditor -->
<script type="text/javascript" charset="utf-8" src="<%=request.getContextPath()%>/js/ueditor1_4_3-utf8-jsp/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="<%=request.getContextPath()%>/js/ueditor1_4_3-utf8-jsp/ueditor.all.min.js"></script>
<script type="text/javascript" charset="utf-8" src="<%=request.getContextPath()%>/js/ueditor1_4_3-utf8-jsp/lang/zh-cn/zh-cn.js"></script>

<!-- 加载CodeMirror -->
<link href="<%=request.getContextPath()%>/js/codemirror-5.43.0/lib/codemirror.css" rel="stylesheet" type="text/css"/>
<script src="<%=request.getContextPath()%>/js/codemirror-5.43.0/lib/codemirror.js" type="text/javascript" charset="utf-8"></script>
<script src="<%=request.getContextPath()%>/js/codemirror-5.43.0/mode/javascript/javascript.js"></script>
<script src="<%=request.getContextPath()%>/js/codemirror-5.43.0/mode/sql/sql.js"></script>
<script src="<%=request.getContextPath()%>/js/codemirror-5.43.0/mode/clike/clike.js"></script><!-- 支持java -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/js/codemirror-5.43.0/theme/eclipse.css">

<!-- Form反序列化 -->
<script src="<%=request.getContextPath()%>/js/jquery.deserialize.js"></script>

<!-- cookie操作工具 -->
<script src="<%=request.getContextPath()%>/js/jquery.cookie.js"></script>

<!-- 添加平台公用js css -->
<script src="<%=request.getContextPath()%>/js/z.js?version=${sp.zversion}" charset="UTF-8"></script>
<link  href="<%=request.getContextPath()%>/css/z.css?version=${sp.zversion}" rel="stylesheet" type="text/css"/>
