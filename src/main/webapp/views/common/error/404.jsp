<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>ERROR_404</title>
		<!-- 添加BootStrap -->
<link  href="<%=request.getContextPath()%>/css/bootstrap-4.0.0/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<script src="<%=request.getContextPath()%>/css/bootstrap-4.0.0/js/bootstrap.min.js"></script>
	</head>
	<body>
		<button type="button" class="btn btn-dark" onclick="javascript:history.back(-1);"><i class="fa fa-hand-o-left"></i> 返回</button>
		<h3>404错误</h3>
		<h6>您访问的页面不存在</h6>
	</body>
</html>
