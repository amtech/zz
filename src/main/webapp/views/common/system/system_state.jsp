<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>系统监听</title>
<%@include file="/views/common/common.jsp"%>
</head>
<body>
<div class="container-fluid">
<div class="row">
<div class="col-md-6">
<div id="syldiv" style="width: 600px;height:400px;"></div>
</div>
<div class="col-md-6">
</div>
</div>
</div>
<c:forEach items="${system_state}" var="item" >
<span>${item.key} : ${item.value}</span><br>
</c:forEach>
<%@include file="/views/common/body.jsp"%>

<script type="text/javascript">

//初始化内存使用率图表
var myChart_syl = echarts.init(document.getElementById('syldiv'));
var option_syl = {
	title: {
	    text: '内存使用率'
	},
    tooltip : {
        formatter: "{a} <br/>{b} : {c}%"
    },
    toolbox: {
        feature: {
        }
    },
    series: [
        {
            name: '性能指标',
            type: 'gauge',
            detail: {formatter:"{value}%"},
            data: [{value: 50, name: '内存使用率'}]
        }
    ]
};

//每10秒刷新一次
window.setInterval(RefreshData, 10000);

//开始执行刷新程序
RefreshData();


/**
 * 	刷新数据
 */
function RefreshData(){
	$.ajax({
		type : "POST",
		url : "system_state_json",
		success : function(data) {
			if(data.code=="SUCCESS"){
				
				//获取数据
				option_syl.series[0].data[0].value = data.data.syl;
				myChart_syl.setOption(option_syl, true);
			}
		}
	});
	
}
</script>
</body>
</html>