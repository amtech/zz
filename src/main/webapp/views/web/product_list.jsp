<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>产品列表</title>
<%@include file="/views/common/common.jsp"%>

<style type="text/css">
</style>


</head>
<body>

	<div data-role="page">
		<div data-role="header" data-position="fixed">
			<h1 id="titleid">标题</h1>
		</div>
		<div id="content" role="main" class="ui-content">

			<c:forEach items="${list}" var="zp">
				<p>编号：${zp.number}</p>
				<p>名称：${zp.name}</p>
				<p>所属品类：${zp.type}</p>
				<z:column tableId="z_product" column_type="8" value="${zp.type}"
					is_readonly="1" column_name="所属品类" column_id="z_product_type"
					z5_table="z_product_type" z5_key="zid" z5_value="name" />
				<p>模板类型：${zp.template_type}</p>
				<p>花色类型：${zp.variety_type}</p>
				<p>厂家编号：${zp.manufacturer_number}</p>
				<p>产品描述：${zp.info}</p>
				<p>详细描述：${zp.info_html}</p>
				<p>
					<img width="200px" height="200px" src="${zp.qrcode}">
				</p>
			</c:forEach>



		</div>
		<div data-role="footer" data-position="fixed">
			<div data-role="navbar">
				<ul>
					<li><a href="#" id="navbar_index" data-icon="custom"
						class="ui-btn-active">首页</a></li>
					<li><a href="#" data-icon="star">商城</a></li>
					<li><a href="#" data-icon="gear">个人中心</a></li>
				</ul>
			</div>
		</div>


	</div>


<script>
$(function(){
    // dropload
    var dropload = $('#content').dropload({
    	scrollArea : window,
        domUp : {
            domClass   : 'dropload-up',
            domRefresh : '<div class="dropload-refresh">↓下拉刷新</div>',
            domUpdate  : '<div class="dropload-update">↑释放更新</div>',
            domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>'
        },
        domDown : {
            domClass   : 'dropload-down',
            domRefresh : '<div class="dropload-refresh">↑上拉加载更多</div>',
            domLoad    : '<div class="dropload-load"><span class="loading"></span>加载中...</div>',
            domNoData  : '<div class="dropload-noData">暂无数据</div>'
        },
        loadUpFn : function(me){
        	
        	// 为了测试，延迟1秒加载
            setTimeout(function(){
            	window.location.href='product_view_list?tableId=z_product';
            },2000);
        	
        	
        	
        },
        loadDownFn : function(me){
        	
        	// 为了测试，延迟1秒加载
            setTimeout(function(){
            	//alert('加载');
            	var num = $('#titleid').val()+1;
            	$('#titleid').val(num);
            	$('#titleid').text(num);
            	dropload.resetload();
            },2000);
        	
        	
        	
        }
    });
});
</script>
</body>
</html>