<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Z平台-免费JAVA集成开发平台</title>
<meta name="Keywords" Content="免费,开发平台,快速开发管理系统,集成开发环境,零编码,动态配置方式"/>
<meta name="Description" Content="通过Z平台集成开发环境，以零编码、动态配置的方式能够快速开发BS管理系统。"/>
<%@include file="/views/common/common.jsp"%>
<style type="text/css">
</style>
<script type="text/javascript">
</script>
</head>
<body>
	<div class="container-fluid" >
		<!-- Login层 -->
		<div class="row" style="padding: 20px;">
			<div class="col-md-4" >
				<img alt="Login" src="img/web/zlogin_1.png" style="height: 40px">
			</div>
			<div class="col-md-4" >
				<button type="button" class="btn btn-light" onclick="window.location.href='login'">登录管理后台</button>
			</div>
			<div class="col-md-4" >
				${custom_ip}
				${city}
				${lang}
			</div>
		</div>
		<!-- 中层 -->
		<div class="row" style="position:fixed;top: 15%;left: 30%;">
			<img alt="Login" src="img/web/index_01.png" >    
		</div>
		<div class="row" style="position:fixed;top: 15%;left: 30%;">
			<img alt="Login" src="img/web/index_02.png" >    
		</div>
		
	</div>
	
</body>
</html>