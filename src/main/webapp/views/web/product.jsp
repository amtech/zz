<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>产品介绍</title>
<%@include file="/views/common/common.jsp"%>
<style type="text/css">
</style>
<script type="text/javascript">
</script>
</head>
<body>

<p>编号：${zp.number}</p>
<p>名称：${zp.name}</p>
<p>所属品类：${zp.type}</p>
<%-- <z:column tableId="z_product" column_type="8" value="${zp.type}" is_readonly="1" column_name="所属品类" 	column_id="z_product_type"  z5_table="z_product_type" z5_key="zid" z5_value="name" /> --%>
<p>模板类型：${zp.template_type}</p>
<p>花色类型：${zp.variety_type}</p>
<p>厂家编号：${zp.manufacturer_number}</p>
<p>产品描述：${zp.info}</p>
<p>详细描述：${zp.info_html}</p>
<p><img width="200px"  height="200px" src="${zp.qrcode}"> </p>
<a href="/product_view_list?tableId=z_product" >返回列表</a>
</body>
</html>