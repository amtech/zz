<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>任务处理</title>
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">

/**
 * 同意
 */
function agree(){
	$("#newApprovalUserBox").hide();
	$("#ExamineStateDisplay").val("同意");
	$("#ExamineStateId").val("1");
	$('#SubmitMessageModal').modal('show');
}

/**
 * 不同意
 */
function disagree(){
	$("#newApprovalUserBox").hide();
	$("#ExamineStateDisplay").val("不同意");
	$("#ExamineStateId").val("0");
	$('#SubmitMessageModal').modal('show');
}

/**
 * 转审
 */
function addTempNode(){
	$("#newApprovalUserBox").show();
	$("#ExamineStateDisplay").val("转审");
	$("#ExamineStateId").val("2");
	$('#SubmitMessageModal').modal('show');
}

/**
 * 提交信息
 */
function SubmitMessage(){
	document.getElementById('job_handle_form').submit();
}


</script>
<style type="text/css">
.Wj_jiantou{
	margin-top: 30px;
	margin-left: 46%;
}

.Wj_tuzhang{
	position:absolute;
	right: 0;
	bottom:0;
	opacity: 0.3;
}

.Wj_jiaqian{
	position:absolute;
	left: 5px;
	top:-18px;
	width: 20px;
}

.card{
margin-top: 20px;
}

.col-sm-5{
text-align: right;
}

.col-sm-5 a{
margin-left: 5px;
}

.card-body{
padding: 0px;
}

</style> 
</head>
<body class="easyui-layout">
${workjob_node_html}

<!-- 信息提交框 -->
<div class="modal fade" id="SubmitMessageModal" tabindex="-1" role="dialog"  >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">审批信息</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="job_handle_form" action="job_handle_submit">
          <input type="hidden"  id="nodeidId" name="nodeid" value="${now_node.zid}">
          <input type="hidden"  id="widId" name="wid" value="${now_node.pid}">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">审批结果:</label>
            <input type="text" class="form-control" id="ExamineStateDisplay" value="" disabled="disabled">
            <input type="hidden" id="ExamineStateId" name="ExamineState">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">审批意见:</label>
            <textarea class="form-control" id="ExamineOpinionId" name="ExamineOpinion"></textarea>
          </div>
          <div class="form-group" id="newApprovalUserBox">
            <label for="recipient-name" class="col-form-label">新审批人:</label>
            <!-- <input type="text" class="form-control" id="newApprovalUserDisplay" value="" disabled="disabled">
            <input type="hidden" id="newApprovalUserId" name="newApprovalUser"> -->
            
            <div class="input-group">
			<input id="user_head_id" name="newApprovalUser" type="hidden" value="">
			<input placeholder="负责人" id="user_head_display" onclick="Z5list('z_org_user_head','user_head_id','user_head_display',0)" name="user_head_display" value="" type="text" class="form-control marginleft1" readonly="">
			<span class="input-group-prepend marginright1">
			<button class="btn btn-light" onclick="Z5Clear('z_org_user_head','user_head_id','user_head_display')" type="button"><i class="fa fa-trash-o"></i></button>
			<button id="user_head_selectbutton_id" class="btn btn-light" onclick="Z5list('z_org_user_head','user_head_id','user_head_display',0)" type="button"><i class="fa fa-search"></i></button>
			</span>
			</div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="SubmitMessage()">提交审批信息</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>