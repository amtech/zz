<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Z平台</title>
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">
function SaveUserPassword(){
	parent.openLoading();
	var form = new FormData($("#main_form")[0]);
	$.ajax({
		type : "POST",
		url : 'SaveUserPassword',
		data:form,
		async: false,//同步请求
		processData: false,//是否序列化 data
		contentType: false,
		success : function(data) {
			if(data.code=='SUCCESS'){
				parent.alertMessager('保存成功');
				window.location.href='user_settings';
			}else{
				parent.alertErrorMessager(''+data.msg);
			}
		},
		error: function (data) {
			parent.alertErrorMessager('ajax错误：'+JSON.stringify(data));
		},
		complete: function () {
			//执行完成
			parent.closedLoading();
		}
	});
}

function ReturnUserSettings(){
	window.location.href='user_settings';
}
</script>
</head>
<body class="easyui-layout">
<div data-options="region:'north',border:false" style="height:50px;overflow:hidden">
	<div id="ButtonHR" class="btn-group" role="group">
		<button id="SaveUserPasswordButton" type="button" class="btn btn-light" onclick="SaveUserPassword();"><i class="fa fa-floppy-o"></i> 保存</button>
		<button id="ReturnUserSettingsButton" type="button" class="btn btn-light" onclick="ReturnUserSettings();"><i class="fa fa-hand-o-left"></i> 返回</button>
	</div>
	
</div>
<div data-options="region:'center',border:false">
	<z:form id="main_form">
		<z:column tableId="z_user" column_type="password" column_size="3" isbr="1" column_name="当前密码" 	column_help="原密码"		column_id="login_password" 	 	/>
		<z:column tableId="z_user" column_type="password" column_size="3" isbr="1" column_name="新密码" 		column_help="新密码"		column_id="new_login_password1" />
		<z:column tableId="z_user" column_type="password" column_size="3" isbr="1" column_name="新密码确认" 	column_help="新密码确认"	column_id="new_login_password2" />
	</z:form>
</div>

<%@include file="/views/common/body.jsp"%>
</body>
</html>