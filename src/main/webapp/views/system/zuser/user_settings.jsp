<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Z平台</title>
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">
function SaveUserButton(){
	parent.openLoading();
	var form = new FormData($("#main_form")[0]);
	$.ajax({
		type : "POST",
		url : 'update',
		data:form,
		async: false,//同步请求
		processData: false,//是否序列化 data
		contentType: false,
		success : function(data) {
			if(data.code=='SUCCESS'){
				parent.alertMessager('保存成功');
				window.location.href='user_settings';
			}else{
				parent.alertErrorMessager(''+data.msg);
			}
		},
		error: function (data) {
			parent.alertErrorMessager('ajax错误：'+JSON.stringify(data));
		},
		complete: function () {
			//执行完成
			parent.closedLoading();
		}
	});
}

function user_settings_password(){
	window.location.href='user_settings_password';
}
</script>
</head>
<body class="easyui-layout">
<div data-options="region:'north',border:false" style="height:50px;overflow:hidden">
	<div id="ButtonHR" class="btn-group" role="group">
		<button id="SaveUserButton" type="button" class="btn btn-light" onclick="SaveUserButton();"><i class="fa fa-floppy-o"></i> 保存信息</button>
		<button id="user_settings_passwordButton" type="button" class="btn btn-light" onclick="user_settings_password();"><i class="fa fa-key"></i> 修改密码</button>
	</div>
	
</div>
<div data-options="region:'center',border:false">
	<z:form id="main_form" tableId="z_user" zid="${user.zid}">
		<z:column tableId="z_user" column_type="0" column_size="3"  column_name="用户名称" column_help="用户名称"	column_id="user_name" 	 value="${user.user_name}"/>
		<z:column tableId="z_user" column_type="0" column_size="3"  column_name="账号" 	column_help="账号"		column_id="user_id" 	 value="${user.user_id}"/>
		<z:column tableId="z_user" column_type="0" column_size="3"  column_name="手机" 	column_help="手机"		column_id="tel" 		 value="${user.tel}"/>
		<z:column tableId="z_user" column_type="0" column_size="3"  column_name="真实姓名" column_help="真实姓名"	column_id="realname" 	 value="${user.realname}"/>
		<z:column tableId="z_user" column_type="4" column_size="3"  column_name="头像" 	column_help="头像"		column_id="photo" 	 	 value="${user.photo}"/>
		<z:column tableId="z_user" column_type="9" column_size="3"  column_name="生日" 	column_help="生日"		column_id="birth_date" 	 value="${user.birth_date}"/>
		<z:column tableId="z_user" column_type="0" column_size="3"  column_name="身份证号" column_help="身份证号"	column_id="idcard" 		 value="${user.idcard}"/>
		<z:column tableId="z_user" column_type="0" column_size="3"  column_name="邮箱" 	column_help="邮箱"		column_id="email" 		 value="${user.email}"/>
		<z:column tableId="z_user" column_type="8" column_size="12" column_name="籍贯" 	column_help="籍贯"		column_id="native_place" value="${user.native_place}" z5_table="z_area" z5_key="area_id" z5_value="area_name" />
		<z:column tableId="z_user" column_type="0" column_size="12" column_name="现住地区" column_help="现住地区"	column_id="address" 	 value="${user.address}"/>
	</z:form>
</div>

<%@include file="/views/common/body.jsp"%>
</body>
</html>