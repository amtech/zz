<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Z平台</title>
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">

$(function () {
    $("#MainTable").datagrid({
        //双击事件
        onDblClickRow: function (index, row) {
        	getReturnValue();
        }
    });
})

/**
 * Z5List行双击事件
 * @param index
 * @param row
 * @returns
 */
function onDblClickRowZ5List(index, row){
	getReturnValue();
}

/**
 * 获取返回值
 */
function getReturnValue(){
	var row = $("#MainTable").datagrid("getSelected");
	if(!isNull(row)){
		//获取userIds
		var userIds =  getTableColumn('MainTable','zid');
		var roleid =  '${roleid}';
		$.ajax({
			type : "POST",
			url : "ImportUsersForRole",
			data:{userIds:userIds,roleid:roleid},
			success : function(data) {
				if(data.code=="SUCCESS"){
					window.close();
					window.opener.location.reload();
				}else{
					alert(data.data);
				}
			},
			error: function (data) {
				alert(data.data);
			}
		});
	}else{
		alert('请选择记录');
	}
}


</script>
</head>
<body class="easyui-layout">
	<!-- 查询域 -->
	<div data-options="region:'north',border:false"  >
		<div class="row" style="padding: 5px;margin:0px;">
			<div class="col-md-3">
				<div class="btn-group btn-group-sm" role="group">
					<button id="select_button" type="button" class="btn btn-dark" onclick="getReturnValue();"><i class="fa fa-get-pocket"></i> 导入用户</button>
				</div>
			</div>
			<div class="col-md-6">
				<div class="input-group input-group-sm">
					<input id="Z5QueryParametersInput" value="" type="text" class="form-control form-control-sm" placeholder="查询条件" />
					<span class="input-group-btn">
						<button class="btn btn-dark" onclick="Z5Query()" type="button"><i class="fa fa-search"> 查询</i></button>
					</span>
				</div>
			</div>
		</div>
	</div>

	<!-- 表格 -->
	<div data-options="region:'center',border:false"  >
		<table id="MainTable" class="easyui-datagrid" data-options="fit:true,ctrlSelect:true">
			<thead>
				<tr>
					<th data-options="field:'zid',checkbox:true,width:200"></th>
					<th data-options="field:'user_id',width:200">账号</th>
					<th data-options="field:'user_name',width:200">名称</th>
					<th data-options="field:'tel',width:200">手机号</th>
					<th data-options="field:'email',width:200">邮箱</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="obj" items="${userList}">
					<tr>
						<td>${obj.zid}</td>
						<td>${obj.user_id}</td>
						<td>${obj.user_name}</td>
						<td>${obj.tel}</td>
						<td>${obj.email}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>