<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Z平台</title>
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		//加载菜单
		init_menu();
	});

	function init_menu() {
		var setting = {
			data : {
				simpleData : {
					enable : true,
					idKey : "zid",
					pIdKey : "parentid",
					rootPId : 0
				}
			},
			check: {
				enable: true
			}
		};
		$.ajax({
			async : false,
			type : "get",
			url : "getMenuListForRole",
			dataType : "json",
			success : function(data) {
				$.fn.zTree.init($("#role_menu"), setting, data.data.menuList);
			}
		});

	}

	function getReturnValue() {
		var treeObj = $.fn.zTree.getZTreeObj("role_menu");
		var nodes = treeObj.getCheckedNodes(true);
		var menuIds="";
		for(var i=0;i<nodes.length;i++){            
			menuIds+=nodes[i].zid + ",";            
        }
		var roleid =  '${roleid}';
		$.ajax({
			type : "POST",
			url : "ImportMenuForRole",
			data:{menuIds:menuIds,roleid:roleid},
			success : function(data) {
				if(data.code=="SUCCESS"){
					window.close();
					window.opener.location.reload();
				}else{
					alert(data.data);
				}
			},
			error: function (data) {
				alert(data.data);
			}
		});
	}
</script>
</head>
<body class="easyui-layout">
	<!-- 查询域 -->
	<div data-options="region:'north',border:false">
		<div class="row" style="padding: 5px; margin: 0px;">
			<div class="col-md-12">
				<div class="btn-group btn-group-sm" role="group">
					<button id="select_button" type="button" class="btn btn-dark" onclick="getReturnValue();">
						<i class="fa fa-get-pocket"></i> 导入菜单
					</button>
				</div>
			</div>
		</div>
	</div>
	<div data-options="region:'center',border:false">
		<ul id="role_menu" class="ztree"></ul>
	</div>
</body>
</html>